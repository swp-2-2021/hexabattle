# Introduction

Hexabattle is an iPadOS Game programmed in Swift.


## Description
This is a Gaming App designed and implemented by some Students of the HFT Stuttgart. Based on an hexagon map, multiple players can fight against each other and grow there empire

# Getting started


## Dev Environment
You'll need a working MacOS development environment with XCode 12 to use work with this project. You can find instructions to get up and running on the Apple [XCode website](https://developer.apple.com/xcode/).

## Git Hooks
Your commits should be verified before your push. Therefore you must add git hooks locally.
The pattern for the commit messages is (#ISSUENUMBER): MESSAGE. If your branch name starts with the issue number the number will be added automatically. To add another issue number to your commit you can write the message like this: NUMBER MESSAGE.

Also linting is checked before the commit.

To do so run the following script from the project root folder:

```sh
./bootstrap.sh
```

