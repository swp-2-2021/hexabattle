//
//  GoldComponent.swift
//  hexabattle
//
//  Created by 91elhe1bif on 22.04.21.
//

import SpriteKit
import GameplayKit

/**
 "Gold" is the currency of the game. It can be used to buy buildings, troops and upgrades.
 The amount of gold the player receives per turn depends on the empire size and amount of cities.
 Each player receives 500 gold (default value) at the beginning of the game.
 A custom start amount is also possible for special gamemodes (express mode), different player
 amount or different team sizes in the future.
 */
class GoldComponent: GKComponent {

    var gold = 0
    var goldGeneration = 0

}
