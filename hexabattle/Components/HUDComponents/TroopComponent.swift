//
// Created by Marvin Klein on 06.05.21.
//

import SpriteKit
import GameplayKit

class TroopComponent: GKComponent {

    var troops = 1

    override init () {
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
