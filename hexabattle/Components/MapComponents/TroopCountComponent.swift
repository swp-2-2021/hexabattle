//
//  TroopCountComponent.swift
//  hexabattle
//
//  Created by Tom Biskupski on 24.04.21.
//

import SpriteKit
import GameplayKit

class TroopCountComponent: GKComponent {

    // Aktuelle Truppen auf dem Tile
    var currentTroops: Int {
        didSet {
            if let labelComponent = entity?.component(ofType: LabelComponent.self) {
                for label in labelComponent.nodes where label.name!.contains(".currentTroops") {
                    if(currentTroops > 0 ) {
                    label.text = String(currentTroops)
                    } else {
                        label.text = ""
                    }
                }
            }
        }
    }
    // Truppen, die in der Reinforcement Phase zu dem Tile hinzugefügt werden
    var addedTroops: Int {
        didSet {
            if let labelComponent = entity?.component(ofType: LabelComponent.self) {
                for label in labelComponent.nodes where label.name!.contains(".addedTroops") {
                    if addedTroops > 0 {
                        label.text = "+ " + String(addedTroops)
                    } else if addedTroops < 0 {
                        label.text = "- " + String(-1*addedTroops)
                    } else {
                        label.text = ""
                    }
                }
            }
        }
    }

    init(currentTroops: Int = 0, addedTroops: Int = 0) {
        self.currentTroops = currentTroops
        self.addedTroops = addedTroops
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
