//
// Created by Marvin Klein on 04.05.21.
//

import SpriteKit
import GameplayKit
import os.log

class CityTileComponent: GKComponent {

    // possible city levels: 1, 2, 3
    var cityLevel: Int = 1

    var isMainCity = false

    override init() {
        super.init()
        cityLevel = 1
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
