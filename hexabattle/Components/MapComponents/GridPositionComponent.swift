//
//  GridPositionComponent.swift
//  hexabattle
//
//  Created by Marvin Klein on 13.04.21.
//

import SpriteKit
import GameplayKit

class GridPositionComponent: GKComponent {

    let row: Int
    let column: Int
    var tileType: TileType
    let name: String

    init(column: Int = 0, row: Int = 0, tileType: TileType, name: String) {
        self.column = column
        self.row = row
        self.tileType = tileType
        self.name = name
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
