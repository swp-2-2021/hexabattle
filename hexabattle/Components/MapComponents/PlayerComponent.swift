//
// Created by Marvin Klein on 03.05.21.
//

import SpriteKit
import GameplayKit

class PlayerComponent: GKComponent {

    var player: Player {
        didSet {
            if let shapeComponent = entity?.component(ofType: ShapeComponent.self) {
                for node in shapeComponent.nodes where node.name!.contains(".circle") {
                    if player == Player.none {
                        node.isHidden = true
                        if let labelComponent = entity?.component(ofType: LabelComponent.self) {
                            for label in labelComponent.nodes where node.name!.contains(".currentTroops") {
                                label.isHidden = true
                            }
                        }
                    } else {
                        node.fillColor = player.getColor()
                        node.isHidden = false

                    }
                }
            }
        }
    }

    override init() {
        player = Player.none
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
