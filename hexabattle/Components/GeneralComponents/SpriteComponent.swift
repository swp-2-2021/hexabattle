//
//  SpriteComponent.swift
//  hexabattle
//
//  Created by Marcel Henning on 08.04.21.
//

import SpriteKit
import GameplayKit

struct Sprite {
    var texture: SKTexture
    var position: CGPoint
    var zPosition: CGFloat
    var size: CGSize?
    var name: String
    var scaleFactor: CGFloat?
}

class SpriteComponent: GKComponent {

    var nodes: [SKSpriteNode] = []

    private func addSpriteNode(sprite: Sprite) {
        let node = SKSpriteNode(texture: sprite.texture)
        node.name = sprite.name
        node.position = sprite.position
        node.zPosition = sprite.zPosition
        if let size = sprite.size {
            node.size = size
        }
        if let scaleFactor = sprite.scaleFactor {
            node.setScale(scaleFactor)
        }
        nodes.append(node)
    }

    /**
    Add single Sprite
     */
    init(sprite: Sprite) {
        super.init()
        addSpriteNode(sprite: sprite)
    }

    /**
    Add multiple Sprites
     */
    init(sprites: [Sprite]) {
        super.init()
        for sprite in sprites {
            addSpriteNode(sprite: sprite)
        }
    }

    /**
    Returns if the node is contained in the component
    - Parameter name: name of the node
    */
    func containsNode(_ name: String) -> Bool! {
        for node in nodes where node.name == name {
            return true
        }
        return false
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
