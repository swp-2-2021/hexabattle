//
//  LabelComponent.swift
//  hexabattle
//
//  Created by Marcel Henning on 08.04.21.
//

import SpriteKit
import GameplayKit

struct Label {
    var name: String
    var position: CGPoint
    var zPosition: CGFloat
    var text: String
    var styling: LabelStyle
    var lines: Int?
}

struct LabelStyle {
    var fontColor = UIColor.hexaDarkBlue
    var fontSize: CGFloat?
    var labelHorizontalAlignmentMode: SKLabelHorizontalAlignmentMode?
}

class LabelComponent: GKComponent {

    var nodes: [SKLabelNode] = []

    private func addLabelNode(label: Label) {
        var node = SKLabelNode()
        node.name = label.name
        node.text = label.text
        node.position = label.position
        node.zPosition = label.zPosition
        node.fontColor = label.styling.fontColor
        node.fontName = Font.fontName
        if let fontSize = label.styling.fontSize {
            node.fontSize = fontSize
        }

        if let lines = label.lines {
            node.lineBreakMode = .byWordWrapping
            node.numberOfLines = lines
            node.preferredMaxLayoutWidth = 300
            node.verticalAlignmentMode = .top
        }

        if let labelHorizontalAlignmentMode = label.styling.labelHorizontalAlignmentMode {
            node.horizontalAlignmentMode = labelHorizontalAlignmentMode
        }
        nodes.append(node)
    }

    /**
     Add single Label
     */
    init(label: Label) {
        super.init()
        addLabelNode(label: label)
    }

    /**
    Add multiple Labels
    */
    init(labels: [Label]) {
        super.init()
        for label in labels {
            addLabelNode(label: label)
        }
    }

    /**
     Returns if the node is contained in the component
     - Parameter name: name of the node
     */
    func containsNode(_ name: String) -> Bool! {
        for node in nodes where node.name == name {
            return true
        }
        return false
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
