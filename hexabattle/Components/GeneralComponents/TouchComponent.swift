//
//  TouchComponent.swift
//  hexabattle
//
//  Created by Marvin Klein on 18.04.21.
//

import SpriteKit
import GameplayKit

class TouchComponent: GKComponent {

    let behavior: AbstractTouchBehavior

    init(behavior: AbstractTouchBehavior) {
        self.behavior = behavior
        super.init()
    }

    func didTap(touchedNodeName: String) {
        if let labelComponent = entity?.component(ofType: LabelComponent.self) {
            if labelComponent.containsNode(touchedNodeName) {
                behavior.didTap(touchedNodeName: touchedNodeName)
                return
            }
        }
        if let spriteComponent = entity?.component(ofType: SpriteComponent.self) {
            if spriteComponent.containsNode(touchedNodeName) {
                behavior.didTap(touchedNodeName: touchedNodeName)
                return
            }
        }
        if let shapeComponent = entity?.component(ofType: ShapeComponent.self) {
            if shapeComponent.containsNode(touchedNodeName) {
                behavior.didTap(touchedNodeName: touchedNodeName)
                return
            }

        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func didHold(touchedNodeName: String) {
        if let labelComponent = entity?.component(ofType: LabelComponent.self) {
            if labelComponent.containsNode(touchedNodeName) {
                behavior.didHold(touchedNodeName: touchedNodeName)
                return
            }
        }
        if let spriteComponent = entity?.component(ofType: SpriteComponent.self) {
            if spriteComponent.containsNode(touchedNodeName) {
                behavior.didHold(touchedNodeName: touchedNodeName)
                return
            }
        }
        if let shapeComponent = entity?.component(ofType: ShapeComponent.self) {
            if shapeComponent.containsNode(touchedNodeName) {
                behavior.didHold(touchedNodeName: touchedNodeName)
                return
            }

        }
    }
}
