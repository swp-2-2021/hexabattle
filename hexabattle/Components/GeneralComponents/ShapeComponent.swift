//
//  ShapeComponent.swift
//  hexabattle
//
//  Created by Marvin Klein on 13.04.21.
//

import SpriteKit
import GameplayKit

struct Shape {
    var shapeType: ShapeType
    var name: String
    var position: CGPoint
    var zPosition: CGFloat
    var color: UIColor?
    var fillColor: UIColor?
    var strokeColor: UIColor?
    var glowWidth: CGFloat?
    var lineWidth: CGFloat = 5.0
    var hide: Bool = false
    var opacity: CGFloat = 1.0
}

enum ShapeType {
    case hexagon(Hexagon)
    case circle(Circle)
    case rectangle(Rectangle)
    case arrow(Arrow)
}

struct Rectangle {
    var size: CGSize
}

struct Hexagon {
    var size: CGFloat
}

struct Circle {
    var radius: CGFloat
}

struct Arrow {
    var srcTile: TileEntity
    var destTile: TileEntity
}

class ShapeComponent: GKComponent {

    var nodes: [SKShapeNode] = []

    func pointyHexCorner(center: CGPoint, size: CGFloat, corner: Int) -> CGPoint {
        let angleDeg = 60 * corner - 30
        let angleRad = CGFloat.pi / 180.0 * CGFloat(angleDeg)
        return CGPoint(
                x: (size * cos(angleRad)),
                y: (size * sin(angleRad)))
    }

    func getHexPath(center: CGPoint, size: CGFloat) -> CGPath {
        let path = CGMutablePath()
        var hexpoints: [CGPoint] = []
        for identifier in 0..<7 {
            hexpoints.append(pointyHexCorner(center: center, size: size, corner: identifier))
        }
        path.addLines(between: hexpoints)
        return path
    }

    func getArrowPath(srcTile: TileEntity, destTile: TileEntity) -> CGPath {
        var srcPosition = CGPoint()
        var destPosition = CGPoint()

        if let gridComponent = srcTile.component(ofType: GridPositionComponent.self) {
            srcPosition = MapManager.instance.offset2Pixel(hexRow: gridComponent.row, hexCol: gridComponent.column)
        }

        if let gridComponent = destTile.component(ofType: GridPositionComponent.self) {
            destPosition = MapManager.instance.offset2Pixel(hexRow: gridComponent.row, hexCol: gridComponent.column)
        }

        let headlen = 25; // Customize the length of the arrow line
        let theta = 45; // Customize the angle between the arrow line and the line, personally feel that 45 ° is just right
        // Calculate the angle of each angle and the corresponding arrow end point
        let angle = atan2(srcPosition.y - destPosition.y, srcPosition.x - destPosition.x) * 180 / 3.14
        let angle1 = (angle + CGFloat(theta)) * 3.14 / 180
        let angle2 = (angle - CGFloat(theta)) * 3.14 / 180
        let topX = CGFloat(headlen) * cos(angle1)
        let topY = CGFloat(headlen) * sin(angle1)
        let botX = CGFloat(headlen) * cos(angle2)
        let botY = CGFloat(headlen) * sin(angle2)

        let path = CGMutablePath()
        // draw a straight line
        path.move(to: srcPosition)
        path.addLine(to: destPosition)

        var arrowX = destPosition.x + topX
        var arrowY = destPosition.y + topY
        // Draw the upper arrow line
        path.move(to: CGPoint(x: arrowX, y: arrowY))
        path.addLine(to: CGPoint(x: destPosition.x, y: destPosition.y))

        arrowX = destPosition.x + botX
        arrowY = destPosition.y + botY
        // Draw the arrow line below
        path.move(to: CGPoint(x: arrowX, y: arrowY))
        path.addLine(to: CGPoint(x: destPosition.x, y: destPosition.y))

        return path
    }

    /**
    Add single Shape
    */
    init(shape: Shape) {
        super.init()
        addShapeNode(shape: shape)
    }

    /**
     Add multiple Shapes
     */
    init(shapes: [Shape]) {
        super.init()
        for shape in shapes {
            addShapeNode(shape: shape)
        }
    }

    private func addShapeNode(shape: Shape) {
        var node = SKShapeNode()
        switch shape.shapeType {
        case .hexagon(let hexagon):
            node.path = getHexPath(center: shape.position, size: hexagon.size)

        case .circle(let circle):
            node = SKShapeNode(circleOfRadius: circle.radius)

        case .rectangle(let rectangle):
            node = SKShapeNode(rectOf: rectangle.size)

        case .arrow(let arrow):
            node.path = getArrowPath(srcTile: arrow.srcTile, destTile: arrow.destTile)
            node.strokeColor = shape.strokeColor!
            node.glowWidth = shape.glowWidth!
        }
        node.name = shape.name
        node.zPosition = shape.zPosition
        node.position = shape.position
        node.isHidden = shape.hide
        node.alpha = shape.opacity
        if let color = shape.color {
            node.strokeColor = color
            node.fillColor = color
        }
        if let fillColor = shape.fillColor {
            node.fillColor = fillColor
        }
        if let strokeColor = shape.strokeColor {
            node.strokeColor = strokeColor
        }
        if let glowWidth = shape.glowWidth {
            node.glowWidth = glowWidth
        }

        node.lineWidth = shape.lineWidth

        nodes.append(node)
    }

    /**
    Returns if the node is contained in the component
    - Parameter name: name of the node
    */
    func containsNode(_ name: String) -> Bool! {
        for node in nodes where node.name == name {
            return true
        }
        return false
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
