//
//  LabelComponent.swift
//  hexabattle
//
//  Created by Marcel Henning on 08.04.21.
//

import SpriteKit
import GameplayKit

struct Slider {
    var name: String
    var position: CGPoint
    var size: CGSize
    var minimumValue: Float
    var maximumValue: Float
    var isContinuous: Bool = true
    var step: CGFloat?
    var initValue: Float?
}

class SliderComponent: GKComponent {
    var uiSlider:UISlider = UISlider()
    var step: CGFloat = 1
    var behavior: AbstractSliderBehavior

    init(slider: Slider, behavior: AbstractSliderBehavior) {
        self.behavior = behavior
        super.init()
        uiSlider = UISlider(frame:CGRect(x: slider.position.x, y: slider.position.y, width: slider.size.width, height: slider.size.height))
        uiSlider.addTarget(self, action: #selector(self.sliderValueDidChange(_:)), for: .valueChanged)
        uiSlider.minimumValue = slider.minimumValue
        uiSlider.maximumValue = slider.maximumValue
        uiSlider.isContinuous = slider.isContinuous
        uiSlider.minimumTrackTintColor = UIColor.hexaDarkBlue
        uiSlider.thumbTintColor = UIColor.hexaLightRed
        uiSlider.isHidden = true
        if let step = slider.step {
            self.step = step
        }
        if let initValue = slider.initValue {
            uiSlider.value = initValue
        }
    }

    @objc func sliderValueDidChange(_ sender: UISlider!) {
        behavior.sliderValueDidChange(sender)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
