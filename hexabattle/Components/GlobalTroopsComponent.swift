//
//  GlobalTroopsComponent.swift
//  hexabattle
//
//  Created by Tom Biskupski on 03.05.21.
//

import SpriteKit
import GameplayKit

class GlobalTroopsComponent: GKComponent {

    // Alle verfügbaren Truppen eines Spieler, die gesetzt werden können
    var troopsAvailable: Int {
        didSet {
            if let labelComponent = entity?.component(ofType: LabelComponent.self) {
                for label in labelComponent.nodes where label.name!.contains(".troopsAvailable") {
                    label.text = "Available Troops: " + String(troopsAvailable)
                }
            }
        }
    }
    // Truppen, die ein Spieler bereits in der aktuellen Reinforcementphase gesetzt hat
    var troopsPlaced: Int {
        didSet {
            if let labelComponent = entity?.component(ofType: LabelComponent.self) {
                for label in labelComponent.nodes where label.name!.contains(".troopsPlaced") {
                    label.text = String(troopsPlaced)
                }
            }
        }
    }

    init(troopsAvailable: Int = 0, troopsPlaced: Int = 0) {
        self.troopsAvailable = troopsAvailable
        self.troopsPlaced = troopsPlaced
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
