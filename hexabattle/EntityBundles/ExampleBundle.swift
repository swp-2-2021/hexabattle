//
// Created by Marvin Klein on 26.04.21.
//

import SpriteKit

class ExampleBundle: AbstractEntityBundle {

    // TODO all positions relate to frame
    init(bundleName: String, frame: CGRect) {
        super.init(name: bundleName, frame: frame)
        self.name = bundleName
        addEntity(entity: SimpleSpriteEntity(imageName: "buttonBlack", position: CGPoint(x: 400, y: 300),
                zPosition: ZPosition.hud3, name: "test-1"))
        addEntity(entity: SimpleSpriteEntity(imageName: "buttonBlack", position: CGPoint(x: 500, y: 300),
                zPosition: ZPosition.hud3, name: "test-2"))

    }
}
