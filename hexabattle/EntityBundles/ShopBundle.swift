//
//  ShopBundle.swift
//  hexabattle
//
//  Created by Dejan Mijic on 27.04.21.
//

import SpriteKit

class ShopBundle: AbstractEntityBundle {

    // TODO all positions relate to frame

    init(bundleName: String, frame: CGRect) {
        super.init(name: bundleName, frame: frame)
        self.name = bundleName
        addEntity(entity: SimpleShapeEntity(
                size: CGSize(width: 1050, height: 700),
                position: point(x: Size.sceneSize.width / 2 + 90, y: Size.sceneSize.height / 2 + 30),
                zPosition: ZPosition.popupBackground,
                name: Names.shopBackground))
        initButtons()
        initLabels()
    }

    private func initLabels() {
        addEntity(entity: SimpleLabelEntity(
                position: point(x: Size.sceneSize.width / 2 - 180, y: Size.sceneSize.height / 2 + 295),
                zPosition: ZPosition.popup2,
                text: "HEXASHOP",
                    labelColor: UIColor.hexaDarkBlue,
                fontSize: 80,
                name: Names.labelShopTitle))
        addEntity(entity: SimpleLabelEntity(
                position: point(x: Size.sceneSize.width / 2 - 150, y: Size.sceneSize.height / 2 + 140),
                zPosition: ZPosition.popup2,
                text: "RECRUIT TROOPS",
                labelColor: UIColor.hexaDarkBlue, fontSize: 60, name: Names.labelTroopsDescription))
        addEntity(entity: SimpleLabelEntity(
                position: point(x: Size.sceneSize.width / 2 - 175, y: Size.sceneSize.height / 2 - 10),
                zPosition: ZPosition.popup2,
                text: "WALL UPGRADE",
                labelColor: UIColor.hexaDarkBlue,
                fontSize: 60,
                name: Names.labelWallDescription))
        addEntity(entity: SimpleLabelEntity(
                position: point(x: Size.sceneSize.width / 2 - 175, y: Size.sceneSize.height / 2 - 60),
                zPosition: ZPosition.popup2,
                text: "Lvl. 1 → Lvl. 2",
                labelColor: UIColor.hexaDarkRed,
                fontSize: 30,
                name: Names.labelWallImpact))
        addEntity(entity: SimpleLabelEntity(
                position: point(x: Size.sceneSize.width / 2 - 135, y: Size.sceneSize.height / 2 - 160),
                zPosition: ZPosition.popup2,
                text: "MARKET UPGRADE",
                labelColor: UIColor.hexaDarkBlue,
                fontSize: 60,
                name: Names.labelMarketDescription))
        addEntity(entity: SimpleLabelEntity(
                position: point(x: Size.sceneSize.width / 2 - 135, y: Size.sceneSize.height / 2 - 210),
                zPosition: ZPosition.popup2,
                text: "Gold Generation Per Turn: " + String(GoldBehavior.instance.getGoldGeneration())
                        + " → " + String(GoldBehavior.instance.getGoldGeneration() + GameValues.goldIncreasement),
                labelColor: UIColor.green,
                fontSize: 30,
                name: Names.labelMarketImpact))
        addEntity(entity: SimpleLabelEntity(
                position: point(x: Size.sceneSize.width / 2 + 520, y: Size.sceneSize.height / 2 + 85),
                zPosition: ZPosition.popup2,
                text: "Total: " + String(EntityManager.instance.troopsAmount * GameValues.priceRecruitTroop),
                labelColor: UIColor.hexaDarkRed,
                fontSize: 28,
                name: Names.labelTotalTroopPrice))
        addEntity(entity: SimpleLabelEntity(
                    position: point(x: Size.sceneSize.width / 2 + 370, y: Size.sceneSize.height / 2 + 150),
                    zPosition: ZPosition.popup2,
                text: String(EntityManager.instance.troopsAmount) + " x " + String(GameValues.priceRecruitTroop),
                labelColor: UIColor.hexaDarkRed,
                fontSize: 28,
                name: Names.labelTroopsPriceTag))
        addEntity(entity: SimpleLabelEntity(
                position: point(x: Size.sceneSize.width / 2 + 130, y: Size.sceneSize.height / 2 - 280),
                zPosition: ZPosition.popup2,
                text: "",
                labelColor: UIColor.hexaDarkRed,
                fontSize: 35,
                name: Names.labelTransactionStatus))
    }

    private func initButtons() {
        addEntity(entity: ButtonEntity(
                imageName: "GoldLabel2",
                position: point(x: Size.sceneSize.width / 2 + 525, y: Size.sceneSize.height / 2 + 160),
                zPosition: ZPosition.popup1,
                size: CGSize(width: 150, height: 50),
                name: Names.buttonBuyTroops,
                labelOffset: CGPoint(x: 30, y: -12),
                text: String(GameValues.priceRecruitTroop),
                labelColor: UIColor.hexaDarkBlue,
                fontSize: 32,
                behavior: BuyTroopsTouchBehavior(entityName: "availableGoldLabel") as AbstractTouchBehavior)
        )
        addEntity(entity: ButtonEntity(
                imageName: "SmallButton",
                position: point(x: Size.sceneSize.width / 2 + 245, y: Size.sceneSize.height / 2 + 160),
                zPosition: ZPosition.popup1,
                size: CGSize(width: 70, height: 70),
                name: Names.buttonAddTroops,
                text: "+",
                labelColor: UIColor.hexaWhite,
                fontSize: 40,
                behavior: AddTroopsTouchBehavior(entityName: Names.labelTroopsPriceTag) as AbstractTouchBehavior)
        )

        addEntity(entity: ButtonEntity(
                imageName: "SmallButton",
                position: point(x: Size.sceneSize.width / 2 + 170, y: Size.sceneSize.height / 2 + 160),
                zPosition: ZPosition.popup1,
                size: CGSize(width: 70, height: 70),
                name: Names.buttonReduceTroops,
                text: "-",
                labelColor: UIColor.hexaWhite,
                fontSize: 40,
                behavior: ReduceTroopsTouchBehavior(entityName: Names.labelTroopsPriceTag) as AbstractTouchBehavior))

        addEntity(entity: ButtonEntity(
                imageName: "GoldLabel2",
                position: point(x: Size.sceneSize.width / 2 + 525, y: Size.sceneSize.height / 2 + 10),
                zPosition: ZPosition.popup1,
                size: CGSize(width: 150, height: 50),
                name: Names.buttonBuyWall,
                labelOffset: CGPoint(x: 30, y: -12),
                text: String(GameValues.priceCityUpgrade),
                labelColor: UIColor.hexaDarkBlue,
                fontSize: 32,
                behavior: BuyWallTouchBehavior(entityName: Names.labelWallImpact) as AbstractTouchBehavior))

        addEntity(entity: ButtonEntity(
                imageName: "GoldLabel2",
                position: point(x: Size.sceneSize.width / 2 + 525, y: Size.sceneSize.height / 2 - 140),
                zPosition: ZPosition.popup1,
                size: CGSize(width: 150, height: 50),
                name: Names.buttonBuyMarket,
                labelOffset: CGPoint(x: 30, y: -12),
                text: String(GameValues.priceMarketUpgrade),
                labelColor: UIColor.hexaDarkBlue,
                fontSize: 32,
                behavior: BuyMarketTouchBehavior(entityName: Names.labelMarketImpact) as AbstractTouchBehavior))

        addEntity(entity: ButtonEntity(
                imageName: "CloseButton",
                position: point(x: Size.sceneSize.width / 2 + 570, y: Size.sceneSize.height / 2 + 330),
                zPosition: ZPosition.popup1,
                size: CGSize(width: 68, height: 69),
                name: Names.buttonCloseShop,
                hasLabel: false,
                labelColor: UIColor.hexaWhite,
                behavior: HideShopTouchBehavior(entityName: Names.labelTroopsPriceTag) as AbstractTouchBehavior))
    }
}
