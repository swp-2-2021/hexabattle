//
// Created by Marvin Klein on 26.04.21.
//

import GameKit
import SpriteKit

class AbstractEntityBundle {

    var name: String
    var frame: CGRect

    var entities = Set<GKEntity>()

    init(name: String, frame: CGRect) {
        self.name = name
        self.frame = frame
    }

    func addEntity(entity: GKEntity) {
        entities.insert(entity)
    }

    // swiftlint:disable identifier_name
    func point(x: CGFloat, y: CGFloat) -> CGPoint {
        CGPoint(x: (x - frame.midX), y: (y - frame.midY))
    }

    func hideBundleEntityByName(hide: Bool, name: String) {
        for entity in entities {
            if let spriteNodes = entity.component(ofType: SpriteComponent.self)?.nodes {
                for spriteNode in spriteNodes where spriteNode.name == name {
                    spriteNode.isHidden = hide
                }
            }
            if let labelNodes = entity.component(ofType: LabelComponent.self)?.nodes {
                for labelNode in labelNodes where labelNode.name == name {
                    labelNode.isHidden = hide
                }
            }
            if let shapeNodes = entity.component(ofType: ShapeComponent.self)?.nodes {
                for shapeNode in shapeNodes where shapeNode.name == name {
                    shapeNode.isHidden = hide
                }
            }
        }
    }

    func hideBundle(hide: Bool) {
        for entity in entities {
            if let spriteNodes = entity.component(ofType: SpriteComponent.self)?.nodes {
                for spriteNode in spriteNodes {
                    spriteNode.isHidden = hide
                }
            }
            if let labelNodes = entity.component(ofType: LabelComponent.self)?.nodes {
                for labelNode in labelNodes {
                    labelNode.isHidden = hide
                }

            }
            if let shapeNodes = entity.component(ofType: ShapeComponent.self)?.nodes {
                for shapeNode in shapeNodes {
                    shapeNode.isHidden = hide
                }
            }
            if let sliderUi = entity.component(ofType: SliderComponent.self)?.uiSlider {
                sliderUi.isHidden = hide
            }
        }
    }
}
