//
//  InGameHelpBundle.swift
//  hexabattle
//
//  Created by 91elhe1bif on 05.06.21.
//
import SpriteKit

class InGameHelpBundle: AbstractEntityBundle {

    // TODO all positions relate to frame

    init(bundleName: String, frame: CGRect) {
        super.init(name: bundleName, frame: frame)
        self.name = bundleName
        addEntity(entity: SimpleShapeEntity(
                size: CGSize(width: 350, height: 170),
                    position: point(x: 200, y: Size.sceneSize.height  - 220),
                    zPosition: ZPosition.helpBackground,
                    name: Names.helpBackground)
                    )
        initButtons()
        initLabels()
    }
    private func initLabels() {
        addEntity(entity: SimpleLabelEntity(
                    position: point(x: 85, y: Size.sceneSize.height - 180),
                zPosition: ZPosition.help2,
                text: "Help",
                labelColor: UIColor.hexaDarkBlue,
                fontSize: 40,
                name: Names.labelHelp))
        addEntity(entity: SimpleLabelEntity(
                position: point(x: 185, y: Size.sceneSize.height - 200),
                zPosition: ZPosition.help2,
                text: "",
                labelColor: UIColor.hexaDarkBlue,
                fontSize: 20,
                name: Names.labelHelpText,
                lines: 3))
    }

    private func initButtons() {
        addEntity(entity: ButtonEntity(
                imageName: "HideButton",
                position: point(x: 340, y: Size.sceneSize.height - 170),
                zPosition: ZPosition.help1,
                size: CGSize(width: 50, height: 51),
                name: Names.buttonCloseInGameHelp,
                behavior: HideInGameHelpTouchBehavior(entityName: Names.buttonCloseInGameHelp))
        )
    }
}
