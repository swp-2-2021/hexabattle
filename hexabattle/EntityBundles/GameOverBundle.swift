//
// Created by Marvin Klein on 27.05.21.
//

import SpriteKit

class GameOverBundle: AbstractEntityBundle {

    init(bundleName: String, text: String) {
        super.init(name: bundleName, frame: CGRect())
        addEntity(entity: SimpleShapeEntity(
                size: CGSize(width: Size.sceneSize.width - 400, height: Size.sceneSize.height - 150),
                position: CGPoint(x: 0, y: 0),
                zPosition: ZPosition.popupBackground,
                name: bundleName + ".background"))
        addEntity(entity: SimpleLabelEntity(
                position: CGPoint(x: 0, y: Size.sceneSize.height * 0.25),
                zPosition: ZPosition.popup1,
                text: "GAMEOVER",
                labelColor: UIColor.hexaDarkRed,
                fontSize: CGFloat(100),
                name: bundleName + ".label"))
        addEntity(entity: SimpleLabelEntity(
                position: CGPoint(x: 0, y: Size.sceneSize.height * 0.1),
                zPosition: ZPosition.popup1,
                text: text,
                labelColor: UIColor.hexaWhite,
                fontSize: CGFloat(80),
                name: bundleName + "label"))
        addEntity(entity: ButtonEntity(
                imageName: "ResumeButton",
                position: CGPoint(x: 0, y: -Size.sceneSize.height * 0.2),
                zPosition: ZPosition.popup1,
                size: CGSize(width: 170, height: 170),
                name: bundleName,
                text: "QUIT GAME",
                labelColor: UIColor.hexaWhite,
                fontSize: 25,
                behavior: QuitGameTouchBehavior(entityName: bundleName) as AbstractTouchBehavior)
        )

    }
}
