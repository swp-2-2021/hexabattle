//
//  ManualBundle.swift
//  hexabattle
//
//  Created by Dejan Mijic on 15.06.21.
//

import SpriteKit

class ManualBundle: AbstractEntityBundle {

    init(bundleName: String, frame: CGRect, imageName: String) {
        super.init(name: bundleName, frame: frame)
        self.name = bundleName
        addEntity(entity: SimpleSpriteEntity(
                    imageName: imageName,
                    position: point(x: Size.sceneSize.width * 0.5, y: Size.sceneSize.height * 0.5),
                    zPosition: ZPosition.options2,
                    scaleFactor: 0.3,
                    name: Names.manualBackground))
        initButtons()
    }

    private func initButtons() {
        addEntity(entity: ButtonEntity(
                imageName: "ResumeButton",
                position: point(x: Size.sceneSize.width * 0.9, y: Size.sceneSize.height * 0.9),
                zPosition: ZPosition.options3,
                size: CGSize(width: 68, height: 69),
                name: Names.buttonCloseManual,
                labelColor: UIColor.hexaWhite,
                fontSize: 25,
                behavior: HideManualTouchBehavior(entityName: Names.buttonCloseManual) as AbstractTouchBehavior)
        )
        addEntity(entity: ButtonEntity(
                imageName: "nextPage",
                position: point(x: Size.sceneSize.width / 2 * 3/2, y: Size.sceneSize.height / 7),
                zPosition: ZPosition.options3,
                size: CGSize(width: 175, height: 45),
                name: Names.buttonNextPage,
                text: "",
                labelColor: UIColor.hexaDarkBlue,
                fontSize: 25,
                behavior: NextPageManualTouchBehavior(entityName: Names.buttonNextPage) as AbstractTouchBehavior)
        )
        addEntity(entity: ButtonEntity(
                imageName: "previousPage",
                position: point(x: Size.sceneSize.width / 2 , y: Size.sceneSize.height / 7 ),
                zPosition: ZPosition.options3,
                size: CGSize(width: 250, height: 45),
                name: Names.buttonPreviousPage,
                text: "",
                labelColor: UIColor.hexaDarkBlue,
                fontSize: 25,
                behavior: PreviousPageManualTouchBehavior(entityName: Names.buttonPreviousPage) as AbstractTouchBehavior)
        )

    }
}
