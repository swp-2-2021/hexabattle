//
//  ManualBundle.swift
//  hexabattle
//
//  Created by Dejan Mijic on 15.06.21.
//

import SpriteKit

class StartManualBundle: AbstractEntityBundle {

    init(bundleName: String, frame: CGRect, imageName: String) {
        super.init(name: bundleName, frame: frame)
        self.name = bundleName
        addEntity(entity: SimpleSpriteEntity(
                    imageName: imageName,
                    position: CGPoint(x: Size.sceneSize.width * 0.5, y: Size.sceneSize.height * 0.5),
                    zPosition: ZPosition.options1,
                    scaleFactor: 0.3,
                    name: Names.manualBackground))
        initButtons()
    }

    private func initButtons() {
        addEntity(entity: SimpleSpriteEntity(
                    imageName: "ResumeButton",
                    position: CGPoint(x: Size.sceneSize.width * 0.9, y: Size.sceneSize.height * 0.88),
                    zPosition: ZPosition.options3,
                    scaleFactor: 1.1,
                    name: "closeManual"))
        addEntity(entity: SimpleSpriteEntity(
                    imageName: "nextPage",
                    position: CGPoint(x: Size.sceneSize.width * 0.6, y: Size.sceneSize.height * 0.1),
                    zPosition: ZPosition.options3,
                    scaleFactor: 0.6,
                    name: "nextPage"))
        addEntity(entity: SimpleSpriteEntity(
                    imageName: "previousPage",
                    position: CGPoint(x: Size.sceneSize.width * 0.4, y: Size.sceneSize.height * 0.1 ),
                    zPosition: ZPosition.options3,
                    scaleFactor: 0.6,
                    name: "previousPage"))

    }
}
