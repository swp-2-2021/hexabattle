//
//  Created by Martin Uhlig on 28.04.21.
//

import SpriteKit

class OptionsBundle: AbstractEntityBundle {

    // TODO all positions relate to frame

    init(bundleName: String, frame: CGRect) {
        super.init(name: bundleName, frame: frame)
        self.name = bundleName
        addEntity(entity: SimpleShapeEntity(
                size: CGSize(width: Size.sceneSize.width * 0.7, height: Size.sceneSize.height * 0.7),
                position: point(x: Size.sceneSize.width * 0.5, y: Size.sceneSize.height * 0.5),
                zPosition: ZPosition.optionsBackground,
                name: Names.optionsBackground)
        )
        initButtons()
        initLabels()
    }

    private func initLabels() {
        addEntity(entity: SimpleLabelEntity(
                position: point(x: Size.sceneSize.width * 0.25, y: Size.sceneSize.height * 0.75),
                zPosition: ZPosition.options2,
                text: "OPTIONS",
                labelColor: UIColor.hexaLightRed,
                fontSize: 80,
                labelHorizontalAlignmentMode: SKLabelHorizontalAlignmentMode.left,
                name: Names.labelOptions))
        addEntity(entity: SimpleLabelEntity(
                position: point(x: Size.sceneSize.width * 0.25, y: Size.sceneSize.height * 0.65),
                zPosition: ZPosition.options2,
                text: "FX Volume",
                labelColor: UIColor.hexaDarkBlue,
                fontSize: 40,
                labelHorizontalAlignmentMode: SKLabelHorizontalAlignmentMode.left,
                name: Names.labelSound))
        addEntity(entity: SimpleLabelEntity(
                position: point(x: Size.sceneSize.width * 0.25, y: Size.sceneSize.height * 0.55),
                zPosition: ZPosition.options2,
                text: "Music Volume",
                labelColor: UIColor.hexaDarkBlue,
                fontSize: 40,
                labelHorizontalAlignmentMode: SKLabelHorizontalAlignmentMode.left,
                name: Names.labelVolume))
    }

    private func initButtons() {
        var fxInitValue: Float
        if let effectVolume = UserDefaults.standard.value(forKey: "effectVolume") as? Float {
            fxInitValue = effectVolume
        } else {
            fxInitValue = GameValues.volumeMusic
        }
        var musicInitValue: Float
        if let effectVolume = UserDefaults.standard.value(forKey: "audioVolume") as? Float {
            musicInitValue = effectVolume
        } else {
            musicInitValue = GameValues.volumeMusic
        }

        addEntity(entity: ButtonEntity(
                imageName: "ResumeButton",
                position: point(x: Size.sceneSize.width * 0.8, y: Size.sceneSize.height * 0.75 + 35),
                zPosition: ZPosition.options1,
                size: CGSize(width: 70, height: 70),
                name: Names.buttonCloseOptions,
                labelColor: UIColor.hexaWhite,
                fontSize: 25,
                behavior: HideOptionsTouchBehavior(entityName: Names.buttonCloseOptions) as AbstractTouchBehavior)
        )

        addEntity(entity: SliderEntity(
                position: CGPoint(x: EntityManager.instance.view.bounds.width / 2, y: EntityManager.instance.view.bounds.height * 0.33),
                size: CGSize(width: 300, height: 20),
                minimumValue: 0,
                maximumValue: 1,
                initValue: fxInitValue,
                step: 0.1,
                name: Names.audioVolume,
                behavior: SetFxVolumeBehavior())
        )
        addEntity(entity: SliderEntity(
                position: CGPoint(x: EntityManager.instance.view.bounds.width / 2, y: EntityManager.instance.view.bounds.height * 0.43),
                size: CGSize(width: 300, height: 20),
                minimumValue: 0,
                maximumValue: 1,
                initValue: musicInitValue,
                step: 0.1,
                name: Names.audioVolume,
                behavior: SetAudioVolumeBehavior())
        )

        addEntity(entity: ButtonEntity(
                imageName: "QuitButton",
                position: point(x: Size.sceneSize.width * 0.6, y: Size.sceneSize.height * 0.3),
                zPosition: ZPosition.options1,
                size: CGSize(width: 170, height: 90),
                name: Names.buttonQuitGame,
                text: "QUIT GAME",
                labelColor: UIColor.hexaWhite,
                fontSize: 25,
                behavior: QuitGameTouchBehavior(entityName: Names.buttonQuitGame) as AbstractTouchBehavior)
        )

        addEntity(entity: ButtonEntity(
                imageName: "ButtonBlack",
                position: point(x: Size.sceneSize.width * 0.4, y: Size.sceneSize.height * 0.3),
                zPosition: ZPosition.options1,
                size: CGSize(width: 200, height: 90),
                name: Names.buttonOpenManual,
                text: "HOW TO PLAY",
                labelColor: UIColor.hexaDarkBlue,
                fontSize: 25,
                behavior: ShowManualTouchBehavior(entityName: Names.buttonOpenManual) as AbstractTouchBehavior)
        )
    }
}
