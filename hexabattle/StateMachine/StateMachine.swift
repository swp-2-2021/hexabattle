//
//  StateMachine.swift
//  hexabattle
//
//  Created by Jonathan Psenicka on 13.05.21.
//

import Foundation
import GameplayKit

class StateMachine: GKStateMachine {

    static let instance = StateMachine()

    private init() {
        super.init( states: [
            PreGameState(), ReinforcementState(), CombatState(), GameOverState(), StartMenuState(), TransitionState(), OtherPlayerTurnState()
        ])
    }

}
