//
// Created by Marcel Henning on 16.05.21.
//

import Foundation
import GameplayKit
import os.log

class CombatFightTroopsReservedState: GKState, AbstractGameState {

    override func didEnter(from previousState: GKState?) {
        os_log("Entering CombatFightTroopsReservedState", type: .info)
    }

    func touchTile(touchedNodeName: String) {
        if !TileBehavior.instance.isPlayerTile(MapManager.instance.tileDict[TileBehavior.instance.getRootName(touchedNodeName)] ?? TileEntity()) &&
                   !TileBehavior.instance.isNeighbor(firstEntity: TileTouchBehavior.previousTouchedTile, secondEntity: TileTouchBehavior.touchedTile) {
            GameStateMachine.instance.enter(CombatIdleState.self)
        }
    }

    func setTroops(touchedNodeName: String) {
        if touchedNodeName == Names.entityMoveFight + ".plusShape" {
            TileBehavior.instance.reserveFightMoveTroops(sourceTile: TileTouchBehavior.previousTouchedTile, destinationTile: TileTouchBehavior.touchedTile, amount: 1)
        }

        if touchedNodeName == Names.entityMoveFight + ".minusShape" {
            TileBehavior.instance.reserveFightMoveTroops(sourceTile: TileTouchBehavior.previousTouchedTile, destinationTile: TileTouchBehavior.touchedTile, amount: -1)
            if TroopBehavior.instance.getPlacedTroops() < 1 {
                GameStateMachine.instance.enter(CombatFightTileTouchedState.self)
            }
        }

        if touchedNodeName == Names.buttonMoveFightConfirm {

            if FeatureFlags.singlePlayer {
                GameStateMachine.instance.enter(CombatIdleState.self)
            }
            StateMachine.instance.enter(OtherPlayerTurnState.self)
        }

    }

    func finishCombat() {
        TroopBehavior.instance.fight(sourceTile: TileTouchBehavior.previousTouchedTile, destinationTile: TileTouchBehavior.touchedTile)
        GameCenterManager.instance.finishLocalCombatWithFight()
    }

    override func willExit(to nextState: GKState) {
        SoundManager.instance.playSoundEffect(soundName: Names.soundFight, volumeLevel: GameValues.volumeBattle)
    }

    func setAllTroops(touchedNodeName: String) {
        if touchedNodeName == Names.entityMoveFight + ".plusShape" && !TileBehavior.instance.isWater(TileTouchBehavior.touchedTile) {
            TileBehavior.instance.reserveAllAvailableFightMoveTroops(sourceTile: TileTouchBehavior.previousTouchedTile, destinationTile: TileTouchBehavior.touchedTile)
            GameStateMachine.instance.enter(CombatFightTroopsReservedState.self)
        }
    }
}
