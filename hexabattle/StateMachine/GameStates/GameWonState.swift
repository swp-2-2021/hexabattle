//
// Created by Marvin Klein on 25.05.21.
//

import GameplayKit
import os.log

class GameWonState: GKState, AbstractGameState {

    override func didEnter(from previousState: GKState?) {
        os_log("Entering GameWonState", type: .info)
        EntityManager.instance.addBundle(bundle: GameOverBundle(bundleName: Names.gameOverBundle, text: "YOU WON"))
    }

    override func willExit(to nextState: GKState) {
    }
}
