//
// Created by Marcel Henning on 15.05.21.
//

import Foundation
import GameplayKit
import os.log

class CombatIdleState: GKState, AbstractGameState {

    override func didEnter(from previousState: GKState?) {
        os_log("Entering CombatIdleState", type: .info)
        if let arrow = EntityManager.instance.entityDict[Names.arrow] {
            EntityManager.instance.remove(arrow)
        }
        GameBehavior.instance.removeReinforcementUI()
        GameBehavior.instance.initCombatUI()
        TileBehavior.instance.removeAllHighlighting()
        EntityManager.instance.updateLabel(
                labelName: Names.buttonMoveFightConfirm,
                newText: "Skip Combat"
        )
    }

    func touchTile(touchedNodeName: String) {
        TileTouchBehavior.previousTouchedTile = TileTouchBehavior.touchedTile
        TileTouchBehavior.touchedTile = MapManager.instance.tileDict[TileBehavior.instance.getRootName(touchedNodeName)] ?? TileEntity()
        if TileBehavior.instance.isPlayerTile(TileTouchBehavior.touchedTile) &&
                   TileTouchBehavior.touchedTile.component(ofType: TroopCountComponent.self)!.currentTroops > 1 {
            GameStateMachine.instance.enter(CombatTileTouchedState.self)
        }
    }

    func setTroops(touchedNodeName: String) {
        if touchedNodeName == Names.buttonMoveFightConfirm {
            if FeatureFlags.singlePlayer {
                GameStateMachine.instance.enter(CombatIdleState.self)
            }
            StateMachine.instance.enter(OtherPlayerTurnState.self)
        }
    }

}
