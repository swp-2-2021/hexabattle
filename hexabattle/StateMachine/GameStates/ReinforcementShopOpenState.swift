//
// Created by Marcel Henning on 14.05.21.
//

import Foundation
import GameplayKit
import os.log

class ReinforcementShopOpenState: GKState, AbstractGameState {

    override func didEnter(from previousState: GKState?) {
        os_log("Entering ShopOpenState", type: .info)
        EntityManager.instance.bundles[Names.bundleShop]?.hideBundle(hide: false)
        GameBehavior.instance.enablePanning(value: false)
    }

    override func willExit(to nextState: GKState) {
        EntityManager.instance.bundles[Names.bundleShop]?.hideBundle(hide: true)
        // TODO this variables should not be in the EntityManager
        EntityManager.instance.troopsAmount = 1
        EntityManager.instance.updateLabel(labelName: Names.labelTroopsPriceTag, newText: String(EntityManager.instance.troopsAmount) + " x " + String(GameValues.priceRecruitTroop))
        EntityManager.instance.updateLabel(labelName: Names.labelTotalTroopPrice, newText: "Total: " + String(EntityManager.instance.troopsAmount * GameValues.priceRecruitTroop))
        GameBehavior.instance.enablePanning(value: true)
    }

    func buyMarketUpgrade() {
        if GoldBehavior.instance.getGoldGeneration() < GameValues.maxGoldGeneration {
            let transactionSucceeded: Bool = GoldBehavior.instance.deductGold(amount: GameValues.priceMarketUpgrade)
            if (transactionSucceeded) {
                SoundManager.instance.playSoundEffect(soundName: Names.soundCoin, volumeLevel: GameValues.volumeCoin)
                GoldBehavior.instance.increaseGoldGeneration(amount: GameValues.goldIncreasement)
                EntityManager.instance.updateLabel(
                        labelName: Names.labelMarketImpact,
                        newText: "Gold Generation Per Turn: " + String(GoldBehavior.instance.getGoldGeneration())
                                + " → " + String(GoldBehavior.instance.getGoldGeneration() + GameValues.goldIncreasement))
            } else {
                EntityManager.instance.updateLabel(labelName: Names.labelTransactionStatus, newText: String("You don't have enough gold to make the purchase"), newColor: UIColor.hexaDarkRed)
            }

        }
    }
}
