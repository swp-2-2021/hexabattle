//
// Created by Marcel Henning on 14.05.21.
//

import Foundation
import GameplayKit
import os.log

class ReinforcementTileTouchedState: GKState, AbstractGameState {
    override func didEnter(from previousState: GKState?) {
        os_log("Entering ReinforcementTileTouchedState", type: .info)
    }

    func touchTile(touchedNodeName: String) {
        TileTouchBehavior.previousTouchedTile = TileTouchBehavior.touchedTile
        TileTouchBehavior.touchedTile = MapManager.instance.tileDict[TileBehavior.instance.getRootName(touchedNodeName)] ?? TileEntity()
        if !TileBehavior.instance.isPlayerTile(MapManager.instance.tileDict[TileBehavior.instance.getRootName(touchedNodeName)] ?? TileEntity()) {
            GameStateMachine.instance.enter(ReinforcementIdleState.self)
        } else {
            TileBehavior.instance.highlightTouchedTile(touchedNodeName: touchedNodeName)
        }

    }

    func setTroops(touchedNodeName: String) {
        if touchedNodeName == Names.changeTroops + ".plusShape" {
            TileBehavior.instance.reserveTroops(tile: TileTouchBehavior.touchedTile, amount: 1)
            GameStateMachine.instance.enter(ReinforcementTroopsAddedState.self)
        }
    }

    func setAllTroops(touchedNodeName: String) {
        if touchedNodeName == Names.changeTroops + ".plusShape" {
            TileBehavior.instance.reserveTroops(tile: TileTouchBehavior.touchedTile, amount: TroopBehavior.instance.getAvailableTroops())
            GameStateMachine.instance.enter(ReinforcementTroopsAddedState.self)
        }
    }

}
