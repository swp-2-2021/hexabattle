//
// Created by Marcel Henning on 15.05.21.
//

import Foundation
import GameplayKit
import os.log

class ReinforcementTroopsAddedState: GKState, AbstractGameState {
    override func didEnter(from previousState: GKState?) {
        EntityManager.instance.updateSprite(spriteName: Names.troopsConfirm, newImageName: "ButtonBlack")
        os_log("Entering ReinforcementTroopsAddedState", type: .info)
    }

    func touchTile(touchedNodeName: String) {
        TileTouchBehavior.previousTouchedTile = TileTouchBehavior.touchedTile
        TileTouchBehavior.touchedTile = MapManager.instance.tileDict[TileBehavior.instance.getRootName(touchedNodeName)] ?? TileEntity()
        if TileBehavior.instance.isPlayerTile(MapManager.instance.tileDict[TileBehavior.instance.getRootName(touchedNodeName)] ?? TileEntity()) {
            TileBehavior.instance.highlightTouchedTile(touchedNodeName: touchedNodeName)
        }
    }

    func setTroops(touchedNodeName: String) {
        if touchedNodeName == Names.changeTroops + ".plusShape" {
            TileBehavior.instance.reserveTroops(tile: TileTouchBehavior.touchedTile, amount: 1)
        }

        if touchedNodeName == Names.changeTroops + ".minusShape" {
            TileBehavior.instance.removeReservedTroops(tile: TileTouchBehavior.touchedTile, amount: 1)
            if TroopBehavior.instance.getPlacedTroops() < 1 {
                GameStateMachine.instance.enter(ReinforcementTileTouchedState.self)
            }
        }
        if touchedNodeName == Names.troopsConfirm {
            EntityManager.instance.updateSprite(spriteName: Names.troopsConfirm, newImageName: "ButtonGrey")
            TroopBehavior.instance.addAvailableTroops(count: -TroopBehavior.instance.getPlacedTroops())
            TileBehavior.instance.placeAddedTroops()
            GameStateMachine.instance.enter(ReinforcementTileTouchedState.self)
        }
    }

    func setAllTroops(touchedNodeName: String) {
        if touchedNodeName == Names.changeTroops + ".plusShape" {
            TileBehavior.instance.reserveTroops(tile: TileTouchBehavior.touchedTile, amount: TroopBehavior.instance.getAvailableTroops() - TroopBehavior.instance.getPlacedTroops())
        }

        if touchedNodeName == Names.changeTroops + ".minusShape" {
            TileBehavior.instance.removeReservedTroops(tile: TileTouchBehavior.touchedTile, amount: TroopBehavior.instance.getPlacedTroops())
            GameStateMachine.instance.enter(ReinforcementTileTouchedState.self)
        }
    }

    override func willExit(to nextState: GKState) {

    }
}
