//
// Created by Marcel Henning on 16.05.21.
//

import Foundation
import GameplayKit
import os.log

class CombatMoveTileTouchedState: GKState, AbstractGameState {

    override func didEnter(from previousState: GKState?) {
        if let arrow = EntityManager.instance.entityDict[Names.arrow] {
            EntityManager.instance.remove(arrow)
        }
        let arrow = ArrowEntity(name: Names.arrow, sourceTile: TileTouchBehavior.previousTouchedTile, destinationTile: TileTouchBehavior.touchedTile)
        EntityManager.instance.addToScene(arrow)

        TileBehavior.instance.highlightNeighbor(touchedNodeName: TileTouchBehavior.previousTouchedTile.name)
        TileBehavior.instance.highlightMoveCombatTouchedTile(entity: TileTouchBehavior.touchedTile)
        EntityManager.instance.updateLabel(
                labelName: Names.buttonMoveFightConfirm,
                newText: "Move"
        )
        os_log("Entering CombatMoveTileTouchedState", type: .info)

    }

    func touchTile(touchedNodeName: String) {
        if MapManager.instance.tileDict[TileBehavior.instance.getRootName(touchedNodeName)] ?? TileEntity() != TileTouchBehavior.previousTouchedTile {
            TileTouchBehavior.touchedTile = MapManager.instance.tileDict[TileBehavior.instance.getRootName(touchedNodeName)] ?? TileEntity()
        }
        if TileBehavior.instance.isPlayerTile(TileTouchBehavior.touchedTile) {
            GameStateMachine.instance.enter(CombatMoveTileTouchedState.self)
        } else if TileBehavior.instance.isNeighbor(firstEntity: TileTouchBehavior.previousTouchedTile, secondEntity: TileTouchBehavior.touchedTile) {
            GameStateMachine.instance.enter(CombatFightTileTouchedState.self)
        } else {
            GameStateMachine.instance.enter(CombatIdleState.self)
        }
    }

    func setTroops(touchedNodeName: String) {
        if touchedNodeName == Names.entityMoveFight + ".plusShape" {
            TileBehavior.instance.reserveFightMoveTroops(sourceTile: TileTouchBehavior.previousTouchedTile, destinationTile: TileTouchBehavior.touchedTile, amount: 1)
            GameStateMachine.instance.enter(CombatMoveTroopsReservedState.self)
        }
    }

    func setAllTroops(touchedNodeName: String) {
        if touchedNodeName == Names.entityMoveFight + ".plusShape" && !TileBehavior.instance.isWater(TileTouchBehavior.touchedTile) {
            TileBehavior.instance.reserveAllAvailableFightMoveTroops(sourceTile: TileTouchBehavior.previousTouchedTile, destinationTile: TileTouchBehavior.touchedTile)
            GameStateMachine.instance.enter(CombatMoveTroopsReservedState.self)
        }
    }
}
