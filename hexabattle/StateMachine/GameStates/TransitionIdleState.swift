//
//  TransitionIdleState.swift
//  hexabattle
//
//  Created by Dejan Mijic on 05.06.21.
//

import GameplayKit
import os.log

class TransitionIdleState: GKState, AbstractGameState {

    override func didEnter(from previousState: GKState?) {
        GameBehavior.instance.removeCombatUI()
        GameBehavior.instance.removeReinforcementUI()
        os_log("Entering TransitionIdleState", type: .info)
    }
}
