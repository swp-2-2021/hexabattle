//
// Created by Marcel Henning on 14.05.21.
//

import Foundation
import GameplayKit
import os.log

protocol AbstractGameState: GKState {
    func buyTroops()
    func buyMarketUpgrade()
    func openOptions()
    func closeOptions()
    func openInGameHelp()
    func closeInGameHelp()
    func leaveGame()
    func touchTile(touchedNodeName: String)
    func setTroops(touchedNodeName: String)
    func setAllTroops(touchedNodeName: String)
    func moveTroops(touchedNodeName: String)
    func finishCombat()
}

extension AbstractGameState {
    func moveTroops(touchedNodeName: String) {
    }

    func buyTroops() {
    }

    func buyMarketUpgrade() {
    }

    func openOptions() {
    }

    func closeOptions() {
    }

    func openInGameHelp() {
    }

    func closeInGameHelp() {
    }

    /*
     Should be called if player leaves the game
     */
    func leaveGame() {
        GameCenterManager.instance.leaveGame()
        GameBehavior.instance.reset()
        for view in EntityManager.instance.view.subviews {
            view.removeFromSuperview()
        }
    }

    func touchTile(touchedNodeName: String) {
        TileTouchBehavior.previousTouchedTile = TileTouchBehavior.touchedTile
        TileTouchBehavior.touchedTile = MapManager.instance.tileDict[TileBehavior.instance.getRootName(touchedNodeName)] ?? TileEntity()
    }

    func finishCombat() {
        GameCenterMatchManager().finishCombatWithNone()

    }
    func setTroops(touchedNodeName: String) {}
    func setAllTroops(touchedNodeName: String) {}
}
