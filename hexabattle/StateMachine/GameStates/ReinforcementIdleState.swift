//
// Created by Marcel Henning on 14.05.21.
//

import Foundation
import GameplayKit
import os.log

class ReinforcementIdleState: GKState, AbstractGameState {

    override func didEnter(from previousState: GKState?) {
        os_log("Entering ReinforcementIdleState", type: .info)
        TileBehavior.instance.highlightPlayerTilesOnly(true)
        GameBehavior.instance.initReinforcementUI()
    }

    func touchTile(touchedNodeName: String) {
        TileTouchBehavior.previousTouchedTile = TileTouchBehavior.touchedTile
        TileTouchBehavior.touchedTile = MapManager.instance.tileDict[TileBehavior.instance.getRootName(touchedNodeName)] ?? TileEntity()
        if TileBehavior.instance.isPlayerTile(MapManager.instance.tileDict[TileBehavior.instance.getRootName(touchedNodeName)] ?? TileEntity()) {
            TileBehavior.instance.highlightTouchedTile(touchedNodeName: touchedNodeName)
            GameStateMachine.instance.enter(ReinforcementTileTouchedState.self)
        }
    }
}
