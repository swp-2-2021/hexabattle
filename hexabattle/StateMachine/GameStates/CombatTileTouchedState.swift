//
// Created by Marcel Henning on 16.05.21.
//

import Foundation
import GameplayKit
import os.log

class CombatTileTouchedState: GKState, AbstractGameState {

    override func didEnter(from previousState: GKState?) {
        os_log("Entering CombatTileTouchedState", type: .info)
        TileBehavior.instance.highlightPlayerTilesOnly(true)
        TileBehavior.instance.highlightTouchedTile(touchedNodeName: TileTouchBehavior.touchedTile.name)
        TileBehavior.instance.highlightNeighbor(touchedNodeName: TileTouchBehavior.touchedTile.name)

    }

    func touchTile(touchedNodeName: String) {
        TileTouchBehavior.previousTouchedTile = TileTouchBehavior.touchedTile
        TileTouchBehavior.touchedTile = MapManager.instance.tileDict[TileBehavior.instance.getRootName(touchedNodeName)] ?? TileEntity()
        if TileBehavior.instance.isPlayerTile(MapManager.instance.tileDict[TileBehavior.instance.getRootName(touchedNodeName)] ?? TileEntity()) &&
                   TileTouchBehavior.touchedTile != TileTouchBehavior.previousTouchedTile {
            GameStateMachine.instance.enter(CombatMoveTileTouchedState.self)
        } else if TileTouchBehavior.touchedTile == TileTouchBehavior.previousTouchedTile {
        } else if !TileBehavior.instance.isWater(TileTouchBehavior.touchedTile)
                          && TileBehavior.instance.isNeighbor(firstEntity: TileTouchBehavior.previousTouchedTile, secondEntity: TileTouchBehavior.touchedTile) {
            GameStateMachine.instance.enter(CombatFightTileTouchedState.self)
        } else {
            GameStateMachine.instance.enter(CombatIdleState.self)
        }
    }
}
