//
// Created by Marvin Klein on 25.05.21.
//

import GameplayKit
import os.log

class GameLostState: GKState, AbstractGameState {

    override func didEnter(from previousState: GKState?) {
        os_log("Entering GameLostState", type: .info)
        EntityManager.instance.addBundle(bundle: GameOverBundle(bundleName: Names.gameOverBundle, text: "YOU LOST"))
        (GameStateMachine.instance.currentState as! AbstractGameState).leaveGame()
    }
}
