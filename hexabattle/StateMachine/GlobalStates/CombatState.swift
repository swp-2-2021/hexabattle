//
//  CombatState.swift
//  hexabattle
//
//  Created by Jonathan Psenicka on 22.04.21.
//

import UIKit
import GameplayKit
import os.log

class CombatState: GKState {

    override func isValidNextState(_ stateClass: AnyClass) -> Bool {
        switch stateClass {
        case is OtherPlayerTurnState.Type:
            return true
        case is StartMenuState.Type:
            return true
        case is GameOverState.Type:
            return true
        default:
            return false
        }
    }

    override func didEnter(from previousState: GKState?) {
        os_log("entering combatstate", type: .info)

        PhaseTimer.instance.startCombatTimer()

        TileBehavior.instance.removeAllHighlighting()

        GameStateMachine.instance.enter(CombatIdleState.self)
    }

    override func updateLabelOnSecondTick() {
        EntityManager.instance.updateLabel(labelName: Names.labelPhase,
                newText: "Combat: \(PhaseTimer.instance.secondsCounter) s")
    }

    override func timerExpired() {
            if !GameBehavior.instance.isGameOver() {
                StateMachine.instance.enter(OtherPlayerTurnState.self)
            }
    }

    override func enterBaseGameState() {
        GameStateMachine.instance.enter(CombatIdleState.self)
    }

    override func willExit(to nextState: GKState) {
        if let arrow = EntityManager.instance.entityDict[Names.arrow] {
            EntityManager.instance.remove(arrow)
        }

        PhaseTimer.instance.stopTimer()
        if (!nextState.isKind(of: GameOverState.self) && !nextState.isKind(of: StartMenuState.self)) {
            (GameStateMachine.instance.currentState as! AbstractGameState).finishCombat()
        }
    }
}
