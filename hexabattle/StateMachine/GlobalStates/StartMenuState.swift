//
// Created by Marvin Klein on 27.05.21.
//

import UIKit
import SpriteKit
import GameplayKit
import os.log

class StartMenuState: GKState {

    override func didEnter(from previousState: GKState?) {
        os_log("Entering StartMenuState", type: .info)
        EntityManager.instance.view.presentScene(StartScene(), transition: SKTransition.fade(withDuration: 1))
    }

}
