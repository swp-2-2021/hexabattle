//
//  TransitionState.swift
//  hexabattle
//
//  Created by Dejan Mijic on 05.06.21.
//

import UIKit
import SpriteKit
import GameplayKit
import os.log

class TransitionState: GKState {

    override func didEnter(from previousState: GKState?) {
        os_log("Entering TransitionState", type: .info)
        EntityManager.instance.updateLabel(
                labelName: Names.labelPhase,
                newText: "Waiting..."
        )
        GameStateMachine.instance.enter(TransitionIdleState.self)
        GameCenterManager.instance.finishLocalReinforcement()
    }
}
