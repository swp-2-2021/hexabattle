//
// Created by Marcel Henning on 09.06.21.
//

import UIKit
import SpriteKit
import GameKit
import GameplayKit
import os.log

class OtherPlayerTurnState: GKState {

    override func didEnter(from previousState: GKState?) {
        os_log("Entering OtherPlayerTurnState", type: .info)

        GameStateMachine.instance.enter(TransitionIdleState.self)

        TileBehavior.instance.removeAllHighlighting()

        InGameHelpManager.instance.enableOtherPlayerTurnHelp()

        EntityManager.instance.updateLabel(
                labelName: Names.labelPhase,
                newText: "Turn of other Player"
        )
    }

    override func willExit(to nextState: GKState) {
        InGameHelpManager.instance.disableOtherPlayerTurnHelp()
    }

}
