//
// Created by Marvin Klein on 27.05.21.
//

import UIKit
import SpriteKit
import GameplayKit

class GameOverState: GKState {

    override func didEnter(from previousState: GKState?) {
        if GameBehavior.instance.isGameWon() {
            GameStateMachine.instance.enter(GameWonState.self)
        } else if GameBehavior.instance.isGameLost() {
            GameStateMachine.instance.enter(GameLostState.self)
        }
    }
}
