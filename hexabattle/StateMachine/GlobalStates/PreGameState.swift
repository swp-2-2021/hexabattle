//
// Created by student on 01.05.21.
//

import Foundation
import GameKit
import os.log

let waitingScreen = LabelWithShapeEntity(
        position: CGPoint(x: 0, y: 0),
        backgroundZPosition: ZPosition.waitingScreen,
        labelZPosition: ZPosition.waitingScreen,
        size: CGSize(width: Size.sceneSize.width, height: Size.sceneSize.height),
        text: "waiting for other players",
        labelColor: UIColor.hexaDarkBlue,
        backgroundColor: UIColor.lightGray,
        name: Names.waitingScreenWithLabel)

/**
 State before the game start.
 Some one time jobs are done here like determining the host and player order.
 */
class PreGameState: GKState {

    override func didEnter(from previousState: GKState?) {
        os_log("Entering PreGameState", type: .info)

        EntityManager.instance.addToCamera(waitingScreen)
        EntityManager.instance.view.presentScene(GameScene(), transition: SKTransition.fade(withDuration: 1))
        sleep(1)
        if (FeatureFlags.singlePlayer) {
            GameBehavior.instance.initGame([GKLocalPlayer.local.playerID])
            StateMachine.instance.enter(ReinforcementState.self)
        }

        GameCenterManager.instance.determineHost()
        if GameCenterManager.instance.isHost {
            GameCenterMatchManager().determinePlayerOrder(GameCenterManager.instance.currentMatch!)
        }
    }

    override func willExit(to nextState: GKState) {
        EntityManager.instance.remove(waitingScreen)
    }

    override func enterBaseGameState() {
        if (GameStateMachine.instance.canEnterState(ReinforcementIdleState.self)) {
            GameStateMachine.instance.enter(ReinforcementIdleState.self)
        } else if (GameStateMachine.instance.canEnterState(CombatIdleState.self)) {
            GameStateMachine.instance.enter(CombatIdleState.self)
        }
    }
}
