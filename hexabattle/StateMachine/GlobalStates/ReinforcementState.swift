//
//  ReinforcementState.swift
//  hexabattle
//
//  Created by Jonathan Psenicka on 22.04.21.
//

import UIKit
import SpriteKit
import GameplayKit
import os.log

class ReinforcementState: GKState {

    override func isValidNextState(_ stateClass: AnyClass) -> Bool {
        switch stateClass {
        case is CombatState.Type:
            return true
        case is GameOverState.Type:
            return true
        case is TransitionState.Type:
            return true
        case is StartMenuState.Type:
            return true
        default:
            return false
        }
    }
    func toString() -> String {
        State.reinforcement.rawValue
    }

    override func didEnter(from previousState: GKState?) {
        os_log("Entering ReinforcementState", type: .info)

        GoldBehavior.instance.reinforcementGold()

        PhaseTimer.instance.startReinforcementTimer()

        TileBehavior.instance.removeAllHighlighting()

        GameStateMachine.instance.enter(ReinforcementIdleState.self)
    }

    override func updateLabelOnSecondTick() {
        EntityManager.instance.updateLabel(labelName: Names.labelPhase,
                newText: "Reinforcement: \(PhaseTimer.instance.secondsCounter) s")
    }

    override func timerExpired() {
        StateMachine.instance.enter(TransitionState.self)

    }

    override func enterBaseGameState() {
        GameStateMachine.instance.enter(ReinforcementIdleState.self)
    }

    override func willExit(to nextState: GKState) {
        PhaseTimer.instance.stopTimer()

        // TODO: !nextState.isKind(of: StartMenuState.self) is only a workaround until real problem is found
        // Remove all troops that are not set
        if !nextState.isKind(of: StartMenuState.self)
                   && !nextState.isKind(of: GameOverState.self) {
            TroopBehavior.instance.addAvailableTroops(count: -TroopBehavior.instance.getPlacedTroops())
            TileBehavior.instance.placeAddedTroops()
            // GameCenterManager.instance.finishLocalReinforcement()
        }
    }
}
