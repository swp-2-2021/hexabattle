//
// Created by Marcel Henning on 14.05.21.
//

import Foundation
import GameplayKit

class GameStateMachine: GKStateMachine {

    static let instance = GameStateMachine()

    private init() {
        super.init( states: [
            ReinforcementShopOpenState(),
            ReinforcementTileTouchedState(),
            ReinforcementIdleState(),
            ReinforcementTroopsAddedState(),
            CombatIdleState(),
            CombatTileTouchedState(),
            CombatMoveTileTouchedState(),
            CombatMoveTroopsReservedState(),
            CombatFightTileTouchedState(),
            CombatFightTroopsReservedState(),
            GameWonState(),
            GameLostState(),
            TransitionIdleState()
        ])
    }

}
