//
// Created by student on 01.05.21.
//

import Foundation
import GameKit

extension GKState {

    @objc func enterBaseGameState() {
    }

    @objc func updateLabelOnSecondTick() {
    }

    @objc func timerExpired() {
    }

}
