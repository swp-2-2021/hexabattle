//
//  MapManager.swift
//  hexabattle
//
//  Created by Marvin Klein on 08.04.21.
//

import Foundation
import SpriteKit
import GameplayKit

class MapManager {
    static var instance = MapManager()
    var tileDict: [String: TileEntity] = [:]

    var scene: SKScene

    var generateMapMatrix: [[Int]] = []

    var toRemove: [String: TileEntity] = [:]

    enum MapType {
        case random
        case constant
    }

    func setMapforValueOfPlayers(Players: Int) {
        switch Players {
        case 2:
            generateMapMatrix = constantMapMatrix2Players
        case 3:
            generateMapMatrix = constantMapMatrix3Players
        case 4:
            generateMapMatrix = constantMapMatrix4Players
        default:
            generateMapMatrix = constantMapMatrix4Players
        }
    }

    private func checkCityDistance(pos1: [Int], pos2: [Int], distance: Double) -> Bool {
        return sqrt(pow(Double(pos1[0] - pos2[0]), 2) + pow(Double(pos1[1] - pos2[1]), 2)) >= distance
    }

    func point(x: CGFloat, y: CGFloat) -> CGPoint {
        CGPoint(x: (x - Size.sceneSize.width / 2), y: (y - Size.sceneSize.height / 2))
    }

    func generateRandomMap() {
        let mapRows = generateMapMatrix.count
        let mapColumn = generateMapMatrix[0].count
        var setRow = 0
        var setColumn = 0
        var distanceToNextCityOk = true
        var positionCityPlaced: [[Int]] = []
        var playersCityPlaced = 0
        let minDistanceBetweenCitys = 5    // Can be modified by an input (needs to be implemented)
        var numberOfPlayers = 4     // Can be modified by an input  (need to be implemented)

        // place City on the map fields
        while playersCityPlaced < numberOfPlayers {
            setRow = Int.random(in: 1..<mapRows)
            setColumn = Int.random(in: 1..<mapColumn)

            // check the distance to all actually setted citys
            if playersCityPlaced != 0 {
                for check in 0..<playersCityPlaced {
                    distanceToNextCityOk = checkCityDistance(pos1: [setRow, setColumn], pos2: positionCityPlaced[check], distance: Double(minDistanceBetweenCitys))
                    if distanceToNextCityOk == false {
                        break
                    }
                }
            }

            // set city if the field is a placeholder and the distance to all actually setted citys is correct
            if generateMapMatrix[setRow][setColumn] == TileType.placeholder.rawValue && distanceToNextCityOk {
                generateMapMatrix[setRow][setColumn] = 1
                playersCityPlaced += 1

                var cityFields = [Int]()
                cityFields.append(setRow)
                cityFields.append(setColumn)

                positionCityPlaced.append(cityFields)
            }
        }

        // place random fields on the map
        for rowX in 0..<mapRows {
            for columnY in 0..<mapColumn where generateMapMatrix[rowX][columnY] == TileType.placeholder.rawValue {
                generateMapMatrix[rowX][columnY] = Int.random(in: 2...4)
            }
        }
    }

    // Half the size of TileEntity textures
    var size: CGFloat = 64

    private init() {
        scene = SKScene.init()
    }

    func setScene(scene: SKScene) {
        self.scene = scene
    }

    private func renderTile(_ tile: TileEntity) {
        if let spriteNodes = tile.component(ofType: SpriteComponent.self)?.nodes {
            for spriteNode in spriteNodes {
                scene.addChild(spriteNode)
            }
        }
        if let shapeNodes = tile.component(ofType: ShapeComponent.self)?.nodes {
            for shapeNode in shapeNodes {
                scene.addChild(shapeNode)
            }
        }
        if let labelNodes = tile.component(ofType: LabelComponent.self)?.nodes {
            for labelNode in labelNodes {
                scene.addChild(labelNode)
            }
        }
        EntityManager.instance.labelSystem.addComponent(foundIn: tile)
    }

    /*
     Renders Map from left top to right bottom. Values for Tiledict: row-column
     */
    func renderMap() {
        var mapSelect = MapType.constant // Can be modified by

        if mapSelect == MapType.random {
            generateMapMatrix = randomMapMatrix4Players
            generateRandomMap()
        }

        for row in 0...generateMapMatrix.count - 1 {
            for column in 0...generateMapMatrix[row].count - 1 {
                let realRow = generateMapMatrix.count - 1 - row
                let pos = offset2Pixel(hexRow: realRow, hexCol: column)
                let value = generateMapMatrix[row][column]
                let tileType = TileType(rawValue: value)
                let name = String(realRow) + "-" + String(column)
                let tile = TileEntity(
                        tileType: tileType!,
                        position: pos,
                        name: name,
                        column: column,
                        row: realRow)
                tileDict[name] = tile
                renderTile(tile)
            }
        }
    }

    func initCities(playerCount: Int) {
        var index = 1
        for row in 0...generateMapMatrix.count - 1 {
            for column in 0...generateMapMatrix[row].count - 1 {
                let key = String(row) + "-" + String(column)
                let entity = tileDict[key]
                if entity?.component(ofType: CityTileComponent.self) != nil {
                    occupyTile(tileName: key, newPlayer: Player(rawValue: index)!, newTroopsAmount: GameValues.startTroops)
                    index += 1

                    if index > playerCount {
                        return
                    }
                }
            }
        }
    }

    /**
     Occupy a Tile and updates Label and Components.
     - Parameters:
       - tileName: name of the tile
       - newPlayer:
       - newTroopsAmount:
     */
    func occupyTile(tileName: String, newPlayer: Player, newTroopsAmount: Int = 0) {
        if let playerComponent = tileDict[tileName]?.component(ofType: PlayerComponent.self) {
            playerComponent.player = newPlayer
            EntityManager.instance.updateLabel(labelName: tileName, newText: String(newTroopsAmount))
        }
        if let troopsCountComponent = tileDict[tileName]?.component(ofType: TroopCountComponent.self) {
            troopsCountComponent.currentTroops = newTroopsAmount
        }
    }

    func mockupGame() {
        for row in 4...7 {
            for column in 4...7 {
                let name = String(row) + "-" + String(column)
                occupyTile(tileName: name, newPlayer: EntityManager.instance.player, newTroopsAmount: 1)
            }

        }
    }

    /**
     Calculates the positions auf every hex. Depends on the hight of the texture.
     */
    func offset2Pixel(hexRow: Int, hexCol: Int) -> CGPoint {
        var xPosition = size * sqrt(3)
        xPosition = xPosition * (CGFloat(hexCol) + 0.5 * CGFloat((hexRow & 1)))
        let yPosition = size * 1.5 * CGFloat(hexRow)
        return CGPoint(x: xPosition, y: yPosition)
    }

    /**
     Calculates the the GridPosition of an HexTile for a given point on the screen
     */
    func pixel2HexGrid(point: CGPoint) -> String {
        var q = (sqrt(3) / 3.0 * point.x - 1 / 3 * point.y)
        q = q / size
        let r = (CGFloat(2.0 / 3.0) * point.y) / size
        let roundedCube = cubeRound(x: q, y: CGFloat(-q) - r, z: r)
        // cube to offsett
        let col = roundedCube[0] + (roundedCube[2] - CGFloat(Int(roundedCube[2]) & 1)) / 2
        let row = roundedCube[2]
        return "\(Int(row))-\(Int(col))"
    }

    // Rounds the values of a cube
    private func cubeRound(x: CGFloat, y: CGFloat, z: CGFloat) -> [CGFloat] {
        var rx = round(x)
        var ry = round(y)
        var rz = round(z)

        let x_diff = abs(rx - x)
        let y_diff = abs(ry - y)
        let z_diff = abs(rz - z)

        if x_diff > y_diff && x_diff > z_diff {
            rx = -ry - rz
        } else if y_diff > z_diff {
            ry = -rx - rz
        } else {
            rz = -rx - ry
        }
        return [rx, ry, rz]
    }

    func changeTileColor(name: String, color: UIColor) {
        let entity = tileDict[name]
        if let shapeNodes = entity?.component(ofType: ShapeComponent.self)?.nodes {
            for shapeNode in shapeNodes {
                shapeNode.fillColor = color
            }
        }
    }

    func didTap(touchedNodeName: String, touchedLocation: CGPoint) {
        if checkIfTileIsTouched(touchedNodeName: touchedNodeName) {
            let touchedEntity = getRealTouchedEntity(touchedLocation)
            if let touchedComponent = touchedEntity.component(ofType: TouchComponent.self) {
                touchedComponent.didTap(touchedNodeName: touchedEntity.name)
            }
        }
    }

    /**
    Get the real touched Entity.
     (Touched Tile could be a different one because of the overlay)
     */
    private func getRealTouchedEntity(_ touchedLocation: CGPoint) -> GKEntity {
        let touchedNodeName = pixel2HexGrid(point: touchedLocation)
        return tileDict[touchedNodeName] ?? GKEntity()
    }

    /**
     Check if the touchedNode is a Tile
     (Then the real position can be calculated)
     */
    private func checkIfTileIsTouched(touchedNodeName: String) -> Bool {
        let touchedName = touchedNodeName.split(separator: ".")
        let touchedEntity = tileDict[String(touchedName[0])]
        return touchedEntity != nil
    }
    func setCameraOnPositionOfTile(tile: TileEntity) {
        if let gridPositionComponent = tile.component(ofType: GridPositionComponent.self) {
            EntityManager.instance.camera.position = MapManager.instance.offset2Pixel(hexRow: gridPositionComponent.row, hexCol: gridPositionComponent.column)
        }
    }

    func update(_ deltaTime: CFTimeInterval) {
        for (_, currentRemove) in toRemove {
            if let spriteNodes = currentRemove.component(ofType: SpriteComponent.self)?.nodes {
                for spriteNode in spriteNodes {
                    spriteNode.removeFromParent()
                }
            }
            if let labelNodes = currentRemove.component(ofType: LabelComponent.self)?.nodes {
                for labelNode in labelNodes {
                    labelNode.removeFromParent()
                }
            }
            if let shapeNodes = currentRemove.component(ofType: ShapeComponent.self)?.nodes {
                for shapeNode in shapeNodes {
                    shapeNode.removeFromParent()
                }
            }
        }

        toRemove.removeAll()
    }

    func removeAll() {
        toRemove = tileDict
        tileDict.removeAll()
    }

    func reset() {
        removeAll()
        MapManager.instance = MapManager()
    }
}
