//
// Created by Marvin Klein on 17.06.21.
//

import SpriteKit
import AVFoundation
import os

class SoundManager {
    public static var instance = SoundManager()

    var audioPlayer = AVAudioPlayer()
    var effectPlayer = AVAudioPlayer()
    var isMusicEnabled = true

    func initBackgroundMusic() {
        guard let audioData = NSDataAsset(name: Names.soundBackground)?.data else {
            os_log("Unable to find asset %s", type: .error, Names.soundBackground)
            return
        }
        do {
            audioPlayer = try AVAudioPlayer(data: audioData)
        } catch {
            os_log("Background Music is broken", type: .error)
        }
        if let audioVolume = UserDefaults.standard.value(forKey: "audioVolume") as? Float {
            setAudioVolume(audioVolume)
        } else {
            setAudioVolume(GameValues.volumeMusic)
        }
        audioPlayer.numberOfLoops = -1
        audioPlayer.prepareToPlay()

        startBackgroundMusic()
    }

    func startBackgroundMusic() {
        audioPlayer.play()
    }

    func playSoundEffect(soundName: String, volumeLevel: Float) {
        guard let audioData = NSDataAsset(name: soundName)?.data else {
            os_log("Unable to find asset %s", type: .error, soundName)
            return
        }
        do {
            effectPlayer = try AVAudioPlayer(data: audioData)
        } catch {
            os_log("Could not load sound file %s", type: .error, soundName)
        }
        if let effectVolume = UserDefaults.standard.value(forKey: "effectVolume") as? Float {
            setFxVolume(volumeLevel * effectVolume)
        } else {
            setFxVolume(volumeLevel)
        }
        effectPlayer.play()

    }

    func stopBackgroundMusic() {
        audioPlayer.stop()
    }

    func setAudioVolume(_ volume: Float) {
        audioPlayer.volume = volume * 0.8
        UserDefaults.standard.set(audioPlayer.volume, forKey: "audioVolume")
    }

    func getAudioVolume() -> Float {
        audioPlayer.volume
    }

    func setFxVolume(_ volume: Float) {
        effectPlayer.volume = volume
        UserDefaults.standard.set(effectPlayer.volume, forKey: "effectVolume")
    }

    func getFxVolume() -> Float {
        effectPlayer.volume
    }
}
