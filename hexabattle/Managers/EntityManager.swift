//
//  EntityManager.swift
//  hexabattle
//
//  Created by Marvin Klein on 08.04.21.
//

import Foundation
import SpriteKit
import GameplayKit
import os.log

class EntityManager {

    static var instance = EntityManager()

    var entityDict: [String: GKEntity] = [:]

    var camera: SKCameraNode

    var player = Player.none {
        didSet {
            os_log("Local player is %s", type: .info, player.toString())
        }
    }

    var playerDict: [String: Player] = [:] {
        didSet {
            os_log("Player(s) set", type: .info)
        }
    }

    var scene: SKScene
    var view: SKView
    var toRemove = Set<GKEntity>()

    var bundles: [String: AbstractEntityBundle] = [:]

    let touchSystem = GKComponentSystem(componentClass: TouchComponent.self)
    let labelSystem = GKComponentSystem(componentClass: LabelComponent.self)
    let spriteSystem = GKComponentSystem(componentClass: SpriteComponent.self)

    // var for shop demonstration
    var troopsAmount = 1
    var defenseBonus = 100

    private init() {
        scene = SKScene.init()
        camera = SKCameraNode.init()
        view = SKView.init()
    }

    func setCamera(_ cam: SKCameraNode) {
        camera = cam
    }

    func setScene(scene: SKScene) {
        self.scene = scene
    }

    func addToScene(_ entity: GKEntity) {
        entityDict[entity.name] = entity

        if let spriteNodes = entity.component(ofType: SpriteComponent.self)?.nodes {
            for spriteNode in spriteNodes {
                scene.addChild(spriteNode)
            }
        }
        if let shapeNodes = entity.component(ofType: ShapeComponent.self)?.nodes {
            for shapeNode in shapeNodes {
                scene.addChild(shapeNode)
            }
        }
        if let labelNodes = entity.component(ofType: LabelComponent.self)?.nodes {
            for labelNode in labelNodes {
                scene.addChild(labelNode)
            }
        }
        touchSystem.addComponent(foundIn: entity)
        labelSystem.addComponent(foundIn: entity)
        spriteSystem.addComponent(foundIn: entity)
    }

    func addToCamera(_ entity: GKEntity) {
        entityDict[entity.name] = entity

        if let spriteNodes = entity.component(ofType: SpriteComponent.self)?.nodes {
            for spriteNode in spriteNodes {
                camera.addChild(spriteNode)
            }
        }
        if let shapeNodes = entity.component(ofType: ShapeComponent.self)?.nodes {
            for shapeNode in shapeNodes {
                camera.addChild(shapeNode)
            }
        }
        if let labelNodes = entity.component(ofType: LabelComponent.self)?.nodes {
            for labelNode in labelNodes {
                camera.addChild(labelNode)
            }
        }
        if let sliderUI = entity.component(ofType: SliderComponent.self)?.uiSlider {
            view.addSubview(sliderUI)
        }

        touchSystem.addComponent(foundIn: entity)
        labelSystem.addComponent(foundIn: entity)
        spriteSystem.addComponent(foundIn: entity)

    }

    func addBundle(bundle: AbstractEntityBundle) {
        for entity in bundle.entities {
            addToCamera(entity)
        }
        bundles[bundle.name] = bundle
    }

    func removeBundle(bundle: AbstractEntityBundle) {
        for entity in bundle.entities {
            remove(entity)
        }
        bundles.removeValue(forKey: bundle.name)
    }

    func addBundleToScene(bundle: AbstractEntityBundle) {
        for entity in bundle.entities {
            addToScene(entity)
        }
        bundles[bundle.name] = bundle
    }

    func update(_ deltaTime: CFTimeInterval) {
        for (_, entity) in entityDict {
            entity.update(deltaTime: deltaTime)
        }
        for currentRemove in toRemove {
            if let spriteNodes = currentRemove.component(ofType: SpriteComponent.self)?.nodes {
                for spriteNode in spriteNodes {
                    spriteNode.removeFromParent()
                }
            }
            if let labelNodes = currentRemove.component(ofType: LabelComponent.self)?.nodes {
                for labelNode in labelNodes {
                    labelNode.removeFromParent()
                }
            }
            if let shapeNodes = currentRemove.component(ofType: ShapeComponent.self)?.nodes {
                for shapeNode in shapeNodes {
                    shapeNode.removeFromParent()
                }
            }
            if let sliderUI = currentRemove.component(ofType: SliderComponent.self)?.uiSlider {
                sliderUI.removeFromSuperview()
            }

            touchSystem.removeComponent(foundIn: currentRemove)
            labelSystem.removeComponent(foundIn: currentRemove)
            spriteSystem.removeComponent(foundIn: currentRemove)
        }

        toRemove.removeAll()
    }

    /**
    Delegate the name of touched node to every TouchComponent
     */
    func didTap(touchedNodeName: String) {
        for components in touchSystem.components {
            if let touchComponent = components as? TouchComponent {
                touchComponent.didTap(touchedNodeName: touchedNodeName)
            }
        }
    }

    /**
     Changes the text (and the text color) of an label.

     - Parameters:
       - labelName: name of the label
       - newText: new text, that will be shown in the label
       - newColor: name of the new color in which the text should be displayed
     */
    func updateLabel(labelName: String, newText: String, newColor: UIColor? = nil) {
        for components in labelSystem.components {
            if let labelComponent = components as? LabelComponent {
                for labelNode in labelComponent.nodes where labelNode.name == labelName {
                    labelNode.text = newText
                    if newColor != nil {
                        labelNode.fontColor = newColor
                    }
                }
            }
        }
    }

    /**
 Changes the picture of a Sprite.

 - Parameters:
   - spriteName: name of the Sprite
   - newImage: name of the new Image, that should be shown
 */
    func updateSprite(spriteName: String, newImageName: String) {
        for components in spriteSystem.components {
            if let spriteComponent = components as? SpriteComponent {
                for spriteNode in spriteComponent.nodes where spriteNode.name == spriteName {
                    spriteNode.texture = SKTexture(imageNamed: newImageName)
                }
            }

        }

    }

    func getBundleByName(bundleName: String) -> AbstractEntityBundle? {
        bundles[bundleName]
    }

    func remove(_ entity: GKEntity) {
        entityDict.removeValue(forKey: entity.name)
        toRemove.insert(entity)
    }

    func removeAll() {
        for (_,entity) in entityDict {
            toRemove.insert(entity)
        }
        entityDict.removeAll()
    }

    func reset() {
        removeAll()
        EntityManager.instance = EntityManager()
    }

    func didHold(touchedNodeName: String) {
        for components in touchSystem.components {
            if let touchComponent = components as? TouchComponent {
                touchComponent.didHold(touchedNodeName: touchedNodeName)
            }
        }

    }
}
