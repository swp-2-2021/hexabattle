//
//  InGameHelpManager.swift
//  hexabattle
//
//  Created by 91elhe1bif on 21.06.21.
//

import SpriteKit
import os

class InGameHelpManager {
    public static var instance = InGameHelpManager()

    var isHelpEnabled = false

    private func setHelpText(text: String) {
        EntityManager.instance.updateLabel(
            labelName: Names.labelHelpText,
            newText: text
        )
    }

    func enableReinforcementHelp() {
        self.setHelpText(text: InGameHelp.reinforcementState)
        EntityManager.instance.bundles[Names.bundleInGameHelp]?.hideBundle(hide: !self.isHelpEnabled)
    }

    func disableReinforcementHelp() {
        EntityManager.instance.bundles[Names.bundleInGameHelp]?.hideBundle(hide: true)
        self.setHelpText(text: "")
    }

    func enableCombatHelp() {
        self.setHelpText(text: InGameHelp.combatState)
        EntityManager.instance.bundles[Names.bundleInGameHelp]?.hideBundle(hide: !self.isHelpEnabled)
    }

    func disableCombatHelp() {
        EntityManager.instance.bundles[Names.bundleInGameHelp]?.hideBundle(hide: true)
        self.setHelpText(text: "")
    }

    func enableOtherPlayerTurnHelp() {
        self.setHelpText(text: InGameHelp.otherPlayerTurnState)
        EntityManager.instance.bundles[Names.bundleInGameHelp]?.hideBundle(hide: !self.isHelpEnabled)
    }

    func disableOtherPlayerTurnHelp() {
        EntityManager.instance.bundles[Names.bundleInGameHelp]?.hideBundle(hide: true)
        self.setHelpText(text: "")
    }

}
