//
//  SliderEntity.swift
//  hexabattle
//
//

import SpriteKit
import GameplayKit

class SliderEntity: GKEntity {
    init(
            position: CGPoint,
            size: CGSize,
            minimumValue: Float,
            maximumValue: Float,
            initValue: Float?,
            step: CGFloat?,
            name: String,
            behavior: AbstractSliderBehavior
    ) {
        super.init()
        self.name = name
        addComponent(SliderComponent(
                slider: Slider(
                        name: name,
                        position: position,
                        size: size,
                        minimumValue: minimumValue,
                        maximumValue: maximumValue,
                        step: step,
                        initValue: initValue),
        behavior: behavior

        ))

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
