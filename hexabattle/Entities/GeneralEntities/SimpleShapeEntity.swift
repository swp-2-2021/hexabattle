//
//  Created by Martin Uhlig on 28.04.21.
//

import SpriteKit
import GameplayKit

/**
 Can only be used if the Entity doesn't have to hold data.
 */
class SimpleShapeEntity: GKEntity {

    init(
            fillColor: UIColor = UIColor.hexaLightBlue,
            size: CGSize,
            position: CGPoint,
            zPosition: CGFloat,
            name: String
    ) {
        super.init()
        self.name = name
        addComponent(ShapeComponent(
                shape: Shape(
                        shapeType: ShapeType.rectangle(Rectangle(size: size)),
                        name: name,
                        position: position,
                        zPosition: zPosition,
                        fillColor: fillColor
                ))
        )

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
