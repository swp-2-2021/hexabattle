//
//  ButtonEntity.swift
//  hexabattle
//
//  Created by Salma Akther on 15.04.21.
//

import SpriteKit
import GameplayKit

class ButtonEntity: GKEntity {
    init(
            imageName: String,
            position: CGPoint,
            zPosition: CGFloat,
            size: CGSize? = nil,
            name: String,
            hasLabel: Bool = true,
            labelOffset: CGPoint = CGPoint(x: 0, y: -12),
            text: String = "",
            labelColor: UIColor = UIColor.hexaDarkBlue,
            fontSize: CGFloat = 12,
            behavior: AbstractTouchBehavior
    ) {
        super.init()
        self.name = name
        addComponent(SpriteComponent(
                sprite: Sprite(
                        texture: SKTexture(imageNamed: imageName),
                        position: position,
                        zPosition: zPosition,
                        size: size,
                        name: name
                )
        ))

        if hasLabel {
            addComponent(LabelComponent(
                    label: Label(
                            name: name,
                            position: CGPoint(x: position.x + labelOffset.x, y: position.y + labelOffset.y),
                            zPosition: zPosition + 0.1,
                            text: text,
                            styling: LabelStyle(
                                    fontColor: labelColor,
                                    fontSize: fontSize
                            )
                    )
            ))
        }
        addComponent(TouchComponent(behavior: behavior))
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
