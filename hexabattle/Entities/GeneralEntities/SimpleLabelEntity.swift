//
//  Created by Martin Uhlig on 28.04.21.
//

import SpriteKit
import GameplayKit

class SimpleLabelEntity: GKEntity {

    init(
            position: CGPoint,
            zPosition: CGFloat,
            text: String,
            labelColor: UIColor = UIColor.hexaDarkBlue,
            fontSize: CGFloat,
            labelHorizontalAlignmentMode: SKLabelHorizontalAlignmentMode? = nil,
            name: String,
            lines: Int? = nil
    ) {
        super.init()
        self.name = name
        addComponent(LabelComponent(
                label: Label(
                        name: name,
                        position: position,
                        zPosition: zPosition,
                        text: text,
                        styling: LabelStyle(fontColor: labelColor, fontSize: fontSize, labelHorizontalAlignmentMode: labelHorizontalAlignmentMode),
                        lines: lines
                ))
        )

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
