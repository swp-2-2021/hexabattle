//
//  PhaseLabel.swift
//  hexabattle
//
//  Created by Jonathan Psenicka on 13.04.21.
//

import SpriteKit
import GameplayKit

/**
 Can only be used if the Entity doesn't have to hold data.
 */
class LabelWithShapeEntity: GKEntity {

    init(
            position: CGPoint,
            backgroundZPosition: CGFloat,
            labelZPosition: CGFloat,
            size: CGSize,
            text: String,
            labelColor: UIColor = UIColor.black,
            backgroundColor: UIColor = UIColor.hexaLightBlue,
            name: String
    ) {
        super.init()
        self.name = name
        addComponent(ShapeComponent(
                shape: Shape(
                        shapeType: ShapeType.rectangle(Rectangle(size: size)),
                        name: name,
                        position: position,
                        zPosition: backgroundZPosition,
                        color: backgroundColor
                ))
        )
        addComponent(LabelComponent(
                labels: [Label(
                        name: name,
                        position: CGPoint(x: position.x, y: position.y - 10),
                        zPosition: labelZPosition,
                        text: text,
                        styling: LabelStyle(fontColor: labelColor)
                )])
        )
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
