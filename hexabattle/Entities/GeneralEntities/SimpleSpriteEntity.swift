//
//  Label.swift
//  hexabattle
//
//  Created by Tom Biskupski on 14.04.21.
//

import SpriteKit
import GameplayKit

/**
 Can only be used if the Entity doesn't have to hold data.
 */
class SimpleSpriteEntity: GKEntity {

    init(imageName: String, position: CGPoint, zPosition: CGFloat, scaleFactor: CGFloat = 1.0, name: String) {
        super.init()
        self.name = name
        addComponent(SpriteComponent(
                sprite: Sprite(
                        texture: SKTexture(imageNamed: imageName),
                        position: position,
                        zPosition: zPosition,
                        name: name,
                        scaleFactor: scaleFactor))
        )
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
