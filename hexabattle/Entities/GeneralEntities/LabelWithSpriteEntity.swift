//
//  ShopPriceTagEntity.swift
//  hexabattle
//
//  Created by Dejan Mijic on 27.04.21.
//

import SpriteKit
import GameplayKit

/**
 Can only be used if the Entity doesn't have to hold data.
 */
class LabelWithSpriteEntity: GKEntity {

    init(
            imageName: String,
            position: CGPoint,
            text: String,
            labelColor: UIColor = UIColor.black,
            fontSize: CGFloat = Size.fontSize,
            backgroundColor: UIColor = UIColor.hexaLightBlue,
            name: String
    ) {
        super.init()
        self.name = name
        addComponent(SpriteComponent(
                sprites: [Sprite(
                        texture: SKTexture(imageNamed: imageName),
                        position: position,
                        zPosition: ZPosition.popup1,
                        name: name
                )])
        )

        addComponent(LabelComponent(
                label: Label(
                        name: name,
                        position: CGPoint(x: position.x, y: position.y - 12),
                        zPosition: ZPosition.popup2,
                        text: text,
                        styling: LabelStyle(fontColor: labelColor, fontSize: fontSize)
                ))
        )
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
