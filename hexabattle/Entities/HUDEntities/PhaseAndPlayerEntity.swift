//
// Created by Marcel Henning on 18.06.21.
//

import SpriteKit
import GameKit
import GameplayKit

class PhaseAndLabelEntity: GKEntity {

    init(
            position: CGPoint,
            backgroundZPosition: CGFloat,
            labelZPosition: CGFloat,
            size: CGSize,
            text: String,
            labelColor: UIColor = UIColor.black,
            backgroundColor: UIColor = UIColor.hexaLightBlue,
            name: String
    ) {
        super.init()
        self.name = name

        addComponent(ShapeComponent(
                shapes: [
                    Shape(
                            shapeType: ShapeType.rectangle(Rectangle(size: size)),
                            name: name,
                            position: position,
                            zPosition: backgroundZPosition,
                            color: backgroundColor),
                    Shape(
                            shapeType: ShapeType.rectangle(Rectangle(size: CGSize(width: 70, height: 70))),
                            name: name,
                            position: CGPoint(x: position.x - 180, y: position.y),
                            zPosition: backgroundZPosition,
                            color: EntityManager.instance.player.getColor())

                ])
        )
        addComponent(LabelComponent(
                labels: [Label(
                        name: Names.labelPhase,
                        position: CGPoint(x: position.x - 120, y: position.y + 5),
                        zPosition: labelZPosition,
                        text: text,
                        styling: LabelStyle(fontColor: labelColor, labelHorizontalAlignmentMode: SKLabelHorizontalAlignmentMode.left)
                ),
                    Label(
                            name: Names.labelPlayer,
                            position: CGPoint(x: position.x - 120, y: position.y - 25),
                            zPosition: labelZPosition,
                            text: GKLocalPlayer.local.displayName,
                            styling: LabelStyle(fontColor: UIColor.hexaWhite, fontSize: 18, labelHorizontalAlignmentMode: SKLabelHorizontalAlignmentMode.left)
                    )
                ])
        )

        addComponent(SpriteComponent(sprite: Sprite(
                texture: SKTexture(imageNamed: "User"),
                position: CGPoint(x: position.x - 180, y: position.y),
                zPosition: backgroundZPosition,
                size: CGSize(width: 60, height: 60),
                name: name,
                scaleFactor: 1)))
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
