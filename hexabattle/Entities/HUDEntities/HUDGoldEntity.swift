//
// Created by Marvin Klein on 05.05.21.
//

import SpriteKit
import GameplayKit

class HUDGoldEntity: GKEntity {

    init(
            imageName: String,
            position: CGPoint,
            text: String,
            labelColor: UIColor = UIColor.black,
            fontSize: CGFloat,
            name: String
    ) {
        super.init()
        super.name = name
        addComponent(SpriteComponent(
                sprites: [
                    Sprite(
                            texture: SKTexture(imageNamed: imageName),
                            position: position,
                            zPosition: ZPosition.hudElementBackground,
                            size: CGSize(width: 150, height: 50),
                            name: name
                    )
                ])
        )
        addComponent(LabelComponent(
                labels: [
                    Label(
                            name: name,
                            position: CGPoint(x: position.x + 17, y: position.y - 12),
                            zPosition: ZPosition.hud1,
                            text: text,
                            styling: LabelStyle(
                                    fontColor: labelColor, fontSize: fontSize)
                    )
                ])
        )
        addComponent(GoldComponent())

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
