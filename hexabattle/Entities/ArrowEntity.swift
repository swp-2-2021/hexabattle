//
//  ArrowEntity.swift
//  hexabattle
//
//  Created by Salma Akther on 07.06.21.
//
import SpriteKit
import GameplayKit
import GameKit

class ArrowEntity: GKEntity {

    override init() {
        super.init()
    }

    init(
            name: String,
            sourceTile: TileEntity,
            destinationTile: TileEntity,
            labelColor: UIColor = UIColor.white
    ) {
        super.init()
        self.name = name

        addComponent(ShapeComponent(shapes:
        [
            Shape(
                shapeType: ShapeType.arrow(Arrow(srcTile: sourceTile, destTile: destinationTile)),
                    name: name,
                    position: CGPoint(),
                    zPosition: ZPosition.mapArrow,
                    strokeColor: EntityManager.instance.player.getColor(),
                    glowWidth: 3,
                    lineWidth: 2,
                    hide: false,
                    opacity: 1)
        ]))
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
