//
//  PlusMinusEntity.swift
//  hexabattle
//
//  Created by Tom Biskupski on 27.04.21.
//

import SpriteKit
import GameplayKit

class PlusMinusEntity: GKEntity {
    var textLabel: String = ""
    var numberLabel = "0"

    init(text: String, position: CGPoint, name: String, behavior: AbstractTouchBehavior, hasGlobalTroopsComponent: Bool) {
        super.init()
        self.name = name

        textLabel = text

        if hasGlobalTroopsComponent {

            addComponent(GlobalTroopsComponent(troopsAvailable: 0))

            textLabel = text + ": \(self.component(ofType: GlobalTroopsComponent.self)?.troopsAvailable ?? -1)"
            numberLabel = "\(self.component(ofType: GlobalTroopsComponent.self)?.troopsPlaced ?? -1)"

            if name == Names.entityMoveFight {
                textLabel = text
            }
        }

        addComponent(SpriteComponent(
                sprite: Sprite(
                texture: SKTexture(imageNamed: "ButtonBlack"),
                position: position,
                zPosition: ZPosition.hudElementBackground,
                size: CGSize(width: 243, height: 90),
                name: name)))
        addComponent(LabelComponent(labels: [
            // This label displays a Text on the top of the button
            Label(
                    name: "\(name).troopsAvailable",
                    position: CGPoint(x: position.x, y: position.y + 10),
                    zPosition: ZPosition.hud2,
                    text: textLabel,
                    styling: LabelStyle(fontSize: 20)
            ),
            Label(
                    name: "\(name).minus",
                    position: CGPoint(x: position.x - 40, y: position.y - 27),
                    zPosition: ZPosition.hud2,
                    text: "-",
                    styling: LabelStyle(fontSize: 40)
            ),
            Label(
                    name: "\(name).plus",
                    position: CGPoint(x: position.x + 40, y: position.y - 27),
                    zPosition: ZPosition.hud2,
                    text: "+",
                    styling: LabelStyle(fontSize: 40)
            ),
            // This label shows the a the Number that can be changed through + and - (is initialized wit "0")
            Label(
                    name: "\(name).troopsPlaced",
                    position: CGPoint(x: position.x, y: position.y - 27),
                    zPosition: ZPosition.hud2,
                    text: numberLabel,
                    styling: LabelStyle(fontSize: 30)
            )
        ]))
        // add Shapes over + and - to make the buttons better pressable
        addComponent(ShapeComponent(shapes: [
            Shape(shapeType: ShapeType.circle(Circle(radius: 20)),
                    name: "\(name).minusShape",
                    position: CGPoint(x: position.x - 40, y: position.y - 17),
                    zPosition: ZPosition.hud3,
                    color: UIColor.white,
                    opacity: 0.001
            ),
            Shape(shapeType: ShapeType.circle(Circle(radius: 20)),
                    name: "\(name).plusShape",
                    position: CGPoint(x: position.x + 40, y: position.y - 17),
                    zPosition: ZPosition.hud3,
                    color: UIColor.white,
                    opacity: 0.001
            )
        ]))
        addComponent(TouchComponent(behavior: behavior))
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
