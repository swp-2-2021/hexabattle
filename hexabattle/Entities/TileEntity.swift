//
//  TileEntity.swift
//  hexabattle
//
//  Created by Marvin Klein on 13.04.21.
//

import SpriteKit
import GameplayKit

class TileEntity: GKEntity {

    override init() {
        super.init()
    }

    init(
            tileType: TileType,
            position: CGPoint,
            name: String,
            labelColor: UIColor = UIColor.black,
            column: Int,
            row: Int) {
        super.init()
        self.name = name
        addComponent(
                SpriteComponent(
                        sprite: Sprite(
                                texture: SKTexture(imageNamed: tileType.getImageName()),
                                position: position,
                                zPosition: ZPosition.mapTiles,
                                name: name,
                                scaleFactor: 1.12
                        ))
        )
        addComponent(GridPositionComponent(column: column, row: row, tileType: tileType, name: name))
        addComponent(TouchComponent(behavior: TileTouchBehavior()))
        if tileType != TileType.water {
            addComponent(ShapeComponent(shapes:
            [
                Shape(
                        shapeType: ShapeType.hexagon(Hexagon(size: 58)),
                        name: name + ".highlight",
                        position: position,
                        zPosition: ZPosition.mapHighlight,
                        strokeColor: EntityManager.instance.player.getColor(),
                        glowWidth: 3,
                        lineWidth: 2,
                        hide: true,
                        opacity: 1),
                Shape(
                        shapeType: ShapeType.hexagon(Hexagon(size: 58)),
                        name: name + ".highlight.moveFight",
                        position: position,
                        zPosition: ZPosition.mapHighlightFightMove,
                        strokeColor: UIColor.hexaDarkRed,
                        glowWidth: 3,
                        lineWidth: 2,
                        hide: true,
                        opacity: 1),
                Shape(
                        shapeType: ShapeType.hexagon(Hexagon(size: 58)),
                        name: name + ".highlight.neighbor",
                        position: position,
                        zPosition: ZPosition.mapHighlight,
                        strokeColor: UIColor.hexaWhite,
                        glowWidth: 3,
                        lineWidth: 2,
                        hide: true,
                        opacity: 1),
                Shape(
                        shapeType: ShapeType.hexagon(Hexagon(size: 64)),
                        name: name + ".lowlight",
                        position: position,
                        zPosition: ZPosition.mapHighlight,
                        color: UIColor.hexaDarkBlue,
                        lineWidth: 0,
                        hide: false,
                        opacity: 0.5),

                Shape(
                        shapeType: ShapeType.circle(Circle(radius: 20)),
                        name: name + ".circle",
                        position: position,
                        zPosition: ZPosition.mapLabelBackground,
                        hide: true,
                        opacity: 0.5)

            ]))

            addComponent(TroopCountComponent(currentTroops: 0))
            addComponent(PlayerComponent())
            if tileType == TileType.cityWall0 {
                addComponent(CityTileComponent())
            }
            addComponent(LabelComponent(labels: [
                Label(
                        name: name + ".currentTroops",
                        position: CGPoint(x: position.x, y: position.y - 10),
                        zPosition: ZPosition.mapLabel,
                        text: "",
                        styling: LabelStyle(fontSize: 23)),

                Label(
                        name: name + ".addedTroops",
                        position: CGPoint(x: position.x + 30, y: position.y - 35),
                        zPosition: ZPosition.mapLabel,
                        text: "",
                        styling: LabelStyle(fontColor: UIColor.blue, fontSize: 23))
            ]))
        }

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
