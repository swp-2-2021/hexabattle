//
//  Player.swift
//  hexabattle
//
//  Created by Salma on 27.04.21.
//

import UIKit

enum Player: Int, Codable {
    case none = 0
    case player1 = 1
    case player2 = 2
    case player3 = 3
    case player4 = 4

    static let allValues = [none, player1, player2, player3, player4]

    func getColor() -> UIColor {
        switch self {
        case .none:
            return UIColor.white
        case .player1:
            return UIColor.cyan
        case .player2:
            return UIColor.blue
        case .player3:
            return UIColor.brown
        case .player4:
            return UIColor.orange
        }
    }

    func toString() -> String {
        switch self {
        case .none:
            return "none"
        case .player1:
            return "player1"
        case .player2:
            return "player2"
        case .player3:
            return "player3"
        case .player4:
            return "player4"
        }
    }
}
