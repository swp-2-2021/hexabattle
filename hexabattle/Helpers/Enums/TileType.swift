//
//  TileType.swift
//  hexabattle
//
//  Created by Marvin Klein on 13.04.21.
//

enum TileType: Int {
    case water = 0
    case cityWall0 = 1
    case cityWall1 = 11
    case cityWall2 = 111
    case mountain = 2
    case forest = 3
    case grass = 4
    case placeholder = 8

    static let allValues = [water, grass, mountain, forest, cityWall0, cityWall1, cityWall2, placeholder]

    func getImageName() -> String {
        if self == .water {
            return "WaterTile"
        }
        if self == .grass {
            return "GrasslandTile"
        }
        if self == .mountain {
            return "MountainTile"
        }
        if self == .forest {
            return "ForestTile"
        }
        if self == .cityWall0 {
            return "CityWall0Tile"
        }
        if self == .cityWall1 {
            return "CityWall1Tile"
        }
        if self == .cityWall2 {
            return "CityWall2Tile"
        }
        if self == .placeholder {
            return "placeholder"
        }
        return "NoType"
    }

}
