//
//  Phase.swift
//  hexabattle
//
//  Created by Jonathan Psenicka on 14.04.21.
//

enum State: String, Codable {
    case reinforcement = "reinforcement"
    case combat = "combat"
    case preGame = "pre_game"
    case startMenu = "start_menu"
    case gameOver = "game_over"
    case transition = "transition"
}
