//
// Created by Marcel Henning on 15.05.21.
//

import Foundation
import GameplayKit

private var storedProperty_FILEPRIVATE: [ObjectIdentifier:String] = [:]
private var storedProperty_DEFAULT: String = ""
extension GKEntity {

    var name: String {
        get {return storedProperty_FILEPRIVATE[ObjectIdentifier(self)] ?? storedProperty_DEFAULT}
        set {storedProperty_FILEPRIVATE[ObjectIdentifier(self)] = newValue}
    }
}
