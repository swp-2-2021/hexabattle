//
// Created by Marvin Klein on 06.05.21.
//

import SpriteKit

class MapCamera: SKCameraNode {

    func setConstraints(with scene: SKScene, and frame: CGRect) {
        let scaledSize = CGSize(width: scene.size.width * xScale, height: scene.size.height * yScale)
        let boardConent = frame
        let xInset = min(scaledSize.width/2, boardConent.width/2)
        let yInset = min(scaledSize.height/2, boardConent.height/2)
        let insentContentRect = boardConent.insetBy(dx: xInset, dy: yInset)
        let xRange = SKRange(lowerLimit: insentContentRect.minX, upperLimit: insentContentRect.maxX)
        let yRange = SKRange(lowerLimit: insentContentRect.minY, upperLimit: insentContentRect.maxY)
        let levelEdgeConstraint = SKConstraint.positionX(xRange, y: yRange)
        constraints = [levelEdgeConstraint]
    }
}
