//
// Created by Marcel Henning on 17.06.21.
//

import Foundation

class PhaseTimer {

    static let instance = PhaseTimer()
    var timer: Timer?
    var secondsCounter = 100

    @objc func secondsTimer() {
        secondsCounter -= 1
        if secondsCounter <= 0 {
            StateMachine.instance.currentState?.timerExpired()
        } else {
            StateMachine.instance.currentState?.updateLabelOnSecondTick()
        }
    }

    func startCombatTimer() {
        secondsCounter = GameValues.combatTime
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(secondsTimer), userInfo: nil, repeats: true)

    }

    func startReinforcementTimer() {
        secondsCounter = GameValues.reinforcementTime
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(secondsTimer), userInfo: nil, repeats: true)

    }

    func stopTimer() {
        timer?.invalidate()
        timer = nil
    }
}
