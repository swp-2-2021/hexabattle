//
//  UIColorExtension.swift
//  hexabattle
//
//  Created by Marcel Henning on 12.04.21.
//

import UIKit

@objc public extension UIColor {

    static let hexaDarkBlue = UIColor.color(named: "hexaDarkBlue")
    static let hexaLightBlue = UIColor.color(named: "hexaLightBlue")
    static let hexaDarkRed = UIColor.color(named: "hexaDarkRed")
    static let hexaLightRed = UIColor.color(named: "hexaLightRed")
    static let hexaWhite = UIColor.color(named: "hexaWhite")

    static let allColors: [UIColor] = [
        hexaDarkBlue,
        hexaLightBlue,
        hexaWhite,
        hexaDarkRed,
        hexaLightRed

    ]

    static let allNames: [String] = [
        "hexaDarkBlue",
        "hexaDarkRed",
        "hexaLightBlue",
        "hexaLightRed",
        "hexaWhite"
    ]

    private static func color(named: String) -> UIColor {
        return UIColor(named: named)!
    }
}
