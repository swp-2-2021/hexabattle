//
//  ZPosition.swift
//  hexabattle
//
//  Created by Marvin Klein on 23.04.21.
//

import SpriteKit

class ZPosition {
    static let mapTiles = CGFloat(0)
    static let mapHighlight = CGFloat(0.1)
    static let mapHighlightFightMove = CGFloat(0.2)
    static let mapArrow = CGFloat(0.3)
    static let mapLabelBackground = CGFloat(0.4)
    static let mapLabel = CGFloat(0.5)
    static let map1 = CGFloat(0.6)
    static let map2 = CGFloat(0.7)
    static let hudElementBackground = CGFloat(10)
    static let hud1 = CGFloat(10.1)
    static let hud2 = CGFloat(10.2)
    static let hud3 = CGFloat(10.3)
    static let popupBackground = CGFloat(20)
    static let popup1 = CGFloat(20.1)
    static let popup2 = CGFloat(20.2)
    static let popup3 = CGFloat(20.3)
    static let optionsBackground = CGFloat(30)
    static let options1 = CGFloat(30.1)
    static let options2 = CGFloat(30.2)
    static let options3 = CGFloat(30.3)
    static let helpBackground = CGFloat(19.7)
    static let help1 = CGFloat(19.8)
    static let help2 = CGFloat(19.9)

    static let optionsIcon = CGFloat(10.1)
    static let InGameHelpIcon = CGFloat(19.6)
    static let waitingScreen = CGFloat(40)

}
