//
//  Size.swift
//  hexabattle
//
//  Created by Dejan Mijic on 27.04.21.
//

import SpriteKit

class Size {
    static let sceneSize = CGSize(width: 1000 * EntityManager.instance.view.bounds.size.width / EntityManager.instance.view.bounds.size.height, height: 1000)
    static let fontSize = CGFloat(20.0)
}
