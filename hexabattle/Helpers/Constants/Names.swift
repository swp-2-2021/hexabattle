//
// Created by Marvin Klein on 26.04.21.
//

class Names {

    // icons
    static let iconSettings = "settingsIcon"
    static let iconHelp = "helpIcon"

    // HUD
    static let hudPlayerAndPhase = "hudPlayerAndPhase"

    // labels
    static let labelPhase = "phaseLabel"
    static let labelPlayer = "labelPlayer"
    static let labelAvailableGold = "availableGoldLabel"
    static let labelArmySize = "armySizeLabel"
    static let labelEmpireSize = "empireSizeLabel"
    static let labelOccupiedCities = "occupiedCitiesLabel"

    // shop
    static let iconShowShop = "showShop"
    static let shopBackground = "shopBackground"

    static let buttonBuyTroops = "buyTroops"
    static let buttonAddTroops = "addTroops"
    static let buttonReduceTroops = "reduceTroops"
    static let buttonBuyWall = "buyWall"
    static let buttonBuyMarket = "buyMarket"
    static let buttonCloseShop = "closeShop"
    static let labelTotalTroopPrice = "labelTotalTroopPrice"
    static let labelTransactionStatus = "labelTransactionStatus"

    static let labelShopTitle = "shopTitle"
    static let labelTroopsDescription = "troopLabel"
    static let labelWallDescription = "wallLabel"
    static let labelWallImpact = "wallImpact"
    static let labelMarketDescription = "marketLabel"
    static let labelMarketImpact = "marketImpact"

    static let labelTroopsPriceTag = "troopsPriceTag"
    static let labelWallPriceTag = "wallPriceTag"
    static let labelMarketPriceTag = "marketPriceTag"

    // options
    static let iconShowOptions = "showOptions"
    static let optionsBackground = "optionsBackGround"
    static let audioVolume = "audioVolume"

    static let buttonCloseOptions = "ResumeButton"
    static let buttonQuitGame = "QuitButton"
    static let buttonSoundOn = "SoundOn"
    static let buttonSoundOff = "SoundOff"

    static let startBackground = "startBackground"
    static let sliderVolume = "slider"

    static let labelOptions = "optionsLabel"
    static let labelSound = "soundLabel"
    static let labelVolume = "volumeLabel"

    // InGame Help
    static let buttonCloseInGameHelp = "CloseButton"
    static let labelHelp = "helpLabel"
    static let labelHelpText = "helpText"
    static let helpBackground = "helpBackground"

    // waitingScreen
    static let waitingScreenWithLabel = "waitingScreen"

    // manual
    static let manualBackground = "manualBackground"
    static let buttonCloseManual = "closeManual"
    static let buttonOpenManual = "openManualButton"
    static let buttonNextPage = "nextPageButton"
    static let buttonPreviousPage = "previousPageButton"

    // bundels
    static let bundleExample = "example"
    static let bundleShop = "shop"
    static let bundleOptions = "options"
    static let bundleInGameHelp = "ingamehelp"
    static let bundleManual = "manual1"
    static let bundleManual2 = "manual2"

    static let gameOverBundle = "gameOverBundle"

    // ChangeTroops +-Button
    static let changeTroops = "changeTroopsButton"
    static let troopsConfirm = "TroopsConfirmButton"
    static let reinforcementFinishButton = "reinforcementFinishButton"

    // move/fight +-Button
    static let entityMoveFight = "moveChangeTroopsButton"
    static let buttonMoveFightConfirm = "moveTroopsButton"
    static let fightTroops = "fightButton"
    static let confirmMove = "confirmMoveButton"

    // Arrow
    static let arrow = "arrow"

    // Sounds
    static let soundBackground = "WarOfExplodingMountains"
    static let soundReinforce = "Reinforce"
    static let soundMove = "TroopsMarching"
    static let soundButtonClick = "Button"
    static let soundCoin = "Coin"
    static let soundFight = "Swords"
}
