//
//  InGameHelp.swift
//  hexabattle
//
//  Created by 91elhe1bif on 06.06.21.
//

import Foundation

class InGameHelp {
    static let reinforcementState = "Set your available troops on tiles that belong to you. Buy troops/upgrades in the shop."
    static let combatState = "Click on a tile to move your troops. Attack an enemy to expand your empire."
    static let otherPlayerTurnState = "It is now your enemies turn. Please wait while the enemy is performing its attack."
}
