//
// Created by Marvin Klein on 06.05.21.
//

import Foundation

class GameValues {
    static let startGold = 300
    static let startTroops = 10
    // timer
    static let reinforcementTime = 60
    static let combatTime = 40

    static let combatRounds = 3

    // Gold Generation
    static let goldGenerationBaseAmount = 100
    static let goldGenerationPerCity = 200
    static let goldGenerationPerTile = 50

    static let goldIncreasement = 300
    static let maxGoldGeneration = 700

    // shop prices
    static let priceMarketUpgrade = 350
    static let priceRecruitTroop = 50
    static let priceCityUpgrade = 250

    // Sound volumes between 0.0 and 1.0
    // music
    static let volumeMusic = Float(0.5)

    // effects
    static let volumeBattle = Float(0.9)
    static let volumeMove = Float(1.0)
    static let volumeCoin = Float(0.8)
    static let volumeButtonClick = Float(0.8)

}
