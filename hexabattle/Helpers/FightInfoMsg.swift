//
//  FightInfoMsg.swift
//  hexabattle
//
//  Created by Salma Akther on 25.05.21.
//

import SpriteKit
import GameKit

class FightInfoMsg {
    class private func showMessage(backgroundColor:UIColor, textColor:UIColor, message:String) {

        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let label = UILabel(frame: CGRect.zero)
        label.textAlignment = NSTextAlignment.center
        label.text = message
        label.font = label.font.withSize(30)
        label.adjustsFontSizeToFitWidth = true

        label.backgroundColor =  backgroundColor
        label.textColor = textColor

        label.sizeToFit()
        label.numberOfLines = 4
        label.layer.shadowColor = UIColor.gray.cgColor
        label.layer.shadowOffset = CGSize(width: 4, height: 3)
        label.layer.shadowOpacity = 0.3
        label.frame = CGRect(x: appDelegate.window!.frame.size.width , y: 64, width: appDelegate.window!.frame.size.width, height: 100)

        label.alpha = 1

        appDelegate.window!.addSubview(label)

        var basketTopFrame: CGRect = label.frame
        basketTopFrame.origin.x = 0

        UIView.animate(withDuration: 5.0,
                    delay: 0.0,
                    usingSpringWithDamping: 0.5,
                    initialSpringVelocity: 0.1,
                    options: UIView.AnimationOptions.curveEaseOut,
                    animations: { () -> Void in label.frame = basketTopFrame},

                    completion: { (_: Bool) in UIView.animate(
                    withDuration: 5.0,
                    delay: 2.0,
                    usingSpringWithDamping: 0.5,
                    initialSpringVelocity: 0.1,
                    options: UIView.AnimationOptions.curveEaseIn,

                    animations: { () -> Void in label.alpha = 0 },

                    completion: { (_: Bool) in label.removeFromSuperview()
            })
        })
    }

    class func showWinningMessage(message:String) {
        showMessage(backgroundColor: UIColor.blue, textColor: UIColor.white, message: message)
    }

    class func showLosingMessage(message:String) {
        showMessage(backgroundColor: UIColor.red, textColor: UIColor.white, message: message)
    }
}
