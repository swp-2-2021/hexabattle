//
// Created by student on 25.04.21.
//

import Foundation

class FeatureFlags {

    static let singlePlayer = ProcessInfo.processInfo.environment["singlePlayer"] == "true"
    static let mockupGame = ProcessInfo.processInfo.environment["mockupGame"] == "true"
}
