//
//  GameViewController.swift
//  hexabattle
//
//  Created by student on 02.04.21.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        if let skView = view as? SKView {
            skView.showsFPS = true
            skView.showsNodeCount = true
            skView.ignoresSiblingOrder = true
            EntityManager.instance.view = skView
        }

        StateMachine.instance.enter(StartMenuState.self)

        GameCenterAuthenticator().initAuthenticationHandler()
    }

    override var prefersStatusBarHidden: Bool {
        true
    }
}
