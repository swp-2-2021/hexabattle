//
//  MatchmakingScene.swift
//  hexabattle
//
//  Created by Salma Akther on 14.04.21.
//

import SpriteKit
import GameplayKit

class MatchmakingScene: SKScene {

    override func didMove(to view: SKView) {

        // EntityManager wird die Scene übergeben, um die UIComponents anzeigen zu können
        EntityManager.instance.setScene(scene: self)
        // Play or Start Button
//        let playButton = ButtonEntity(
//                imageName: "play-button",
//                spritePosition: CGPoint(x: size.width / 2, y: size.height / 3),
//                zPosition: 1,
//                text: "Start",
//                size: CGSize(width: 150, height: 150),
//                name: "play-button",
//                behavior: TileTouchBehavior()
//        )
//        EntityManager.instance.add(playButton)
//        // Me Button
//        let meButton = ButtonEntity(
//                imageName: "me",
//                spritePosition: CGPoint(x: size.width - size.width / 1.25, y: size.height / 1.6),
//                zPosition: 1,
//                text: "Me",
//                size: CGSize(width: 150, height: 150),
//                name: "me",
//                behavior: TileTouchBehavior()
//        )
//        EntityManager.instance.add(meButton)
//        // Player-Add Button
//        let playerButton = ButtonEntity(
//                imageName: "add-player",
//                spritePosition: CGPoint(x: size.width / 1.25, y: size.height / 1.6),
//                zPosition: 1,
//                text: "Add Player",
//                size: CGSize(width: 150, height: 150),
//                name: "add-player",
//                behavior: TileTouchBehavior()
//        )
//        EntityManager.instance.add(playerButton)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }

    override func update(_ currentTime: TimeInterval) {

    }
}
