//
//  StartScene.swift
//  hexabattle
//
//  Created by Tom Biskupski on 14.04.21.
//

import SpriteKit
import GameplayKit
import GameKit
import os.log

class StartScene: SKScene {

    private static let logObject = OSLog(subsystem: OSLog.subsystem, category: "startscene")

    override init() {
        SoundManager.instance.initBackgroundMusic()
        let aspectRatio = Size.sceneSize.width / Size.sceneSize.height
        super.init(size: CGSize(width: 1000 * aspectRatio, height: 1000))
        scaleMode = .aspectFill
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func didMove(to view: SKView) {
        os_log("StartScene didMove() called", log: StartScene.logObject, type: .info)

        // EntityManager wird die Scene übergeben, um die UIComponents anzeigen zu können
        EntityManager.instance.setScene(scene: self)

        // Hexagon Hintergrund
        let backgroundSprite = SimpleSpriteEntity(
                imageName: "Background",
                position: CGPoint(x: size.width / 2, y: size.height / 2),
                zPosition: ZPosition.hudElementBackground,
                scaleFactor: 0.5,
                name: Names.startBackground
        )
        EntityManager.instance.addToScene(backgroundSprite)

        // StartSprite einfügen
        let startSprite2 = SimpleSpriteEntity(
                imageName: "2Player",
                position: CGPoint(x: size.width / 2 - 200, y: size.height / 3 + 100),
                zPosition: ZPosition.hud1,
                scaleFactor: 0.7,
                name: "start2"
            )
        EntityManager.instance.addToScene(startSprite2)

        // StartSprite einfügen
        let startSprite3 = SimpleSpriteEntity(
                imageName: "3Player",
                position: CGPoint(x: size.width / 2, y: size.height / 3 + 100),
                zPosition: ZPosition.hud1,
                scaleFactor: 0.7,
                name: "start3"
        )
        EntityManager.instance.addToScene(startSprite3)

        // StartSprite einfügen
        let startSprite4 = SimpleSpriteEntity(
                imageName: "4Player",
                position: CGPoint(x: size.width / 2 + 200, y: size.height / 3 + 100),
                zPosition: ZPosition.hud1,
                scaleFactor: 0.7,
                name: "start4"
        )
        EntityManager.instance.addToScene(startSprite4)

        let howtoplay = SimpleSpriteEntity(
                imageName: "howtoplay",
                position: CGPoint(x: size.width / 2, y: size.height / 3 - 100),
                zPosition: ZPosition.hud1,
                scaleFactor: 0.7,
                name: "howtoplay"
        )
        EntityManager.instance.addToScene(howtoplay)

        // Manual
        EntityManager.instance.addBundleToScene(bundle: StartManualBundle(bundleName: Names.bundleManual, frame: frame, imageName: "manual1"))
        EntityManager.instance.bundles[Names.bundleManual]?.hideBundle(hide: true)

        EntityManager.instance.addBundleToScene(bundle: StartManualBundle(bundleName: Names.bundleManual2, frame: frame, imageName: "manual2"))
        EntityManager.instance.bundles[Names.bundleManual2]?.hideBundle(hide: true)

    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let firstTouch = touches.first {
            let touchedNode = atPoint(firstTouch.location(in: self)) as? SKSpriteNode
            if touchedNode?.name == "start2" {
                SoundManager.instance.playSoundEffect(soundName: Names.soundButtonClick, volumeLevel: GameValues.volumeButtonClick)
                GameCenterManager.instance.showMatchmaker(playerNumber: 2)
            } else if touchedNode?.name == "start3" {
                SoundManager.instance.playSoundEffect(soundName: Names.soundButtonClick, volumeLevel: GameValues.volumeButtonClick)
                GameCenterManager.instance.showMatchmaker(playerNumber: 3)
            } else if touchedNode?.name == "start4" {
                SoundManager.instance.playSoundEffect(soundName: Names.soundButtonClick, volumeLevel: GameValues.volumeButtonClick)
                GameCenterManager.instance.showMatchmaker(playerNumber: 4)
            } else if touchedNode?.name == "howtoplay" {
                SoundManager.instance.playSoundEffect(soundName: Names.soundButtonClick, volumeLevel: GameValues.volumeButtonClick)
                EntityManager.instance.bundles[Names.bundleManual]?.hideBundle(hide: false)
                EntityManager.instance.bundles[Names.bundleManual]?.hideBundleEntityByName(hide: true, name: "previousPage")
            } else if touchedNode?.name == "closeManual" {
                SoundManager.instance.playSoundEffect(soundName: Names.soundButtonClick, volumeLevel: GameValues.volumeButtonClick)
                EntityManager.instance.bundles[Names.bundleManual]?.hideBundle(hide: true)
                EntityManager.instance.bundles[Names.bundleManual2]?.hideBundle(hide: true)
            } else if touchedNode?.name == "nextPage" {
                SoundManager.instance.playSoundEffect(soundName: Names.soundButtonClick, volumeLevel: GameValues.volumeButtonClick)
                EntityManager.instance.bundles[Names.bundleManual]?.hideBundle(hide: true)
                EntityManager.instance.bundles[Names.bundleManual2]?.hideBundle(hide: false)
                EntityManager.instance.bundles[Names.bundleManual2]?.hideBundleEntityByName(hide: true, name: "nextPage")
            } else if touchedNode?.name == "previousPage" {
                SoundManager.instance.playSoundEffect(soundName: Names.soundButtonClick, volumeLevel: GameValues.volumeButtonClick)
                EntityManager.instance.bundles[Names.bundleManual]?.hideBundle(hide: false)
                EntityManager.instance.bundles[Names.bundleManual2]?.hideBundle(hide: true)
                EntityManager.instance.bundles[Names.bundleManual]?.hideBundleEntityByName(hide: true, name: "previousPage")
            }
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }

    override func update(_ currentTime: TimeInterval) {
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
