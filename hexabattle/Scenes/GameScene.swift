//
//  GameScene.swift
//  hexabattle
//
//  Created by Marvin Klein on 02.04.21.
//

import SpriteKit
import GameplayKit
import GameKit
import os.log

class GameScene: SKScene {

    let mapCamera = MapCamera()

    override init() {
        let aspectRatio = EntityManager.instance.view.bounds.size.width / EntityManager.instance.view.bounds.size.height
        super.init(size: CGSize(width: 1000 * aspectRatio, height: 1000))
    }

    override func didMove(to view: SKView) {

        // MapManager is passed the Scene to be able to render the map
        MapManager.instance.setScene(scene: self)

        // EntityManager is passed the Scene to be able to display the UIComponents
        EntityManager.instance.setScene(scene: self)
        // init Camera
        addCamera()
        setupGestureRecognizers()
        EntityManager.instance.setCamera(mapCamera)

        InitUiBehavior.instance.frame = frame

        initHUD()

        // settings icon
        EntityManager.instance.addToCamera(
                ButtonEntity(imageName: "SettingsButton",
                        position: point(x: size.width - 50, y: size.height - 50),
                        zPosition: ZPosition.optionsIcon,
                        size: CGSize(width: 50, height: 50),
                        name: Names.iconSettings,
                        hasLabel: false,
                        labelColor: UIColor.hexaDarkBlue,
                        behavior: ShowOptionsTouchBehavior(entityName: Names.bundleOptions) as AbstractTouchBehavior)
        )

        // InGameHelp icon
        EntityManager.instance.addToCamera(
                ButtonEntity(imageName: "HelpButton",
                        position: point(x: 55, y: size.height - 170),
                        zPosition: ZPosition.InGameHelpIcon,
                        size: CGSize(width: 58, height: 59),
                        name: Names.iconHelp,
                        hasLabel: false,
                        behavior: ShowInGameHelpTouchBehavior(entityName: Names.bundleInGameHelp) as AbstractTouchBehavior)

        )

        // Option PopUp
        EntityManager.instance.addBundle(bundle: OptionsBundle(bundleName: Names.bundleOptions, frame: frame))
        EntityManager.instance.bundles[Names.bundleOptions]?.hideBundle(hide: true)

        // InGame Help
        EntityManager.instance.addBundle(bundle: InGameHelpBundle(bundleName: Names.bundleInGameHelp, frame: frame))
        EntityManager.instance.bundles[Names.bundleInGameHelp]?.hideBundle(hide: !InGameHelpManager.instance.isHelpEnabled)

        // Manual
        EntityManager.instance.addBundle(bundle: ManualBundle(bundleName: Names.bundleManual, frame: frame, imageName: "manual1"))
        EntityManager.instance.bundles[Names.bundleManual]?.hideBundle(hide: true)

        EntityManager.instance.addBundle(bundle: ManualBundle(bundleName: Names.bundleManual2, frame: frame, imageName: "manual2"))
        EntityManager.instance.bundles[Names.bundleManual2]?.hideBundle(hide: true)

        if FeatureFlags.singlePlayer {
            GameBehavior.instance.initGame([GKLocalPlayer.local.playerID])
        }
    }

    private func initHUD() {
        // available Gold
        EntityManager.instance.addToCamera(
                HUDGoldEntity(
                        imageName: "GoldLabel",
                        position: point(x: size.width - 195, y: size.height - 50),
                        text: "",
                        labelColor: UIColor.hexaDarkRed,
                        fontSize: 28.0,
                        name: Names.labelAvailableGold
                )
        )

        // total army size
        EntityManager.instance.addToCamera(
                HUDTroopEntity(
                        imageName: "ArmyLabel",
                        position: point(x: size.width - 350, y: size.height - 50),
                        text: "",
                        labelColor: UIColor.hexaDarkRed,
                        fontSize: 28.0,
                        name: Names.labelArmySize
                ))

        // empire size
        EntityManager.instance.addToCamera(
                HUDEmpireEntity(
                        imageName: "FieldsLabel",
                        position: point(x: size.width - 505, y: size.height - 50),
                        text: "",
                        labelColor: UIColor.hexaDarkRed,
                        fontSize: 28.0,
                        name: Names.labelEmpireSize
                ))

        // occupied cities
        EntityManager.instance.addToCamera(
                HUDCityEntity(
                        imageName: "CitiesLabel",
                        position: point(x: size.width - 660, y: size.height - 50),
                        text: "",
                        labelColor: UIColor.hexaDarkRed,
                        fontSize: 28.0,
                        name: Names.labelOccupiedCities
                ))
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }

    override func update(_ currentTime: TimeInterval) {
        EntityManager.instance.update(currentTime)
        MapManager.instance.update(currentTime)
    }

    func addCamera() {
        addChild(mapCamera)
        mapCamera.position = CGPoint(x: frame.midX, y: frame.midY)
        camera = mapCamera
        let border = CGRect(x: 0, y: -30, width: 2000, height: 1500)
        mapCamera.setConstraints(with: self, and: border)
    }

    @objc func panning(sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: sender.view)
        mapCamera.position = CGPoint(x: mapCamera.position.x - translation.x, y: mapCamera.position.y + translation.y)
        sender.setTranslation(CGPoint.zero, in: view)
    }

    @objc func longPressed(sender: UIPanGestureRecognizer) {
        if sender.state == .began {
            var post = sender.location(in: sender.view)
            post = self.convertPoint(fromView: post)
            let touchedNode = self.atPoint(post)
            if touchedNode.name != nil {
                EntityManager.instance.didHold(touchedNodeName: (touchedNode.name)!)
            } else {
                os_log("touched node has no name", type: .info)
            }
        }
    }

    func setupGestureRecognizers() {
        guard let view = view else {
            return
        }
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panning))
        view.addGestureRecognizer(panRecognizer)

        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapView))
        view.addGestureRecognizer(tapRecognizer)

        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
        longPressRecognizer.minimumPressDuration = 1.0 // 1 second press
        longPressRecognizer.allowableMovement = 60 // 15 points
        view.addGestureRecognizer(longPressRecognizer)

    }

    @objc func didTapView(_ sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            var post = sender.location(in: sender.view)
            post = self.convertPoint(fromView: post)
            let touchedNode = self.atPoint(post)
            if touchedNode.name != nil {
                EntityManager.instance.didTap(touchedNodeName: (touchedNode.name)!)
                MapManager.instance.didTap(touchedNodeName: (touchedNode.name)!, touchedLocation: post)
            } else {
                os_log("touched node has no name", type: .info)
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

// swiftlint:disable identifier_name
    func point(x: CGFloat, y: CGFloat) -> CGPoint {
        CGPoint(x: (x - frame.midX), y: (y - frame.midY))
    }
}
