//
// Created by Marvin Klein on 06.05.21.
//

import SpriteKit
import GameplayKit

class GoldBehavior {

    static let instance = GoldBehavior()

    private init() {
    }

    func initGold() {
        getEntity().component(ofType: GoldComponent.self)!.gold = GameValues.startGold - GameValues.goldGenerationBaseAmount
        getEntity().component(ofType: GoldComponent.self)!.goldGeneration = GameValues.goldGenerationBaseAmount
        updateGoldLabel()
    }

    func addGold(amount: Int) {
        if amount >= 0 {
            getEntity().component(ofType: GoldComponent.self)!.gold += amount
            updateGoldLabel()
        }
    }

    /*
     Add the given amount to gold generation.
     */
    func increaseGoldGeneration(amount: Int) {
        getEntity().component(ofType: GoldComponent.self)!.goldGeneration += amount
    }

    /**
     That function should be called after each transaction.
     For example after buying a new building or troops
     If enough gold is available the execute transaction the function will return true,
     otherwise if there isn't enough gold available it will return false
     */
    func deductGold(amount: Int) -> Bool { // Gold amount cant be < 0
        if getGold() - amount < 0 && amount >= 0 {
            return false
        } else {
            getEntity().component(ofType: GoldComponent.self)!.gold -= amount
            updateGoldLabel()
            return true
        }
    }

    /**
     That function should be called at the beginning of every reinforcement phase
     It contains the amount of occupied cities and the empire size (amount of HexaTiles owned
     by the player).

     The base amount it 100 gold per round.
     So at the beginning (1 city on 1 HexaTile) the player gets 100 gold per round.
     For every additional HexaTile the player will get 50 gold per round.
     For every additional city the player will get 80 gold per round extra

     Gold per round: 100 + 50*empireSize + 80*amountCities)
     */
    func reinforcementGold() {
        addGold(amount: calculateReinforcementGold())
    }

    func calculateReinforcementGold() -> Int {
         getGoldGeneration() + GameValues.goldGenerationPerTile * (EmpireBehavior.instance.getEmpireSize() - 1)
                + GameValues.goldGenerationPerCity * (CityBehavior.instance.getCitiesCount() - 1)
    }

    func updateGoldLabel() {
        getEntity().component(ofType: LabelComponent.self)?.nodes.first?.text = String(getGold())
    }

    func getGold() -> Int {
        getEntity().component(ofType: GoldComponent.self)!.gold
    }

    func getGoldGeneration() -> Int {
        getEntity().component(ofType: GoldComponent.self)!.goldGeneration
    }

    func getEntity() -> GKEntity {
        EntityManager.instance.entityDict[Names.labelAvailableGold]!
    }

}
