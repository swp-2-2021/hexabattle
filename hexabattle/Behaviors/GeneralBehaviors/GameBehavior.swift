//
// Created by Marcel Henning on 15.05.21.
//

import Foundation
import GameKit
import os.log

class GameBehavior {
    static var instance = GameBehavior()

    var reinforcementUIAdded: Bool = false
    var combatUIAdded: Bool = false
    var gameUIAdded: Bool = false

    private init() {
    }

    func reset() {
        GameBehavior.instance = GameBehavior()
    }

    /**
    Should be called once when all participants are known.
    */
    func initGame(_ playerOrder: [String]) {
        if !gameUIAdded {
            MapManager.instance.setMapforValueOfPlayers(Players: playerOrder.count)
            for (index, playerId) in playerOrder.enumerated() {
                // player with index 0 is "none", but the first player is needed.
                EntityManager.instance.playerDict[playerId] = Player.init(rawValue: index + 1)
            }

            EntityManager.instance.player = EntityManager.instance.playerDict[GKLocalPlayer.local.playerID]!
            if FeatureFlags.mockupGame {
                MapManager.instance.mockupGame()
            }
            MapManager.instance.renderMap()
            MapManager.instance.initCities(playerCount: playerOrder.count)
            TileBehavior.instance.setMainCityTile()
            InitUiBehavior.instance.initPhaseAndPlayerUI()
            GameBehavior.instance.centerMapOnPlayerCity()
            TroopBehavior.instance.updateTroopsCountLabel()
            EmpireBehavior.instance.updateEmpireSizeLabel()
            CityBehavior.instance.updateCitiesOccupiedLabel()
            GoldBehavior.instance.initGold()
            EntityManager.instance.addBundle(bundle: ShopBundle(bundleName: Names.bundleShop, frame: GameScene().frame))
            EntityManager.instance.bundles[Names.bundleShop]?.hideBundle(hide: true)
            gameUIAdded = true
            os_log("Game is ready", type: .info)
        }
    }

    /*
     Returns true if the game is lost or won and change to GameOverState.
     */
    func isGameOver() -> Bool {
        if isGameLost() || isGameWon() {
            StateMachine.instance.enter(GameOverState.self)
            return true
        }
        return false
    }

    /*
     The Game is lost when I do not own any citiy.
     */
    func isGameLost() -> Bool {
        CityBehavior.instance.getCitiesCount() < 1
    }

    /*
     Checks if any city on the map is owned by another player.
     Returns true when I am the only player with one ore more cities in the game.
     */
    func isGameWon() -> Bool {
        for (_, tile) in MapManager.instance.tileDict where (tile.component(ofType: CityTileComponent.self) != nil) {
            if let player = tile.component(ofType: PlayerComponent.self)?.player {
                if player != EntityManager.instance.player && player != Player.none {
                    return false
                }
            }
        }
        return true
    }

    /**
     Adds all Reinforcement UI elements to the Entity Manager
     */
    func initReinforcementUI() {
        removeCombatUI()
        if !reinforcementUIAdded {
            InGameHelpManager.instance.enableReinforcementHelp()

            // PLACE TROOPS: place +- button on the low right corner
            EntityManager.instance.addToCamera(
                    PlusMinusEntity(
                            text: "Available Troops",
                            position: CGPoint(x: 200, y: -400),
                            name: Names.changeTroops,
                            behavior: ChangeTroopCountTouchBehavior(entityName: Names.changeTroops),
                            hasGlobalTroopsComponent: true
                    ))

            // place reinforcement finish button next to +-entity
            EntityManager.instance.addToCamera(
                    ButtonEntity(
                            imageName: "ButtonBlack",
                            position: CGPoint(x: 325, y: -320),
                            zPosition: ZPosition.hud1,
                            size: CGSize(width: 500, height: 60),
                            name: Names.reinforcementFinishButton,
                            text: "Finish Reinforcement",
                            fontSize: 30,
                            behavior: FinishReinforcementTouchBehaviour(entityName: Names.reinforcementFinishButton)))

            // place "Confirm" button on the low right corner
            EntityManager.instance.addToCamera(
                    ButtonEntity(
                            imageName: "ButtonGrey",
                        position: CGPoint(x: 450, y: -400),
                            zPosition: ZPosition.hud1,
                            name: Names.troopsConfirm,
                            text: "Confirm",
                            fontSize: 30,
                            behavior: ConfirmTroopsSetTouchBehavior(entityName: Names.troopsConfirm)))
            // shop icon
            EntityManager.instance.addToCamera(
                    ButtonEntity(
                            imageName: "ShopIcon",
                            position: CGPoint(x: -500, y: -400),
                            zPosition: ZPosition.hud1,
                            size: CGSize(width: 90, height: 90),
                            name: Names.iconShowShop,
                            text: "",
                            fontSize: 0,
                            behavior: ShowShopTouchBehavior(entityName: Names.bundleShop) as AbstractTouchBehavior))

            EntityManager.instance.bundles[Names.bundleShop]?.hideBundle(hide: true)

            reinforcementUIAdded = true
        }
    }

    func removeReinforcementUI() {
        InGameHelpManager.instance.disableReinforcementHelp()

        if let shopButton = EntityManager.instance.entityDict[Names.iconShowShop] {
            EntityManager.instance.remove(shopButton)
        }
        if let troopsConfirmButton = EntityManager.instance.entityDict[Names.troopsConfirm] {
            EntityManager.instance.remove(troopsConfirmButton)
        }
        if let reinforcementFinishButton = EntityManager.instance.entityDict[Names.reinforcementFinishButton] {
            EntityManager.instance.remove(reinforcementFinishButton)
        }
        if let changeTroopsButton = EntityManager.instance.entityDict[Names.changeTroops] {
            EntityManager.instance.remove(changeTroopsButton)
        }
        reinforcementUIAdded = false
    }

    /**
    Adds all Combat UI elements to the Entity Manager
     */
    func initCombatUI() {
        removeReinforcementUI()
        if !combatUIAdded {
            InGameHelpManager.instance.enableCombatHelp()

            // MOVING: place +- button on the low right corner
            EntityManager.instance.addToCamera(
                    PlusMinusEntity(
                            text: "Move Troops",
                            position: CGPoint(x: 200, y: -400),
                            name: Names.entityMoveFight,
                            behavior: MoveFightBehavior(entityName: Names.entityMoveFight),
                            hasGlobalTroopsComponent: true
                    ))

            // Move/Confirm Button
            EntityManager.instance.addToCamera(
                    ButtonEntity(
                            imageName: "ButtonBlack",
                            position: CGPoint(x: 450, y: -400),
                            zPosition: ZPosition.hud1,
                            name: Names.buttonMoveFightConfirm,
                            text: "Skip Combat",
                            fontSize: 30,
                            behavior: MoveFightConfirmTouchBehavior(entityName: Names.buttonMoveFightConfirm)))

        }
        combatUIAdded = true
    }

    func removePlayerFromMap(player: Player) {
        for (key, tile) in MapManager.instance.tileDict where tile.component(ofType: PlayerComponent.self)?.player == player {
            MapManager.instance.occupyTile(tileName: key, newPlayer: Player.none)
            os_log("Remove Tile %s from map", type: .info, key)
            // TODO: Wenn es eine City ist muss diese ersetzt oder wieder auf Start gesetzt werden
        }
    }

    func removeCombatUI() {
        InGameHelpManager.instance.disableCombatHelp()

        if let entityMoveFight = EntityManager.instance.entityDict[Names.entityMoveFight] {
            EntityManager.instance.remove(entityMoveFight)
        }
        if let moveTroops = EntityManager.instance.entityDict[Names.buttonMoveFightConfirm] {
            EntityManager.instance.remove(moveTroops)
        }
        combatUIAdded = false
    }

    func enablePanning(value: Bool) {
        for gestureRecognizer in EntityManager.instance.view.gestureRecognizers! {
            if gestureRecognizer.isMember(of: UIPanGestureRecognizer.self) {
                gestureRecognizer.isEnabled = value
            }
        }
    }

    func updateReceivedTiles(_ tiles: [String: GameTile]) {
        for (dictKey, gameTile) in tiles {
            MapManager.instance.occupyTile(tileName: dictKey, newPlayer: gameTile.owner, newTroopsAmount: gameTile.troopCount)
            // Breakes the game: EXC_BAD_ACCESS
//                os_log("Refreshing tile: [%s] with troops: %d and Player: %d %s", type: .info, dictKey,
//                        gameTile.troopCount, gameTile.owner.rawValue)
        }

        TroopBehavior.instance.updateTroopsCountLabel()
        EmpireBehavior.instance.updateEmpireSizeLabel()
        CityBehavior.instance.updateCitiesOccupiedLabel()
    }

    func centerMapOnPlayerCity() {
        for (_, value) in MapManager.instance.tileDict where value.component(ofType: CityTileComponent.self) != nil {
            if let player = value.component(ofType: PlayerComponent.self)?.player {
                if player == EntityManager.instance.player {
                    if let gridPositionComponent = value.component(ofType: GridPositionComponent.self) {
                        EntityManager.instance.camera.position = MapManager.instance.offset2Pixel(hexRow: gridPositionComponent.row, hexCol: gridPositionComponent.column)
                    }
                }
            }
        }
    }
}
