//
// Created by Marcel Henning on 06.05.21.
//

import GameplayKit
import SpriteKit
import GameKit
import os.log

class TileBehavior {
    static let instance = TileBehavior()

    private init() {
    }

    // TODO the all methods that do something with the toucheTile can relate to TileTouchBehavior static touchedTile variable
    /**
     Highlight a tile in player color. the previous highlighted tile will be dehighlighted.
     */
    func highlightTouchedTile(touchedNodeName: String) {
        let touchedName = getRootName(touchedNodeName)
        let touchedTile = getTileEntity(nodeName: touchedName)
        for (_, entity) in MapManager.instance.tileDict {
            setHighlightVisibility(entity: entity, isHidden: true)
        }
        setHighlightVisibility(entity: touchedTile, isHidden: false)
        setHighlightCombatMoveVisibility(entity: touchedTile, isHidden: true)
        setLowlightVisibility(entity: touchedTile, isHidden: true)
    }

    /**
    Highlight a tile in primary color.
    */
    func highlightMoveCombatTouchedTile(entity: TileEntity) {
        for (_, entity) in MapManager.instance.tileDict {
            setHighlightCombatMoveVisibility(entity: entity, isHidden: true)
        }
        setHighlightCombatMoveVisibility(entity: entity, isHidden: false)
        setNeighborVisibility(entity: entity, isHidden: true)
    }

    /**
     Highlight all Player tiles only. The primary Highlighting will be hidden.
     */
    func highlightPlayerTilesOnly(_ hide: Bool) {
        for tile in MapManager.instance.tileDict.values {
            if isPlayerTile(tile) {
                setLowlightVisibility(entity: tile, isHidden: true)
            } else {
                setLowlightVisibility(entity: tile, isHidden: !hide)
            }
            setHighlightVisibility(entity: tile, isHidden: true)
        }
    }

    /**
     Highlight all Neighbors that are not a player or water tile.
     */
    func highlightNeighbor(touchedNodeName: String) {
        let touchedEntity = getTileEntity(nodeName: getRootName(touchedNodeName))
        for (_, entity) in MapManager.instance.tileDict {
            setNeighborVisibility(entity: entity, isHidden: true)
        }
        if !isWater(touchedEntity) && isPlayerTile(touchedEntity) {
            if let gridPosition = touchedEntity.component(ofType: GridPositionComponent.self) {
                let neighbors = getNeighbors(column: gridPosition.column, row: gridPosition.row)
                for neighborEntity in neighbors {
                    if !isPlayerTile(neighborEntity) {
                        setNeighborVisibility(entity: neighborEntity, isHidden: false)
                        setLowlightVisibility(entity: neighborEntity, isHidden: true)
                    }
                }
            }
        }
    }

    /**
     Remove all primary, neighbor, Combat/Move and player tile highlighting from the Map
     */
    func removeAllHighlighting() {
        for (_, tile) in MapManager.instance.tileDict {
            setLowlightVisibility(entity: tile, isHidden: true)
            setHighlightVisibility(entity: tile, isHidden: true)
            setHighlightCombatMoveVisibility(entity: tile, isHidden: true)
            setNeighborVisibility(entity: tile, isHidden: true)
        }
    }

    private func setNeighborVisibility(entity: TileEntity, isHidden: Bool) {
        if let shapeNodes = entity.component(ofType: ShapeComponent.self)?.nodes {
            for shape in shapeNodes where shape.name == getRootName(shape.name!) + ".highlight.neighbor" {
                shape.isHidden = isHidden
            }
        }
    }

    private func setLowlightVisibility(entity: TileEntity, isHidden: Bool) {
        if let shapeNodes = entity.component(ofType: ShapeComponent.self)?.nodes {
            for shapeNode in shapeNodes where shapeNode.name == getRootName(shapeNode.name!) + ".lowlight" {
                shapeNode.isHidden = isHidden
            }
        }
    }

    private func setHighlightVisibility(entity: TileEntity, isHidden: Bool) {
        if let shapeNodes = entity.component(ofType: ShapeComponent.self)?.nodes {
            for shape in shapeNodes where shape.name == getRootName(shape.name!) + ".highlight" {
                shape.strokeColor = EntityManager.instance.player.getColor()
                shape.isHidden = isHidden
            }
        }
    }

    private func setHighlightCombatMoveVisibility(entity: TileEntity, isHidden: Bool) {
        if let shapeNodes = entity.component(ofType: ShapeComponent.self)?.nodes {
            for shape in shapeNodes where shape.name == getRootName(shape.name!) + ".highlight.moveFight" {
                shape.isHidden = isHidden
            }
        }
    }

    private var oddrDirections: [[[Int]]] = [
        [[1, 0], [0, -1], [-1, -1],
         [-1, 0], [-1, +1], [0, 1]],
        [[1, 0], [1, -1], [0, -1],
         [-1, 0], [0, 1], [1, 1]]]

    private func offsetNeighbor(column: Int, row: Int, direction: Int) -> (Int, Int) {

        let parity = row & 1
        let dir = oddrDirections[parity][direction]
        return (column + dir[0], row + dir[1])
    }

    /**
    A function to get all neigbors of a tile as an array of TileEntities
    */
    func getNeighbors(column: Int, row: Int) -> [TileEntity] {
        var neighbors: [TileEntity] = []
        for index in 0...5 {
            let (deltaColumn, deltaRow) = offsetNeighbor(column: column, row: row, direction: index)
            neighbors.append(MapManager.instance.tileDict[String(deltaRow) + "-" + String(deltaColumn)]!)
        }
        return neighbors
    }

    /**
    Check if two tiles are neighbors.
     */
    func isNeighbor(firstEntity: TileEntity, secondEntity: TileEntity) -> Bool {
        for index in 0...5 {
            let (deltaColumn, deltaRow) = offsetNeighbor(
                    column: firstEntity.component(ofType: GridPositionComponent.self)!.column,
                    row: firstEntity.component(ofType: GridPositionComponent.self)!.row,
                    direction: index)
            if secondEntity.component(ofType: GridPositionComponent.self)!.column == deltaColumn &&
                       secondEntity.component(ofType: GridPositionComponent.self)!.row == deltaRow {
                return true
            }
        }
        return false
    }

    /**
    Get the root Name of a TileEntity Component. Removes all suffixes and returns the name as "row-column"
     */
    func getRootName(_ name: String) -> String {
        String(name.split(separator: ".")[0])
    }

    private func getTileEntity(nodeName: String) -> TileEntity {
        MapManager.instance.tileDict[nodeName] ?? TileEntity()
    }

    /**
    Check if the provided Entity is Water.
    Returns true if the Entity is Water
    */
    // TODO dont use GKEntity here use TileEntity
    func isWater(_ entity: GKEntity) -> Bool {
        if let gridPosition = entity.component(ofType: GridPositionComponent.self) {
            return gridPosition.tileType == TileType.water
        } else {
            return false
        }
    }

    /**
     Check if the provided Entity is a player tile.
     Returns true if the Entity has the same Player as the currentPlayer
     */

    // TODO dont use GKEntity here use TileEntity
    func isPlayerTile(_ entity: GKEntity) -> Bool {
        if let player = entity.component(ofType: PlayerComponent.self) {
            return player.player == EntityManager.instance.player
        } else {
            return false
        }
    }

    /**
     Place troops that are added to the Tiles and set them as current Troops.
     Current Troops are the troops that are available on the tile to fight.
     Added Troops are the troops that are reserved on the file from the reinforcement.
     */
    func placeAddedTroops() {
        for (key, tileEntity) in MapManager.instance.tileDict {
            if let troops = tileEntity.component(ofType: TroopCountComponent.self) {
                troops.currentTroops += troops.addedTroops
                if (troops.addedTroops != 0) {
                    if let owner = tileEntity.component(ofType: PlayerComponent.self)?.player {
                        GameCenterManager.instance.registerTroopChange(position: key, troopCount: troops.currentTroops, owner: owner)
                    }
                }
                troops.addedTroops = 0
            }
        }
        TroopBehavior.instance.addPlacedTroops(count: -TroopBehavior.instance.getPlacedTroops())
        TroopBehavior.instance.updateTroopsCountLabel()
    }

    /**
     Reserve Troops on a Tile. This troops will be added when the placeAddedTroops Method is called.
     */
    func reserveTroops(tile: TileEntity, amount: Int) {
        if TroopBehavior.instance.getPlacedTroops() < TroopBehavior.instance.getAvailableTroops() {
            TroopBehavior.instance.addPlacedTroops(count: amount)
            tile.component(ofType: TroopCountComponent.self)?.addedTroops += amount

        } else {
            // TODO Placeholder Fehlermeldung sollte auf einem Label oä angezeigt werden
            os_log("Keine Truppen mehr verfügbar", type: .info)
        }
    }

    /**
    Remove reserve Troops on a Tile. If there are troops left they will be added when the placeAddedTroops Method is called.
    */
    func removeReservedTroops(tile: TileEntity, amount: Int) {
        if (TroopBehavior.instance.getPlacedTroops() > 0) &&
                   (tile.component(ofType: TroopCountComponent.self)!.addedTroops) > 0 {
            TroopBehavior.instance.addPlacedTroops(count: -amount)
            tile.component(ofType: TroopCountComponent.self)!.addedTroops -= amount
        } else {
            // TODO Placeholder Fehlermeldung sollte auf einem Label oä angezeigt werden
            os_log("Sie können keine Truppen mehr entfernen", type: .info)
        }

    }

    /**
 Reserve Troops to Move or to Fight on a Tile. This troops will be added when the placeAddedTroops Method is called.
 */
    func reserveFightMoveTroops(sourceTile: TileEntity, destinationTile: TileEntity, amount: Int) {
        if sourceTile.component(ofType: TroopCountComponent.self)!.currentTroops + sourceTile.component(ofType: TroopCountComponent.self)!.addedTroops - amount > 0 {
            sourceTile.component(ofType: TroopCountComponent.self)?.addedTroops -= amount
            destinationTile.component(ofType: TroopCountComponent.self)?.addedTroops += amount
            TroopBehavior.instance.addPlacedTroops(count: amount)
        } else {
            os_log("Es können nicht mehr Truppen verschoben werden", type: .info)
        }
    }

    /**
    Reserve all available Troops to Move or to Fight on a Tile.
*/
    func reserveAllAvailableFightMoveTroops(sourceTile: TileEntity, destinationTile: TileEntity) {
        if sourceTile.component(ofType: TroopCountComponent.self)!.currentTroops + sourceTile.component(ofType: TroopCountComponent.self)!.addedTroops > 0 {
            if var sourceTileCurrentTroops = sourceTile.component(ofType: TroopCountComponent.self)?.currentTroops {
                let troopChange = sourceTileCurrentTroops - destinationTile.component(ofType: TroopCountComponent.self)!.addedTroops - 1
                destinationTile.component(ofType: TroopCountComponent.self)?.addedTroops += troopChange
                sourceTile.component(ofType: TroopCountComponent.self)?.addedTroops -= troopChange
                TroopBehavior.instance.addPlacedTroops(count: troopChange)
            }
        } else {
            os_log("Es können nicht mehr Truppen verschoben werden", type: .info)
        }
    }

    func upgradeCityTile(key: String, level: Int) {
        for (tileKey, tile) in MapManager.instance.tileDict where tileKey == key {
            if let cityComponent = tile.component(ofType: CityTileComponent.self) {
                cityComponent.cityLevel = level
                var newTileType = TileType.cityWall0
                switch level {
                case 1:
                    newTileType = TileType.cityWall0
                case 2:
                    newTileType = TileType.cityWall1
                case 3:
                    newTileType = TileType.cityWall2
                default:
                    break
                }
                tile.component(ofType: GridPositionComponent.self)!.tileType = newTileType
                tile.component(ofType: SpriteComponent.self)!.nodes[0].texture = SKTexture.init(imageNamed: newTileType.getImageName())
            }
        }
    }

    func setMainCityTile() {
        for (key, value) in MapManager.instance.tileDict where value.component(ofType: CityTileComponent.self) != nil {
            if let player = value.component(ofType: PlayerComponent.self)?.player {
                if player == EntityManager.instance.player {
                    os_log("Main City is at: %s", type: .info, key)
                    value.component(ofType: CityTileComponent.self)!.isMainCity = true
                }
            }
        }
    }

    func getMainCityTile() -> TileEntity {
        for (_, value) in MapManager.instance.tileDict where value.component(ofType: CityTileComponent.self) != nil {
            if let player = value.component(ofType: PlayerComponent.self)?.player {
                if player == EntityManager.instance.player {
                    if (value.component(ofType: CityTileComponent.self)!.isMainCity == true) {
                        return value
                    }
                }
            }
        }
        return TileEntity()
    }

}
