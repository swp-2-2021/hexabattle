//
// Created by Marvin Klein on 06.05.21.
//

import SpriteKit
import GameKit

class EmpireBehavior {

    static let instance = EmpireBehavior()

    private init() {
    }

    func updateEmpireSizeLabel() {
        var counter = 0
        for tile in MapManager.instance.tileDict
            where tile.value.component(ofType: PlayerComponent.self)?.player == EntityManager.instance.player {
            counter += 1
        }
        getEntity().component(ofType: EmpireComponent.self)!.tilesCount = counter
        getEntity().component(ofType: LabelComponent.self)?.nodes.first?.text = String(getEmpireSize())
    }

    func getEmpireSize() -> Int {
        getEntity().component(ofType: EmpireComponent.self)!.tilesCount
    }

    func getEntity() -> GKEntity {
        EntityManager.instance.entityDict[Names.labelEmpireSize]!
    }

}
