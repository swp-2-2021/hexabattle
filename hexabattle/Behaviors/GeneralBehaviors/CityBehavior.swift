//
// Created by Marvin Klein on 06.05.21.
//

import SpriteKit
import GameKit

class CityBehavior {

    static let instance = CityBehavior()

    private init() {
    }

    func updateCitiesOccupiedLabel() {
        var counter = 0
        for tile in MapManager.instance.tileDict
            where (tile.value.component(ofType: PlayerComponent.self)?.player == EntityManager.instance.player
                    && (tile.value.component(ofType: CityTileComponent.self) != nil)) {
            counter += 1
        }
        getEntity().component(ofType: CityComponent.self)!.cities = counter
        getEntity().component(ofType: LabelComponent.self)?.nodes.first?.text = String(getCitiesCount())
    }

    func getCitiesCount() -> Int {
        getEntity().component(ofType: CityComponent.self)!.cities
    }

    func getEntity() -> GKEntity {
        EntityManager.instance.entityDict[Names.labelOccupiedCities]!
    }

}
