//
// Created by Marvin Klein on 06.05.21.
//

import SpriteKit
import GameKit

class TroopBehavior {

    static let instance = TroopBehavior()

    private init() {
    }

    func updateTroopsCountLabel() {
        var counter = 0
        for tile in MapManager.instance.tileDict
            where tile.value.component(ofType: PlayerComponent.self)?.player == EntityManager.instance.player {
            counter += tile.value.component(ofType: TroopCountComponent.self)!.currentTroops
        }
        getHUDEntity().component(ofType: TroopComponent.self)!.troops = counter
        getHUDEntity().component(ofType: LabelComponent.self)?.nodes.first?.text = String(getTotalTroopsCount())
    }

    func addAvailableTroops(count: Int) {
        getPlusMinusEntity().component(ofType: GlobalTroopsComponent.self)!.troopsAvailable += count
    }

    func getAvailableTroops() -> Int {
        getPlusMinusEntity().component(ofType: GlobalTroopsComponent.self)!.troopsAvailable
    }

    func addPlacedTroops(count: Int) {
        getPlusMinusEntity().component(ofType: GlobalTroopsComponent.self)!.troopsPlaced += count
    }

    func getPlacedTroops() -> Int {
        getPlusMinusEntity().component(ofType: GlobalTroopsComponent.self)!.troopsPlaced
    }

    func getTotalTroopsCount() -> Int {
        getHUDEntity().component(ofType: TroopComponent.self)!.troops
    }

    func getHUDEntity() -> GKEntity {
        EntityManager.instance.entityDict[Names.labelArmySize]!
    }

    func getPlusMinusEntity() -> GKEntity {
        if let plusMinusReinforcement = EntityManager.instance.entityDict[Names.changeTroops] {
            return plusMinusReinforcement
        }
        if let plusMinusCombat = EntityManager.instance.entityDict[Names.entityMoveFight] {
            return plusMinusCombat
        }
        return GKEntity()
    }

    // Each of the attacking troops has a possibility to kill a defending troop and each defending troop has a possibility to kill an attacking troop
    // The attacker only wins a fight, if he kills all the defending troops on the enemy Tile ->
    // If the attacker loses, each Player (attacker/defender) will lose the troops which are killed in the fight
    func fight(sourceTile: TileEntity, destinationTile: TileEntity) {
        // TODO Better logic with states and implement what is to do in the states
        // Neutral Tile
        if destinationTile.component(ofType: PlayerComponent.self)?.player == Player.none {
            MapManager.instance.occupyTile(tileName: destinationTile.name,
                    newPlayer: EntityManager.instance.player
            )
            TileBehavior.instance.placeAddedTroops()
            // winning condition
        } else {
            // random fight
            var killedDefenders = 0
            var killedAttackers = 0

            // kill possibilites in %
            // Default Values for grassland
            var killPossibilityAttackers = 70
            var killPossibilityDefenders = 70

            // get Tile type, where the fight takes place
            if let gridPosComponent = destinationTile.component(ofType: GridPositionComponent.self) {
                let tileType = gridPosComponent.tileType

                // change kill possibilities dependent on th Tile
                switch tileType {
                case .mountain:
                    killPossibilityDefenders = 85
                case .forest:
                    killPossibilityAttackers = 85
                case .cityWall1:
                    killPossibilityAttackers = 65
                    killPossibilityDefenders = 80
                case .cityWall2:
                    killPossibilityAttackers = 55
                    killPossibilityDefenders = 90
                default:
                    break
                }
            }

            for _ in 1...destinationTile.component(ofType: TroopCountComponent.self)!.addedTroops {
                if Int.random(in: 1...100) <= killPossibilityAttackers {
                    killedDefenders += 1
                }
            }
            for _ in 1...destinationTile.component(ofType: TroopCountComponent.self)!.currentTroops {
                if Int.random(in: 1...100) <= killPossibilityDefenders {
                    killedAttackers += 1
                }
            }

            if killedAttackers > destinationTile.component(ofType: TroopCountComponent.self)!.addedTroops {
                killedAttackers = destinationTile.component(ofType: TroopCountComponent.self)!.addedTroops
            }

            // You win, if you kill all defenders, otherwise the fight is lost
            if (destinationTile.component(ofType: TroopCountComponent.self)!.currentTroops - killedDefenders) <= 0 {
                killedDefenders = destinationTile.component(ofType: TroopCountComponent.self)!.currentTroops

                sourceTile.component(ofType: TroopCountComponent.self)?.addedTroops = -1 * destinationTile.component(ofType: TroopCountComponent.self)!.addedTroops
                destinationTile.component(ofType: TroopCountComponent.self)!.currentTroops = 0
                destinationTile.component(ofType: TroopCountComponent.self)?.addedTroops -= killedAttackers

                if (destinationTile.component(ofType: TroopCountComponent.self)?.addedTroops == 0) {
                    destinationTile.component(ofType: TroopCountComponent.self)?.addedTroops = 1
                }

                GameCenterMatchManager().registerWinnerLooser(
                        winner: EntityManager.instance.player,
                        looser: destinationTile.component(ofType: PlayerComponent.self)!.player,
                        killedAttackers: killedAttackers,
                        killedDefenders: killedDefenders
                )

                MapManager.instance.occupyTile(tileName: destinationTile.name,
                        newPlayer: EntityManager.instance.player
                )
                TileBehavior.instance.placeAddedTroops()

                // losing condition
            } else {
                sourceTile.component(ofType: TroopCountComponent.self)?.addedTroops = -1 * killedAttackers
                destinationTile.component(ofType: TroopCountComponent.self)?.addedTroops = -1 * killedDefenders
                GameCenterMatchManager().registerWinnerLooser(
                        winner: destinationTile.component(ofType: PlayerComponent.self)!.player,
                        looser: EntityManager.instance.player,
                        killedAttackers: killedAttackers,
                        killedDefenders: killedDefenders
                )

                TileBehavior.instance.placeAddedTroops()
            }
        }
        EmpireBehavior.instance.updateEmpireSizeLabel()
        CityBehavior.instance.updateCitiesOccupiedLabel()
    }

}
