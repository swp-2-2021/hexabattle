//
// Created by Marcel Henning on 18.06.21.
//

import SpriteKit
import GameplayKit

class InitUiBehavior {

    static let instance = InitUiBehavior()

    var frame: CGRect = CGRect()

    private init() {
    }

    func initPhaseAndPlayerUI() {
        // Phase Label
        let phaseAndLabel = PhaseAndLabelEntity(
                position: cameraToScenePoint(x: 250, y: 930),
                backgroundZPosition: ZPosition.hudElementBackground,
                labelZPosition: ZPosition.hud1,
                size: CGSize(width: 450, height: 90),
                text: "",
                labelColor: UIColor.hexaDarkRed,
                backgroundColor: UIColor.hexaDarkBlue,
                name: Names.hudPlayerAndPhase)
        EntityManager.instance.addToCamera(phaseAndLabel)

    }

    func cameraToScenePoint(x: CGFloat, y: CGFloat) -> CGPoint {
        CGPoint(x: (x - frame.midX), y: (y - frame.midY))
    }
}
