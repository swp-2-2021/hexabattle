//
// Created by Marcel Henning on 28.06.21.
//

import Foundation
import SpriteKit

protocol AbstractSliderBehavior {
    func sliderValueDidChange(_ sender: UISlider!)
}

extension AbstractSliderBehavior {
    func sliderValueDidChange(_ sender: UISlider!) {

    }
}
