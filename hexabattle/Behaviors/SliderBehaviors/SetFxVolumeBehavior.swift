//
// Created by Marcel Henning on 28.06.21.
//

import Foundation
import SceneKit

class SetFxVolumeBehavior: AbstractSliderBehavior {
    func sliderValueDidChange(_ sender: UISlider!) {
        SoundManager.instance.setFxVolume(sender.value)
        SoundManager.instance.playSoundEffect(soundName: Names.soundButtonClick, volumeLevel: GameValues.volumeButtonClick)
    }
}
