//
// Created by Marcel Henning on 28.06.21.
//

import Foundation
import SceneKit

class SetAudioVolumeBehavior: AbstractSliderBehavior {
    func sliderValueDidChange(_ sender: UISlider!) {
           SoundManager.instance.setAudioVolume(sender.value)
    }
}
