//
//  HideInGameHelpTouchBehavior.swift
//  hexabattle
//
//  Created by 91elhe1bif on 06.06.21.
//

import GameplayKit
import SpriteKit

class HideInGameHelpTouchBehavior: AbstractTouchBehavior {

    var entityName: String

    init(entityName: String) {
        self.entityName = entityName
    }

    func executeTap(touchedNodeName: String ) {
        InGameHelpManager.instance.isHelpEnabled = false
        EntityManager.instance.bundles[Names.bundleInGameHelp]?.hideBundle(hide: true)
    }
}
