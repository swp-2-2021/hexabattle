//
//  ShowInGameHelp.swift
//  hexabattle
//
//  Created by 91elhe1bif on 06.06.21.
//

import GameplayKit
import SpriteKit

class ShowInGameHelpTouchBehavior: AbstractTouchBehavior {

    var entityName: String

    init(entityName: String) {
        self.entityName = entityName
    }

    func executeTap(touchedNodeName: String ) {
        InGameHelpManager.instance.isHelpEnabled = true
        EntityManager.instance.bundles[Names.bundleInGameHelp]?.hideBundle(hide: false)
    }
}
