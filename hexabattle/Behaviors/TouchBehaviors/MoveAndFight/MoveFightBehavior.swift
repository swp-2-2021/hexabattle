//
//  MoveFightBehavior.swift
//  hexabattle
//
//  Created by Dejan Mijic on 09.05.21.
//

import GameplayKit
import SpriteKit
import os.log

class MoveFightBehavior: AbstractTouchBehavior {

    var entityName: String

    init(entityName: String) {
        self.entityName = entityName
    }

    func executeTap(touchedNodeName: String) {
        (GameStateMachine.instance.currentState as! AbstractGameState).setTroops(touchedNodeName: touchedNodeName)
    }

    func executeHold(touchedNodeName: String) {
        (GameStateMachine.instance.currentState as! AbstractGameState).setAllTroops(touchedNodeName: touchedNodeName)
    }
}
