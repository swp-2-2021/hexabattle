//
//  ReduceTroopsBehaviour.swift
//  hexabattle
//
//  Created by Dejan Mijic on 27.04.21.
//

import GameplayKit
import SpriteKit

class ReduceTroopsTouchBehavior: AbstractTouchBehavior {

    var entityName: String

    init(entityName: String) {
        self.entityName = entityName
    }

    func executeTap(touchedNodeName: String ) {
        if EntityManager.instance.troopsAmount > 1 {
        EntityManager.instance.troopsAmount -= 1
        EntityManager.instance.updateLabel(labelName: entityName, newText: String(EntityManager.instance.troopsAmount) + " x " + String(GameValues.priceRecruitTroop))
        EntityManager.instance.updateLabel(labelName: Names.labelTotalTroopPrice, newText: "Total: " + String(EntityManager.instance.troopsAmount * GameValues.priceRecruitTroop))
        }
    }

}
