//
//  TouchesBeganBehavior.swift
//  hexabattle
//
//  Created by Marvin Klein on 18.04.21.
//

import GameplayKit
import SpriteKit

class BuyTroopsTouchBehavior: AbstractTouchBehavior {

    var entityName: String

    init(entityName: String) {
        self.entityName = entityName
    }

    func executeTap(touchedNodeName: String ) {
        let totalValue: Int = EntityManager.instance.troopsAmount * GameValues.priceRecruitTroop
        let transactionSucceeded: Bool =  GoldBehavior.instance.deductGold(amount: totalValue)
        if(transactionSucceeded) {
            SoundManager.instance.playSoundEffect(soundName: Names.soundCoin, volumeLevel: GameValues.volumeCoin)
            TroopBehavior.instance.addAvailableTroops(count: EntityManager.instance.troopsAmount)
            EntityManager.instance.updateLabel(labelName: Names.labelTransactionStatus, newText: String("The purchase was successfully completed"), newColor: UIColor.hexaWhite)
            EntityManager.instance.troopsAmount = 1
            EntityManager.instance.updateLabel(labelName: Names.labelTroopsPriceTag, newText: String(EntityManager.instance.troopsAmount) + " x " + String(GameValues.priceRecruitTroop))
            EntityManager.instance.updateLabel(labelName: Names.labelTotalTroopPrice, newText: "Total: " + String(EntityManager.instance.troopsAmount * GameValues.priceRecruitTroop))
        } else {
            EntityManager.instance.updateLabel(labelName: Names.labelTransactionStatus, newText: String("You don't have enough gold to make the purchase"), newColor: UIColor.hexaDarkRed)
        }
    }
    func tapSound() {
       }
}
