//
//  showShopBehavior.swift
//  hexabattle
//
//  Created by Dejan Mijic on 27.04.21.
//

import GameplayKit
import SpriteKit

class ShowShopTouchBehavior: AbstractTouchBehavior {

    var entityName: String

    init(entityName: String) {
        self.entityName = entityName
    }

    func executeTap(touchedNodeName: String ) {
        GameStateMachine.instance.enter(ReinforcementShopOpenState.self)
    }

}
