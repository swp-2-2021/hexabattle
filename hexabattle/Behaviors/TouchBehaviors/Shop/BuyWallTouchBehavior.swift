//
//  BuyWallBehavior.swift
//  hexabattle
//
//  Created by Dejan Mijic on 27.04.21.
//

import GameplayKit
import GameKit
import SpriteKit
import os.log

class BuyWallTouchBehavior: AbstractTouchBehavior {
    var entityName: String

    init(entityName: String) {
        self.entityName = entityName
    }

    func tapSound() {
    }

    func executeTap(touchedNodeName: String) {
        var mainCityTile: TileEntity = TileEntity.init()
        for (_, value) in MapManager.instance.tileDict
            where value.component(ofType: CityTileComponent.self)?.isMainCity == true && value.component(ofType: PlayerComponent.self)!.player == EntityManager.instance.player {
            mainCityTile = value
        }

        guard let cityTileComponent = mainCityTile.component(ofType: CityTileComponent.self) else {
            return
        }

        if (cityTileComponent.cityLevel < 3) {
            let transactionSucceeded: Bool = GoldBehavior.instance.deductGold(amount: GameValues.priceCityUpgrade)
            if transactionSucceeded {
                SoundManager.instance.playSoundEffect(soundName: Names.soundCoin, volumeLevel: GameValues.volumeCoin)

                if cityTileComponent.cityLevel <= 2 {
                    cityTileComponent.cityLevel += 1

                    TileBehavior.instance.upgradeCityTile(key: mainCityTile.name, level: cityTileComponent.cityLevel)

                    if (cityTileComponent.cityLevel == 3) {
                        EntityManager.instance.updateLabel(
                                labelName: entityName,
                                newText: "Maximum Level")
                    } else {
                        EntityManager.instance.updateLabel(
                                labelName: entityName,
                                newText: "Lvl. \(cityTileComponent.cityLevel) → Lvl. \(cityTileComponent.cityLevel + 1)")
                    }
                    GameCenterManager.instance.localCityUpgrades = [mainCityTile.name: cityTileComponent.cityLevel]
                    EntityManager.instance.updateLabel(labelName: Names.labelTransactionStatus, newText: String("The purchase was successfully completed"), newColor: UIColor.hexaWhite)

                } else {
                    os_log("Can't upgrade any further", type: .info)
                }

            } else {
                EntityManager.instance.updateLabel(labelName: Names.labelTransactionStatus, newText: String("You don't have enough gold to make the purchase"), newColor: UIColor.hexaDarkRed)
            }
        } else {
            EntityManager.instance.updateLabel(labelName: Names.labelTransactionStatus, newText: String("Can't upgrade any further"), newColor: UIColor.hexaDarkRed)
        }
    }
}
