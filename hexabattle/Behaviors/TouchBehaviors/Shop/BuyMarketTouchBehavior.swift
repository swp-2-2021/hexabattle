//
//  BuyMarketBehavior.swift
//  hexabattle
//
//  Created by Dejan Mijic on 27.04.21.
//

import GameplayKit
import SpriteKit

class BuyMarketTouchBehavior: AbstractTouchBehavior {

    var entityName: String

    init(entityName: String) {
        self.entityName = entityName
    }

    func executeTap(touchedNodeName: String) {
        (GameStateMachine.instance.currentState as! AbstractGameState).buyMarketUpgrade()
    }

    func tapSound() {
    }
}
