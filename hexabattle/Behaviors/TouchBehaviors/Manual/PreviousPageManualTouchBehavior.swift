//
//  PreviousPageManualBehavior.swift
//  hexabattle
//
//  Created by Dejan Mijic on 15.06.21.
//

import GameplayKit
import SpriteKit

class PreviousPageManualTouchBehavior: AbstractTouchBehavior {

    var entityName: String

    init(entityName: String) {
        self.entityName = entityName
    }

    func executeTap(touchedNodeName: String ) {
        EntityManager.instance.bundles[Names.bundleManual2]?.hideBundle(hide: true)
        EntityManager.instance.bundles[Names.bundleManual]?.hideBundle(hide: false)
        EntityManager.instance.bundles[Names.bundleManual]?.hideBundleEntityByName(hide: true, name: "previousPageButton")
    }
}
