//
//  Created by Martin Uhlig on 28.04.21.
//

import GameplayKit
import SpriteKit

class QuitGameTouchBehavior: AbstractTouchBehavior {

    var entityName: String

    init(entityName: String) {
        self.entityName = entityName
    }

    func executeTap(touchedNodeName: String) {
        (GameStateMachine.instance.currentState as! AbstractGameState).leaveGame()
        StateMachine.instance.enter(StartMenuState.self)
    }

}
