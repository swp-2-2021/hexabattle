//
//  Created by Martin Uhlig on 28.04.21.
//

import GameplayKit
import SpriteKit

class SoundOffTouchBehavior: AbstractTouchBehavior {

    var entityName: String

    init(entityName: String) {
        self.entityName = entityName
    }

    func executeTap(touchedNodeName: String ) {
        SoundManager.instance.stopBackgroundMusic()
    }

}
