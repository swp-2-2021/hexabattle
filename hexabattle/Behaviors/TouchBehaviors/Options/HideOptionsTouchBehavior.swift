//
//  Created by Martin Uhlig on 28.04.21.
//

import GameplayKit
import SpriteKit

class HideOptionsTouchBehavior: AbstractTouchBehavior {

    var entityName: String

    init(entityName: String) {
        self.entityName = entityName
    }

    func executeTap(touchedNodeName: String ) {
        EntityManager.instance.bundles[Names.bundleOptions]?.hideBundle(hide: true)
        GameBehavior.instance.enablePanning(value: true)
    }
}
