//
// Created by Marcel Henning on 22.04.21.
//

import GameplayKit
import SpriteKit

class TileTouchBehavior: AbstractTouchBehavior {

    // last touchedTile
    static var previousTouchedTile = TileEntity()
    // saves the touched tile until another tile is touched
    static var touchedTile = TileEntity()

    init() {
    }

    func executeTap(touchedNodeName: String) {
        (GameStateMachine.instance.currentState as! AbstractGameState).touchTile(touchedNodeName: touchedNodeName)
    }

    // Override disable Button Sound click
    func tapSound() {}
}
