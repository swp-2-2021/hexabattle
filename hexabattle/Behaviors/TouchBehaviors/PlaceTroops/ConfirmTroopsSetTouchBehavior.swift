//
// Created by Tom Biskupski on 27.04.21.
//

class ConfirmTroopsSetTouchBehavior: AbstractTouchBehavior {
// Behavior to confirm the troops that are added in the reinforcement phase and places the troops
    var entityName: String

    init(entityName: String) {
        self.entityName = entityName
    }

    func executeTap(touchedNodeName: String) {
        (GameStateMachine.instance.currentState as! AbstractGameState).setTroops(touchedNodeName: touchedNodeName)
    }
}
