//
// Created by student on 14.05.21.
//

import Foundation

// Behavior to finish reinforcement and enter transition state before timer expires
class FinishReinforcementTouchBehaviour: AbstractTouchBehavior {

    var entityName: String

    init(entityName: String) {
        self.entityName = entityName
    }

    func executeTap(touchedNodeName: String) {
        StateMachine.instance.enter(TransitionState.self)
    }
}
