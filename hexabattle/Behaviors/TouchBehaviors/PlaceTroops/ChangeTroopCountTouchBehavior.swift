//
// Created by Tom Biskupski on 27.04.21.
//

import GameplayKit
import SpriteKit
import os.log

class ChangeTroopCountTouchBehavior: AbstractTouchBehavior {

    var entityName: String

    init(entityName: String) {
        self.entityName = entityName
    }

    func executeTap(touchedNodeName: String) {
        (GameStateMachine.instance.currentState as! AbstractGameState).setTroops(touchedNodeName: touchedNodeName)
    }

    func executeHold(touchedNodeName: String) {
        (GameStateMachine.instance.currentState as! AbstractGameState).setAllTroops(touchedNodeName: touchedNodeName)
    }
}
