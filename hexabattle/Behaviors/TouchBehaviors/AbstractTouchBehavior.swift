//
//  Behavior.swift
//  hexabattle
//
//  Created by Marvin Klein on 18.04.21.
//

protocol AbstractTouchBehavior {
    func executeTap(touchedNodeName: String)
    func executeHold(touchedNodeName: String)
    func tapSound()
    func holdSound()
}

extension AbstractTouchBehavior {
    func executeTap(touchedNodeName: String) {
    }

    func executeHold(touchedNodeName: String) {
    }

    func tapSound() {
        SoundManager.instance.playSoundEffect(soundName: Names.soundButtonClick, volumeLevel: GameValues.volumeButtonClick)
    }

    func holdSound() {
    }

    func didTap(touchedNodeName: String) {
        tapSound()
        executeTap(touchedNodeName: touchedNodeName)
    }

    func didHold(touchedNodeName: String) {
        holdSound()
        executeHold(touchedNodeName: touchedNodeName)
    }
}
