//
// Created by student on 03.06.21.
//

import Foundation
import os.log
import GameKit

class MatchDelegate: NSObject, GKMatchDelegate {

    private static let logObject = OSLog(subsystem: OSLog.subsystem, category: "gamecenter.match.delegate")

    func match(_ match: GKMatch, didReceive data: Data, fromRemotePlayer player: GKPlayer) {
        os_log("didReceive fromRemotePlayer '%s'", log: MatchDelegate.logObject, type: .info, player.displayName)

        handleData(match, data, from: player)
    }

    func match(_ match: GKMatch, didReceive data: Data, forRecipient recipient: GKPlayer, fromRemotePlayer player: GKPlayer) {
        os_log("didReceive forRecipient '%s' fromRemotePlayer '%s'", log: MatchDelegate.logObject, type: .info,
                recipient.displayName, player.displayName)

        handleData(match, data, from: player)
    }

    private func handleData(_ match: GKMatch, _ data: Data, from player: GKPlayer) {
        let identifier: Action = data.toModel()

        os_log("handling action %s", log: MatchDelegate.logObject, type: .info, identifier.type.rawValue)

        // Could be done better with some sort of generics.
        // Because the enum types each map to a specific class
        switch identifier.type {
        case ActionType.hostFound:
            let action: HostFoundAction = data.toModel()
            action.apply(GameCenterManager.instance, match, from: player)

        case ActionType.playerOrder:
            let action: PlayerOrderDeterminedAction = data.toModel()
            action.apply(GameCenterManager.instance, match, from: player)

        case ActionType.finishLocalReinforcement:
            let action: FinishLocalReinforcementAction = data.toModel()
            action.apply(GameCenterManager.instance, match, from: player)

        case ActionType.finishReinforcement:
            let action: FinishReinforcementAction = data.toModel()
            action.apply(GameCenterManager.instance, match, from: player)

        case ActionType.combatMove:
            let action: CombatMoveAction = data.toModel()
            action.apply(GameCenterManager.instance, match, from: player)

        case ActionType.combatFight:
            let action: CombatFightAction = data.toModel()
            action.apply(GameCenterManager.instance, match, from: player)

        case ActionType.combatNone:
            let action: CombatNoneAction = data.toModel()
            action.apply(GameCenterManager.instance, match, from: player)

        default:
            fatalError("Missing case " + identifier.type.rawValue)
        }
    }

    func match(_ match: GKMatch, player: GKPlayer, didChange state: GKPlayerConnectionState) {
        os_log("player '%s' didChange '%s'", log: MatchDelegate.logObject, type: .info, player.displayName, state.toString())
        let gcm = GameCenterManager.instance

        switch state {
        case .disconnected:
            handleDisconnection(player: player, gcm: gcm)
        default: break
        }
    }

    private func handleDisconnection(player: GKPlayer, gcm: GameCenterManager) {
        if let gamePlayer = EntityManager.instance.playerDict[player.playerID] {
            os_log("Removed %s from map", log: MatchDelegate.logObject, type: .info, gamePlayer.toString())
            GameBehavior.instance.removePlayerFromMap(player: gamePlayer)
        }

        let gameOver = GameBehavior.instance.isGameOver()
        os_log("Game is over: %s", log: MatchDelegate.logObject, type: .info, String(gameOver))

        if !gameOver {
            let playerOrderIndex = gcm.playerOrder!.firstIndex(of: player.playerID)!
            gcm.playerOrder!.remove(at: playerOrderIndex)

            if gcm.host!.playerID == player.playerID {
                gcm.determineHost()
            } else {
                os_log("Disconnecting player was not host", log: MatchDelegate.logObject, type: .info)
            }

            if gcm.isHost {
                os_log("Local player is host", log: MatchDelegate.logObject, type: .info)

                let gcmm = GameCenterMatchManager()
                // Not using inheritance model of game state to perform different actions for different states,
                // because the actions need to access internal state of game center classes
                if let currentState = StateMachine.instance.currentState {
                    if currentState.isKind(of: ReinforcementState.self)
                               && gcmm.isReinforcementFinished() {
                        gcmm.finishReinforcementForAll()

                    } else if (currentState.isKind(of: CombatState.self)
                            || currentState.isKind(of: OtherPlayerTurnState.self))
                                      && gcm.currentPlayerId == player.playerID {
                        let combatInfo = gcmm.determineNextPlayerIdAndCombatRound(currentPlayerIndex: playerOrderIndex - 1)
                        gcmm.finishCombatWithNone(combatInfo: combatInfo)
                    } else if (currentState.isKind(of: TransitionState.self) && playerOrderIndex == 0) {
                        gcmm.finishReinforcementForAll()
                    }
                }
            } else {
                os_log("Local player is not host, finished handling disconnecting player",
                        log: MatchDelegate.logObject, type: .info)
            }
        }
    }

    func match(_ match: GKMatch, didFailWithError error: Error?) {
        os_log("didFailWithError %s", log: MatchDelegate.logObject, type: .info,
                (error as CustomDebugStringConvertible).debugDescription)
    }

    func match(_ match: GKMatch, shouldReinviteDisconnectedPlayer player: GKPlayer) -> Bool {
        // Don't try to reconnect
        false
    }
}

private extension GKPlayerConnectionState {
    func toString() -> String {
        switch self {
        case .unknown:
            return "unknown"
        case .connected:
            return "connected"
        case .disconnected:
            return "disconnected"
        @unknown default:
            return "unknown"
        }
    }
}
