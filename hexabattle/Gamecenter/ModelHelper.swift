//
// Created by student on 14.05.21.
//

import Foundation

extension Data {

    func toModel<T>() -> T where T: Decodable {
        do {
            return try JSONDecoder().decode(T.self, from: self)
        } catch {
            fatalError("Failed to decode data to game model.\n\(debugDescription)")
        }
    }

    static func fromModel<T>(_ value: T) -> Data where T: Encodable {
        do {
            return try JSONEncoder().encode(value)
        } catch {
            fatalError("Failed to encode game model to data.\n\((error as CustomDebugStringConvertible).debugDescription)")
        }
    }

}
