//
// Created by student on 03.06.21.
//

import Foundation
import os.log
import GameKit

class GameCenterAuthenticator {

    private static let logObject = OSLog(subsystem: OSLog.subsystem, category: "gamecenter.authenticator")

    func initAuthenticationHandler() {
        GKLocalPlayer.local.authenticateHandler = { viewController, error in
            if let error = error {
                os_log("Auth error: %s", log: GameCenterAuthenticator.logObject, type: .error,
                        (error as CustomDebugStringConvertible).debugDescription)
            } else if let authVC = viewController {
                os_log("Showing login view controller to user", log: GameCenterAuthenticator.logObject, type: .info)
                let rootVC = UIApplication.shared.windows.first!.rootViewController!
                rootVC.present(authVC, animated: true)
            } else {
                os_log("User is already authenticated", log: GameCenterAuthenticator.logObject, type: .info)
                GKLocalPlayer.local.unregisterAllListeners()
                GKLocalPlayer.local.register(GameCenterManager.instance.inviteListener)
            }
        }
    }
}
