//
// Created by student on 04.06.21.
//

import Foundation
import os.log
import GameKit

internal class GameCenterMatchManager {

    private static let logObject = OSLog(subsystem: OSLog.subsystem, category: "gamecenter.match")

    func determinePlayerOrder(_ match: GKMatch) {
        var players = match.players
        players.append(GKLocalPlayer.local)
        players.shuffle()
        let order = players.map { player -> String in
            player.playerID
        }

        os_log("player order is: %s", log: GameCenterMatchManager.logObject, type: .info,
                order.debugDescription)

        let action = PlayerOrderDeterminedAction(order)
        do {
            try match.sendData(toAllPlayers: Data.fromModel(action), with: .reliable)
        } catch {
            os_log("Failed to send player order %s", log: GameCenterMatchManager.logObject, type: .error,
                    (error as CustomDebugStringConvertible).debugDescription)
        }

        action.apply(GameCenterManager.instance, GameCenterManager.instance.currentMatch!, from: GKLocalPlayer.local)
    }

    func registerTroopChange(_ position: String, _ troopCount: Int, _ owner: Player) {
        os_log("registered troop change. position: %s, count: %d", log: GameCenterMatchManager.logObject, type: .info,
                position, troopCount)
        let tile = GameTile(troopCount: troopCount, owner: owner)
        GameCenterManager.instance.localTroopChanges[position] = tile
    }

    func registerWinnerLooser(winner: Player, looser: Player, killedAttackers: Int, killedDefenders: Int) {
        os_log("registered Winner and looser. Winner: %s, Looser: %s", log: GameCenterMatchManager.logObject, type: .info,
                winner.toString(), looser.toString())
        let gcm = GameCenterManager.instance
        gcm.fightWinnerLooser["winner"] = winner
        gcm.fightWinnerLooser["looser"] = looser
        gcm.killedAttackers = killedAttackers
        gcm.killedDefenders = killedDefenders
    }

    func finishLocalReinforcement() {
        os_log("finishLocalReinforcement", log: GameCenterMatchManager.logObject, type: .info)
        let gcm = GameCenterManager.instance

        os_log("send message to save local player reinforcement changes",
                log: GameCenterMatchManager.logObject, type: .info)

        let action = FinishLocalReinforcementAction(gcm.localTroopChanges, gcm.localCityUpgrades)

        do {
            try gcm.currentMatch?.sendData(toAllPlayers: Data.fromModel(action), with: .reliable)
        } catch {
            os_log("finishLocalReinforcement failed %s", log: GameCenterMatchManager.logObject,
                    type: .error, (error as CustomDebugStringConvertible).debugDescription)
        }

        action.apply(gcm, gcm.currentMatch!, from: GKLocalPlayer.local)

        gcm.localCityUpgrades.removeAll()
        gcm.localTroopChanges.removeAll()
    }

    func isReinforcementFinished() -> Bool {
        let gcm = GameCenterManager.instance
        // +1 because players only contains remote players and not the local player
        return gcm.currentMatch!.players.count + 1 == gcm.finishedReinforcementPlayers.count
    }

    func doPostReinforcementCleanup() {
        let gcm = GameCenterManager.instance

        gcm.allCityUpgrades.removeAll()
        gcm.allTroopChanges.removeAll()
        gcm.finishedReinforcementPlayers.removeAll()
        gcm.fightWinnerLooser = [:]
    }

    func finishReinforcementForAll() {
        let gcm = GameCenterManager.instance

        os_log("Finishing reinforcement for all. Troop changes %s",
                log: GameCenterMatchManager.logObject, type: .info, gcm.allTroopChanges.debugDescription)
        let action = FinishReinforcementAction(gcm.allTroopChanges, gcm.allCityUpgrades)

        do {
            try gcm.currentMatch!.sendData(toAllPlayers: Data.fromModel(action), with: .reliable)
        } catch {
            os_log("failed sending finish reinforcement for all %s", log: GameCenterMatchManager.logObject,
                    type: .error, (error as CustomDebugStringConvertible).debugDescription)
        }

        action.apply(gcm, gcm.currentMatch!, from: GKLocalPlayer.local)
    }

    func finishCombatWithMove() {
        os_log("finishCombatWithMove", log: GameCenterMatchManager.logObject, type: .info)
        let gcm = GameCenterManager.instance

        let combatDetails = determineNextPlayerIdAndCombatRound()
        let action = CombatMoveAction(gcm.localTroopChanges, combatDetails.nextPlayerId, combatDetails.combatRound)

        do {
            try gcm.currentMatch!.sendData(toAllPlayers: Data.fromModel(action), with: .reliable)
        } catch {
            os_log("finishCombatWithMove failed %s", log: GameCenterMatchManager.logObject,
                    type: .error, (error as CustomDebugStringConvertible).debugDescription)
        }

        gcm.localTroopChanges.removeAll()

        action.apply(gcm, gcm.currentMatch!, from: GKLocalPlayer.local)
    }

    func finishCombatWithFight() {
        os_log("finishCombatWithFight", log: GameCenterMatchManager.logObject, type: .info)
        let gcm = GameCenterManager.instance

        let combatDetails = determineNextPlayerIdAndCombatRound()
        let action = CombatFightAction(
                gcm.localTroopChanges,
                combatDetails.nextPlayerId,
                combatDetails.combatRound,
                gcm.fightWinnerLooser,
                gcm.killedAttackers,
                gcm.killedDefenders)

        do {
            try gcm.currentMatch!.sendData(toAllPlayers: Data.fromModel(action), with: .reliable)
        } catch {
            os_log("finishCombatWithFight failed %s", log: GameCenterMatchManager.logObject,
                    type: .error, (error as CustomDebugStringConvertible).debugDescription)
        }

        gcm.localTroopChanges.removeAll()
        gcm.fightWinnerLooser = [:]
        gcm.killedAttackers = 0
        gcm.killedDefenders = 0

        action.apply(gcm, gcm.currentMatch!, from: GKLocalPlayer.local)
    }

    func finishCombatWithNone(combatInfo: (nextPlayerId: String?, combatRound: Int)? = nil) {
        os_log("finishCombatWithNone", log: GameCenterMatchManager.logObject, type: .info)
        let gcm = GameCenterManager.instance

        let combatDetails = combatInfo ?? determineNextPlayerIdAndCombatRound()
        let action = CombatNoneAction(combatDetails.nextPlayerId, combatDetails.combatRound)

        do {
            try gcm.currentMatch!.sendData(toAllPlayers: Data.fromModel(action), with: .reliable)
        } catch {
            os_log("finishCombatWithNone failed %s", log: GameCenterMatchManager.logObject,
                    type: .error, (error as CustomDebugStringConvertible).debugDescription)
        }

        action.apply(gcm, gcm.currentMatch!, from: GKLocalPlayer.local)
    }

    private func determineNextPlayerIdAndCombatRound() -> (nextPlayerId: String?, combatRound: Int) {
        let gcm = GameCenterManager.instance
        let playerOrder = gcm.playerOrder!

        let currentPlayerIndex = playerOrder.firstIndex(of: GKLocalPlayer.local.playerID)!
        return determineNextPlayerIdAndCombatRound(currentPlayerIndex: currentPlayerIndex)
    }

    func determineNextPlayerIdAndCombatRound(currentPlayerIndex: Int)
                    -> (nextPlayerId: String?, combatRound: Int) {
        let gcm = GameCenterManager.instance
        let playerOrder = gcm.playerOrder!

        let nextPlayerIndex = (currentPlayerIndex + 1) % playerOrder.count
        var nextPlayerId: String? = playerOrder[nextPlayerIndex]

        var combatRound = gcm.combatRound
        if (playerOrder.first == nextPlayerId) {
            combatRound += 1
        }
        if (combatRound >= GameValues.combatRounds) {
            combatRound = 0
            nextPlayerId = nil
        }

        return (nextPlayerId: nextPlayerId, combatRound: combatRound)
    }
}
