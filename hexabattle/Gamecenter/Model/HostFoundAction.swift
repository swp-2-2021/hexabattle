//
// Created by student on 05.06.21.
//

import Foundation
import SerializedSwift
import GameKit

class HostFoundAction: Action {

    required init() {
        super.init()
        super.type = .hostFound
    }

    required init(_ host: String) {
        super.init()
        super.type = .hostFound
        self.host = host
    }

    /**
     PlayerId of the host
     */
    @Serialized
    var host: String

    override func apply(_ gcm: GameCenterManager, _ match: GKMatch, from player: GKPlayer) {
        // host id must be inside match.players. Because the host notified the local player through the match
        let host = match.players.first { player in
            player.playerID == self.host
        }!
        gcm.host = host
    }
}
