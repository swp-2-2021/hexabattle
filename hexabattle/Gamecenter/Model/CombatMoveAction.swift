//
// Created by student on 07.06.21.
//

import Foundation
import GameKit
import os.log

class CombatMoveAction: CombatAction {

    required init() {
        super.init()
        super.type = .combatMove
    }

    override init(_ troopChanges: [String: GameTile], _ nextPlayerId: String?, _ combatRound: Int) {
        super.init(troopChanges, nextPlayerId, combatRound)
        super.type = .combatMove
    }

    override func apply(_ gcm: GameCenterManager, _ match: GKMatch, from player: GKPlayer) {
        os_log("Received combatMove troop changes %s", type: .info, troopChanges.debugDescription)

        super.apply(_: gcm, _: match, from: player)
    }

}
