//
// Created by student on 05.06.21.
//

import Foundation

/**
 Serializable representation of a tile
 */
struct GameTile: Codable {

    /**
     The current amount of troops on this tile
     */
    var troopCount: Int

    /**
     The player who owns the tile
     */
    var owner: Player
}
