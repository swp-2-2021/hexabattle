//
// Created by student on 05.06.21.
//

import Foundation
import SerializedSwift
import GameKit
import os.log

class FinishReinforcementAction: Action {

    required init() {
        super.init()
        super.type = .finishReinforcement
    }

    required init(_ troopChanges: [String: GameTile], _ cityUpgrades: [String: Int]) {
        super.init()
        super.type = .finishReinforcement
        self.troopChanges = troopChanges
        self.cityUpgrades = cityUpgrades
    }

    /**
     Troops that were placed by the all players in the current reinforcement phase.
     The key is the same one as in MapManager
    */
    @Serialized
    var troopChanges: [String: GameTile]

    @Serialized
    var cityUpgrades: [String: Int]

    override func apply(_ gcm: GameCenterManager, _ match: GameKit.GKMatch, from player: GameKit.GKPlayer) {
        os_log("Received reinforcement troop changes %s", type: .info, troopChanges.debugDescription)
        GameCenterMatchManager().doPostReinforcementCleanup()

        GameBehavior.instance.updateReceivedTiles(troopChanges)
        for (dictKey, level) in cityUpgrades {
            TileBehavior.instance.upgradeCityTile(key: dictKey, level: level)
        }

        let currentPlayer = gcm.playerOrder?.first
        gcm.currentPlayerId = currentPlayer

        if (currentPlayer == GKLocalPlayer.local.playerID) {
            StateMachine.instance.enter(CombatState.self)
        } else {
            StateMachine.instance.enter(OtherPlayerTurnState.self)
        }
    }
}
