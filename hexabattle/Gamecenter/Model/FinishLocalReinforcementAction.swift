//
// Created by student on 05.06.21.
//

import Foundation
import SerializedSwift
import GameKit

class FinishLocalReinforcementAction: Action {

    required init() {
        super.init()
        super.type = .finishLocalReinforcement
    }

    required init(_ troopChanges: [String: GameTile], _ cityUpgrades: [String: Int]) {
        super.init()
        super.type = .finishLocalReinforcement
        self.troopChanges = troopChanges
        self.cityUpgrades = cityUpgrades
    }

    /**
     Troops that were placed by the local players who sent this action 
     during the current reinforcement phase.
     The key is the same one as in MapManager
    */
    @Serialized
    var troopChanges: [String: GameTile]

    /**
    Cities That were upgraded by the local players who sent this action
    during the current reinforcement phase.
    The key is the same one as in MapManager
    */
    @Serialized
    var cityUpgrades: [String: Int]

    override func apply(_ gcm: GameCenterManager, _ match: GameKit.GKMatch, from player: GameKit.GKPlayer) {
        gcm.allTroopChanges.merge(troopChanges) { (current: GameTile, _: GameTile) in
            current
        }
        gcm.finishedReinforcementPlayers.insert(player.playerID)

        gcm.allCityUpgrades.merge(cityUpgrades) { (current: Int, _: Int) in
            current
        }

        let gcmm = GameCenterMatchManager()
        if (gcm.isHost && gcmm.isReinforcementFinished()) {
            gcmm.finishReinforcementForAll()
        }
    }
}
