//
// Created by student on 05.06.21.
//

import Foundation
import SerializedSwift

enum ActionType: String, Codable {

    /**
     The host was found
     */
    case hostFound

    /**
     The player order was determined
     */
    case playerOrder

    /**
     A single player has finished their own local reinforcement phase
     */
    case finishLocalReinforcement

    /**
     Reinforcement phase is finishing for all players
     */
    case finishReinforcement

    /**
     A player moved troops during their combat phase
     */
    case combatMove

    /**
     A player attacked a field during their combat phase
     */
    case combatFight

    /**
     A player attacked did nothing during their combat phase (timer ran out)
     */
    case combatNone

    /**
     A Player can start his combat phase
     */
    case nextPlayerTurn

    case finishCombat

    /**
     Only used for serialization
     */
    case none

}
