//
// Created by student on 16.06.21.
//

import Foundation
import GameKit
import os.log

class CombatNoneAction: CombatAction {

    required init() {
        super.init()
        super.type = .combatNone
    }

    init(_ nextPlayerId: String?, _ combatRound: Int) {
        super.init([:], nextPlayerId, combatRound)
        super.type = .combatNone
    }

    override func apply(_ gcm: GameCenterManager, _ match: GKMatch, from player: GKPlayer) {
        os_log("Received combatNone action %s", type: .info)

        super.apply(_: gcm, _: match, from: player)
    }
}
