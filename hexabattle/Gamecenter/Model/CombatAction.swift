//
// Created by student on 16.06.21.
//

import Foundation
import GameKit
import SerializedSwift
import os.log

class CombatAction: Action {

    required init() {
        super.init()
        super.type = .none
    }

    init(_ troopChanges: [String: GameTile], _ nextPlayerId: String?, _ combatRound: Int) {
        super.init()
        self.troopChanges = troopChanges
        self.nextPlayerId = nextPlayerId
        self.combatRound = combatRound
    }

    @Serialized
    var troopChanges: [String: GameTile]

    @Serialized
    var nextPlayerId: String?

    @Serialized
    var combatRound: Int

    override func apply(_ gcm: GameCenterManager, _ match: GKMatch, from player: GKPlayer) {
        GameBehavior.instance.updateReceivedTiles(troopChanges)
        if (GameBehavior.instance.isGameOver()) {
            return
        }

        gcm.combatRound = combatRound
        if nextPlayerId != nil {
            os_log("nextPlayerId is not null", type: .info)

            gcm.currentPlayerId = nextPlayerId
            if nextPlayerId == GKLocalPlayer.local.playerID {
                os_log("local player is next player")
                StateMachine.instance.enter(CombatState.self)
            }
        } else {
            // async because at the end of the combat phase it sends the gamecenter messages and then enters OtherPlayerState
            // That overrides the change here to enter ReinforcementState
            DispatchQueue.main.async {
                os_log("nextPlayerId is null. Finishing combat phase")
                StateMachine.instance.enter(ReinforcementState.self)
            }
        }
    }
}
