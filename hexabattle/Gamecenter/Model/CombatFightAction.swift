//
// Created by student on 07.06.21.
//

import Foundation
import SerializedSwift
import GameKit
import os.log

class CombatFightAction: CombatAction {

    @Serialized
    var fightWinnerLooser: [String: Player]

    @Serialized
    var killedAttackers: Int

    @Serialized
    var killedDefenders: Int

    required init() {
        super.init()
        super.type = .combatFight
    }

    init(_ troopChanges: [String: GameTile],
         _ nextPlayerId: String?,
         _ combatRound: Int,
         _ fightWinnerLooser: [String: Player],
         _ killedAttackers: Int,
         _ killedDefenders: Int
    ) {
        super.init(troopChanges, nextPlayerId, combatRound)
        super.type = .combatFight
        self.fightWinnerLooser = fightWinnerLooser
        self.killedAttackers = killedAttackers
        self.killedDefenders = killedDefenders
    }

    override func apply(_ gcm: GameCenterManager, _ match: GKMatch, from player: GKPlayer) {
        os_log("Received combatFight troop changes %s", type: .info, troopChanges.debugDescription)
        if let winner = fightWinnerLooser["winner"] {
            if let looser = fightWinnerLooser["looser"] {
                os_log("Received WinnerLoser winner: %s looser: %s", type: .info, winner.toString(), looser.toString())
            }
        } else {
            os_log("Received WinnerLoser, no winner, no looser", type: .info)
        }

        if fightWinnerLooser["winner"] == EntityManager.instance.player {
            FightInfoMsg.showWinningMessage(message: "Fight Won! Total defenders killed \(killedDefenders). Total attackers killed \(killedAttackers)")
        }
        if fightWinnerLooser["looser"] == EntityManager.instance.player {
            FightInfoMsg.showLosingMessage(message: "Fight Lost! Total attackers killed \(killedAttackers). Total defenders killed \(killedDefenders)")
        }

        super.apply(_: gcm, _: match, from: player)
    }
}
