//
// Created by student on 05.06.21.
//

import Foundation
import SerializedSwift
import GameKit

class Action: Serializable {

    /**
     Should be of type ActionType, but SerializedSwift does not work proberly with enum
     */
    @Serialized
    var type: ActionType

    required init() {
        type = .none
    }

    init(_ type: ActionType) {
        self.type = type
    }

    func apply(_ gcm: GameCenterManager, _ match: GKMatch, from player : GKPlayer) {
        fatalError("Subclasses need to implement 'apply()'")
    }
}
