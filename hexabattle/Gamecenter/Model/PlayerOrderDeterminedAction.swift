//
// Created by student on 05.06.21.
//

import Foundation
import SerializedSwift
import GameKit
import os.log

class PlayerOrderDeterminedAction: Action {

    required init() {
        super.init()
        super.type = .playerOrder
    }

    required init(_ playerOrder: [String]) {
        super.init()
        super.type = .playerOrder
        self.playerOrder = playerOrder

    }

    /**
    playerIds of the player in the match in order of their turns
    */
    @Serialized
    var playerOrder: [String]

    override func apply(_ gcm: GameCenterManager, _ match: GKMatch, from player: GKPlayer) {
        os_log("Received player order: %s", type: .info, playerOrder.debugDescription)
        GameCenterManager.instance.playerOrder = playerOrder

        GameBehavior.instance.initGame(playerOrder)
        StateMachine.instance.enter(ReinforcementState.self)

    }
}
