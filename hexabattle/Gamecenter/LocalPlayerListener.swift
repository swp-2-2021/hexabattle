//
// Created by student on 03.06.21.
//

import Foundation
import GameKit
import NotificationCenter
import os.log

internal class LocalPlayerListener: NSObject, GKLocalPlayerListener {

    private static let logObject = OSLog(subsystem: OSLog.subsystem, category: "gamecenter.listener")

    func player(_ player: GKPlayer, didAccept invite: GKInvite) {
        os_log("didAccept", log: LocalPlayerListener.logObject, type: .info)

        GameCenterManager.instance.matchmaker.showMatchmaker(invite)
    }

    func player(_ player: GKPlayer, didRequestMatchWithRecipients recipientPlayers: [GKPlayer]) {
        os_log("didRequestMatchWithRecipients", log: LocalPlayerListener.logObject, type: .info)

    }
}
