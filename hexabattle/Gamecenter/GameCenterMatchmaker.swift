//
// Created by student on 03.06.21.
//

import Foundation
import GameKit
import os.log

class GameCenterMatchmaker {

    private static let logObject = OSLog(subsystem: OSLog.subsystem, category: "gamecenter.matchmaker")
    private static var matchmakerVC: GKMatchmakerViewController? = nil {
        didSet {
            oldValue?.dismiss(animated: false)
        }
    }

    // A strong reference preventing the delegate to be deallocated immediately
    // swiftlint:disable:next weak_delegate
    private var delegate: MatchmakerDelegate?

    /**
     Show the GameCenter matchmaker view controller.
     If an invite is passed it will be for that invite.
     Otherwise it will be for a new game request
     - Parameter invite: The optional invite to show
     */
    func showMatchmaker(_ invite: GKInvite? = nil, playerNumber: Int = 4) {
        guard GKLocalPlayer.local.isAuthenticated else {
            os_log("User is not authenticated", log: GameCenterMatchmaker.logObject, type: .error)
            return
        }

        if let invite = invite {
            GameCenterMatchmaker.matchmakerVC = GKMatchmakerViewController(invite: invite)!
        } else {
            let request = GKMatchRequest()

            request.minPlayers = playerNumber
            request.maxPlayers = playerNumber

            request.inviteMessage = "Join my game"

            GameCenterMatchmaker.matchmakerVC = GKMatchmakerViewController(matchRequest: request)!
        }

        delegate = MatchmakerDelegate(GameCenterMatchmaker.matchmakerVC!)
        GameCenterMatchmaker.matchmakerVC!.matchmakerDelegate = delegate

        let rootVC = UIApplication.shared.windows.first!.rootViewController
        rootVC!.present(GameCenterMatchmaker.matchmakerVC!, animated: true)
    }
}

private class MatchmakerDelegate: NSObject, GKMatchmakerViewControllerDelegate {

    private static let logObject = OSLog(subsystem: OSLog.subsystem, category: "gamecenter.matchmaker.delegate")

    private let matchmakerVC: GKMatchmakerViewController

    init(_ matchmakerVC: GKMatchmakerViewController) {
        self.matchmakerVC = matchmakerVC
    }

    func matchmakerViewControllerWasCancelled(_ viewController: GKMatchmakerViewController) {
        os_log("matchmakerViewControllerWasCancelled", log: MatchmakerDelegate.logObject, type: .info)

        matchmakerVC.dismiss(animated: true)
    }

    func matchmakerViewController(_ viewController: GKMatchmakerViewController, didFailWithError error: Error) {
        os_log("didFailWithError", log: MatchmakerDelegate.logObject, type: .error)
    }

    func matchmakerViewController(_ viewController: GKMatchmakerViewController, didFind match: GKMatch) {
        os_log("didFind", log: MatchmakerDelegate.logObject, type: .info)

        match.delegate = GameCenterManager.instance.matchDelegate
        matchmakerVC.dismiss(animated: true)
        GameCenterManager.instance.currentMatch = match

        StateMachine.instance.enter(PreGameState.self)
    }
}
