//
// Created by student on 03.06.21.
//

import Foundation
import GameKit
import os.log

/**
 Don't use any classes besides this outside gamecenter package. Don't use any field or functions that are not public
 This class contains the state for the gamecenter.
 All other gamecenter classes should be stateless and rely on this, in order to allow creation from anywhere.
 */
class GameCenterManager: NSObject {

    private static let logObject = OSLog(subsystem: OSLog.subsystem, category: "gamecenter.manager")
    static var instance = GameCenterManager()

    internal var currentMatch: GKMatch?

    // member field in order to not be deallocated
    internal let matchmaker: GameCenterMatchmaker
    // A strong reference preventing the delegate to be deallocated immediately
    // swiftlint:disable:next weak_delegate
    internal var matchDelegate: MatchDelegate

    // A strong reference preventing the delegate to be deallocated immediately
    // That is probably the reason it is needed, without it invites don't work
    internal var inviteListener: LocalPlayerListener
    /**
     The playerId of the player who acts as host
     */
    internal var host: GKPlayer?
    internal var isHost: Bool {
        // ? instead of ! in case that host is not determined.
        host?.playerID == GKLocalPlayer.local.playerID
    }

    internal var playerOrder: [String]?

    /**
     Troop changes made by the local player in the current phase
     */
    internal var localTroopChanges: [String: GameTile] = [:]

    /**
    City upgrades made by the local player in the current phase
    */
    internal var localCityUpgrades: [String: Int] = [:]

    /**
    City upgrades made by all player in the current phase.
    The host will fill this field during the reinforcement phase and send these out at the end.
    */
    internal var allCityUpgrades: [String: Int] = [:]

    /**
     The playerId the player who have finished their reinforcement phase
     */
    internal var finishedReinforcementPlayers: Set<String> = []

    /**
     The players that won or lost a fight
     */
    internal var fightWinnerLooser: [String: Player] = ["winner": Player.none, "looser": Player.none]

    /**
     How many attackers were killed during a fight
     */
    internal var killedAttackers: Int = 0

    /**
     How many defenders were killed during a fight
     */
    internal var killedDefenders: Int = 0

    /**
     Troop changes made by all player in the current phase.
     The host will fill this field during the reinforcement phase and send these out at the end.
     */
    internal var allTroopChanges: [String: GameTile] = [:]

    /**
     The id of the player who currently has the turn in the combat phase.
     Only set during combat phase, otherwise null
     */
    internal var currentPlayerId: String?

    /**
     How many rounds of combat already occurred in the current combat phase
     */
    internal var combatRound = 0

    private override init() {
        matchmaker = GameCenterMatchmaker()
        matchDelegate = MatchDelegate()
        inviteListener = LocalPlayerListener()
    }

    func showMatchmaker(playerNumber: Int) {
        matchmaker.showMatchmaker(playerNumber: playerNumber)
    }

    func leaveGame() {
        GameCenterManager.instance.currentMatch?.disconnect()

        // reset state after leaving game
        GameCenterManager.instance = GameCenterManager()
    }

    /**
     Register the placement of troops in the reinforcement phase.
     Needed as temporary save place since troops can be set multiple time throughout the reinforcement phase,
     but it should only be communicated with others player at the end the phase.
      
     - Parameters:
       - position: The position in the same format as in MapManger
       - currentTroops: The number of troops on the field, after the placement
     */
    func registerTroopChange(position: String, troopCount: Int, owner: Player) {
        GameCenterMatchManager().registerTroopChange(position, troopCount, owner)
    }

    /*
    Finish the reinforcement phase for the local player
    */
    func finishLocalReinforcement() {
        GameCenterMatchManager().finishLocalReinforcement()
    }

    func finishLocalCombatWithMove() {
        GameCenterMatchManager().finishCombatWithMove()
    }

    func finishLocalCombatWithFight() {
        GameCenterMatchManager().finishCombatWithFight()
    }

    internal func determineHost() {
        let shouldChoose = shouldBeInitialHost()
        os_log("Local player will choose host: %s", log: GameCenterManager.logObject, type: .info,
                String(shouldChoose))
        if (shouldChoose) {
            GameCenterManager.instance.host = GKLocalPlayer.local

            let action = HostFoundAction(GKLocalPlayer.local.playerID)
            do {
                try currentMatch!.sendData(toAllPlayers: Data.fromModel(action), with: .reliable)
            } catch {
                os_log("failed to send host: %s", log: GameCenterManager.logObject, type: .error,
                        (error as CustomDebugStringConvertible).debugDescription)
            }
        }
    }

    /**
     Deterministic way to determine if the local player should be the host.
     In a given match only one player will get true, all other get false.
     - Returns: Whether the local player should be the host.
     */
    private func shouldBeInitialHost() -> Bool {
        var players = currentMatch!.players
        players.append(GKLocalPlayer.local as GKPlayer)
        players.sort { player, player2 in
            player.playerID < player2.playerID
        }

        let hostChooser = players.first!

        return GKLocalPlayer.local.playerID == hostChooser.playerID
    }
}
