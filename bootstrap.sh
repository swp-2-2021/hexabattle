#!/bin/sh

echo "Configuring git hooks..."

# make a symbolic link with the commit-msg hook
if [ ! -f ./git/hooks/commit-msg ]; then
  ln git-hooks/commit-msg .git/hooks/commit-msg
  echo "Done"
else
  cat <<EOF
A commit-msg hook exists already.
EOF
fi

# make a symbolic link with the pre-commit hook
if [ ! -f ./git/hooks/pre-commit ]; then
  ln git-hooks/pre-commit .git/hooks/pre-commit
  echo "Done"
else
  cat <<EOF
A pre-commit hook exists already.
EOF
fi